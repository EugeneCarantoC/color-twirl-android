# Color Twirl

Color Twirl is a game created using JAVA programming language and is targeted for Android. 

## Game Mechanics

Shoot the bullet to it's matching color on a spinning color wheel.

Each stage has its own unique pattern of colors and behavior when spinning. (Some has rotating shied to avoid!).

Unlock all stage by consuming all bullets without hitting the wrong color or hitting an obstacle.
   
Watch the demo video: https://www.youtube.com/watch?v=l34EDafJyqo
 
## Author

* **Eugene C. Caranto** 

## Acknowledgments

*  [Kilobolt](http://www.kilobolt.com/game-development-tutorial.html) - Base framework used
