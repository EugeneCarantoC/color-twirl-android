package com.eugexstudios.colortwirl;
   
import android.graphics.Typeface;

import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.Music;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.Sound;
import com.eugexstudios.framework.Graphics.ImageFormat;

public class AssetManager extends Screen {
	public AssetManager(Game game) {
		super(game); 
	}

	public static Sound 
	click
	, GeneralizedSound 
	, level_complete;
	 
	public static Music GameMusic; 
	public static Typeface Comforta; 
	public static Image image ; 
	public static Image privacy_policy ; 
	public static Image sprites ; 
	public static Image wheel_detail ;  
	public static Image background_shades ;  
	public static Sound wheel_painted;
	public static Sound shoot_sound;
	public static Sound wrong_hit_shield;
	public static Sound splash_sound;
	public static Sound game_result_entry;
	 
	@Override
	public void update(float deltaTime) { 
 		Graphics g = game.getGraphics();
// 		GameMusic	      = game.getAudio().createMusic("DARK_BETWEEN_STARS.mp3");
//		splash_sound  = game.getAudio().createSound("34170__glaneur-de-sons__electric-wire-03.ogg");   
 		Comforta = Typeface.createFromAsset(g.getAssetManager(), "Comfortaa-Bold.ttf" );    
// 		PowerChord = Typeface.createFromAsset(g.getAssetManager(), "PowerChord.ttf" );    
 		sprites			  = g.newImage("sprites.png", ImageFormat.ARGB8888); 
 		background_shades = g.newImage("background_shades.png", ImageFormat.ARGB8888);  
 		wheel_detail	  = g.newImage("wheel_detail.png", ImageFormat.ARGB8888);  
 		privacy_policy	  = g.newImage("privacypolicy.png", ImageFormat.ARGB8888);  
 		
		splash_sound      = game.getAudio().createSound("71852__ludvique__digital-whoosh-soft.ogg");   
	 	click 			  = game.getAudio().createSound("191678__porphyr__waterdrop.ogg");    //328117__greenvwbeetle__pop
	 	GeneralizedSound  = game.getAudio().createSound("prompt_soft.wav");   
	 	wheel_painted 	  = game.getAudio().createSound("wheel_painted.ogg");  
	 	shoot_sound 	  = game.getAudio().createSound("shoot_sound.ogg");  
	 	wrong_hit_shield  = game.getAudio().createSound("explosion.ogg");  //explosion
	 	level_complete    = game.getAudio().createSound("level_complete.ogg");  
	 	game_result_entry = game.getAudio().createSound("game_result_entry.ogg");  
	 	
	 	 
 	game.setScreen(new GameScreen(game)); 

	}
	 

	@Override
	public void paint(float deltaTime) 
	{
 
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	@Override
	public void backButton() {

	}
}