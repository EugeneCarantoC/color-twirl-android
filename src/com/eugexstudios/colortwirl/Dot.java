package com.eugexstudios.colortwirl;
 
import java.util.Random;
import java.util.Vector;
  


import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;  
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;

import android.graphics.Point;
import android.graphics.Rect;

public class Dot {
 
	private float				       m_posX;
	private float				       m_posY;
	private int 				       m_radius;
	private int 					   m_speed;
	private int				  		   m_color;
	private Rect 					   m_bounds;
	private boolean					   m_released; 
	private boolean 				   m_active;
	
	private enum TAIL_STYLE {CIRCLE, FADESQUARE};
	private TAIL_STYLE				   m_tailStyle;
	private Vector<Point>			   m_tailPts;
	private Vector<int[]>			   m_tailRad;
	private float		   			   m_tailGap; 
	private float 					   m_noticeRad; 
	private float 					   m_moveBuffer; 
	private float					   m_noticeTimer;   
	private boolean					   m_shattering; 
	private Vector<DotShatter>	       m_dotShatter;
	

	public Dot(int p_color)
	{
		m_color       = p_color;
		ActivateFull();
	}
	public void ActivateFull()
	{
		m_tailStyle   = TAIL_STYLE.CIRCLE;
		m_tailGap	  = m_tailStyle==TAIL_STYLE.FADESQUARE?0.05f:0.1f;
		m_posX 	      = CONFIG.SCREEN_MID;
		m_posY 	      = Constants.DOT_POS_Y;
		m_radius      = Constants.DOT_RADIUS;
		m_released    = false; 
		m_speed	      = Constants.DOT_SPEED;
		m_active      = false;
		m_moveBuffer  = 0;
	
		m_tailPts     = new Vector<Point>();
	  	m_tailRad     = new Vector<int[]>();
	  	  
	  	m_noticeTimer = 0;
		m_noticeRad   = 1;
	
		m_shattering  = false; 
		m_dotShatter  = new Vector<DotShatter>();
		UpdateBounds();
	}
	
	public void Activate()
	{
	  	m_noticeTimer = 0;
		m_noticeRad   = 1;
		m_active      = true;
	} 
	
	public void Shatter()
	{ 
		m_shattering=true;
		Random l_rand = new Random();
		Disapear();
		
		int SHATTER_SIZE=37+l_rand.nextInt(5);
  
		for(int idx=0;idx<SHATTER_SIZE;idx++)
		{
			m_dotShatter.add(new DotShatter(getPos(),m_color,false));
		}
	}
	
	public void ShatterFinish(int[] p_colors)
	{ 
		m_shattering=true; 
		Random l_rand = new Random();
		Disapear();
 
		int SHATTER_SIZE=90+l_rand.nextInt(40); 
		for(int idx=0;idx<SHATTER_SIZE;idx++)
		{ 
			m_dotShatter.add(new DotShatter(getPos(),p_colors[l_rand.nextInt(p_colors.length)],true));
		} 
	}
	public boolean isShattering()
	{
		return m_shattering;
	}
	public void SetPosition(Point p_pos)
	{
		m_posX = p_pos.x;
		m_posY = p_pos.y;
	}
	public void Paint(Graphics g, float p_radius)
	{ 
//		if(Debugger.show_bounds && !Debugger.DEMO_MODE)
//		{
//			g.drawRect(m_bounds, Constants.DEBUG_BOUNDS_COLOR);
//		}    
		g.drawCircle((int)m_posX, (int)m_posY, (int)p_radius, m_color);
 
	}
	 
	public void PaintTail(Graphics g)
	{
		switch(m_tailStyle)
		{
			case CIRCLE:
				for(int idx=0;idx<m_tailPts.size();idx++)
				{ 
					g.drawCircle(m_tailPts.get(idx),  m_tailRad.get(idx)[0],ColorUtil.SetOpacity(0.45f+idx*2,m_color));   
				}
			break;
				
			case FADESQUARE: 
				for(int idx=0;idx<m_tailPts.size();idx++)
				{ 
					g.drawRect(m_tailPts.get(idx).x - Constants.DOT_RADIUS, m_tailPts.get(idx).y, Constants.DOT_RADIUS*2, (m_tailPts.size()+idx)*5, ColorUtil.SetOpacity(idx*0.08f,m_color));
				}
				break;
		}

	}
	
	public void PaintShattered(Graphics g)
	{ 
		
		for(int idx=0;idx<m_dotShatter.size();idx++)
		{ 
			m_dotShatter.get(idx).Paint(g);
		 }
	}
	

	public void Paint(Graphics g, int p_x, int p_y, int p_rad)
	{
		g.drawCircle((int) p_x,(int)p_y, p_rad,m_color);   
	}
	public void Paint(Graphics g)
	{ 
//		if(Debugger.show_bounds && !Debugger.DEMO_MODE)
//		{
//			g.drawRect(m_bounds, Constants.DEBUG_BOUNDS_COLOR);
//		}   
		if(m_noticeRad>0&&!m_released)
		{
			g.drawCircle((int) m_posX,(int)m_posY, (int)(m_radius+(m_radius*2*(1-m_noticeRad))),ColorUtil.SetOpacity(m_noticeRad, m_color)); 
		} 
		if(m_active)
		{ 
			g.drawCircle((int) m_posX,(int)m_posY, m_radius,m_color); 
 
		}
		

		
	} 
	public void Reactivate()
	{ 
		ActivateFull();
		m_released=false;
		m_active=true;
	}
	public void Shoot()
	{ 
		if(m_released)return ;
		m_released = true;  
		AssetManager.shoot_sound.play(); 
	}
	
	public boolean isReleased()
	{
		return m_released;
	}	
	
	public boolean isActive()
	{
		return m_active;
	}

	public void Stop()
	{
		m_speed=0; 
	}

	public void Disapear()
	{ 
		m_active=false;
	}
	
	public Rect getBounds()
	{
		return m_bounds;
	}
	
	public void Update(float p_deltaTime, int p_noticeSpeed)
	{
		if(m_released && m_active)
		{
			float l_accel = m_speed*p_deltaTime;
			m_posY  	  = m_posY - l_accel; 
			m_moveBuffer  = m_moveBuffer + l_accel;
			UpdateBounds();  
		}
		
		m_noticeTimer+=1*(p_deltaTime);
		if(m_noticeTimer>=p_noticeSpeed&&m_noticeRad<=0)
		{
			m_noticeRad=1;
			m_noticeTimer=0;
		}
		else if(m_noticeRad>0)
		{
			m_noticeRad-=0.015*(p_deltaTime);
		}
	}
	 
	public void UpdateTail(float p_deltaTime)
	{
		if(m_moveBuffer>=m_tailGap)
		{
			m_moveBuffer=0;

			m_tailPts.add(getPos());
			m_tailRad.add(new int[]{Constants.DOT_RADIUS});
		}
		
		for(int idx=0;idx<m_tailPts.size();idx++)
		{ 
			 m_tailRad.get(idx)[0]-=0.50f*p_deltaTime;
			 
			 if(m_tailRad.get(idx)[0]<=0)
			 {
				 m_tailRad.remove(idx);
				 m_tailPts.remove(idx);
			 }
		}
		
	}
	public void UpdateShatter(float p_deltaTime)
	{
	   for(int idx=0;idx<m_dotShatter.size();idx++)
	   {
		   m_dotShatter.get(idx).Update(p_deltaTime);
	   }
	}
	
	private void UpdateBounds()
	{
		m_bounds = new Rect((int)(m_posX- Constants.DOT_RADIUS),(int)(m_posY- Constants.DOT_RADIUS), (int)(m_posX+Constants.DOT_RADIUS),(int)(m_posY+Constants.DOT_RADIUS));
	}

	public void AdjustY(int p_adj) 
	{  
		m_posY+=p_adj;
		UpdateBounds(); 
	}
	
	public int GetColor()
	{
		return m_color;
	}
	 
	public Point getPos()
	{
		return new Point((int)(m_posX),(int)(m_posY));
	}
}
