package com.eugexstudios.colortwirl;

import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Paint.Align; 

import com.eugexstudios.adprovider.AdProvider; 
import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.colortwirl.AskUserWindow.QUESTION; 
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class GameOverWindow  extends Screen{
	
	private final byte 
	  RESTART_BTN  = 2 
	, CONTINUE_BTN = 4  
	, HOME_BTN 	   = 6
	, LEVEL_SEL_BTN =7 
    , AD_BTN  = 8
	;
	private enum LeaveDest { 
		  LEVEL_SELECT
		, MENU 
		
	}; 
	
	public enum State 
	{ 
		  ENTRY
		, IDLE  
		, RESTART  
		, LEAVE  
		, AD_OPENLOAD
		, CONTINUE_ASK
	}
	
	private final int ANIM_CTR_SIZE=7; 
	private final int SUB_BTN_DIM_1 =(int)(CONFIG.SCREEN_WIDTH*0.09f); //(int)(CONFIG.SCREEN_WIDTH*0.15f);
	private final int SUB_BTN_DIM_2 =(int)(CONFIG.SCREEN_WIDTH*0.08f); //(int)(CONFIG.SCREEN_WIDTH*0.15f);
	 
	private State 				       m_state;   
	private LeaveDest 				   m_dest; 
    private float[]					   m_animCtr; 
 
	private static AdProvider 		   m_adProvider; 
	private int 				       m_currentLevel;
	private AskUserWindow			   m_askUserWindow;
    
	public GameOverWindow()
	{
		resetAnim();
	    m_adProvider = new AdProvider(); 
		m_adProvider.RequestVideoAd(Constants.REWARD_VIDEO_AD_CONT,VideoAdType.RewardToContinue);
		 
		m_askUserWindow = new AskUserWindow(QUESTION.CONTINUE_GAME);
	}
	
	State getState()
	{
		return m_state;
	}
	
	void GoToIdle()
	{
		m_state = State.IDLE;
	}
	
	public void Show(int p_level,boolean p_canContinue)
	{
		m_state = State.ENTRY;
		m_currentLevel=p_level; 
		resetAnim(); 
		
		if(m_adProvider.isVideoAdAvailable()&&p_canContinue)
		{
			m_state         = State.CONTINUE_ASK;
			m_askUserWindow = new AskUserWindow(QUESTION.CONTINUE_GAME);
		} 
	}
	
	@Override
	public void Paint(Graphics g) { 
		switch(m_state)
		{
			case ENTRY:         PaintEntry(g);             break;
			case IDLE:          PaintIdle(g);              break; 
			case RESTART:       PaintEntry(g);             break;
			case CONTINUE_ASK:  m_askUserWindow.Paint(g);  break; 
			case LEAVE:  	    PaintLeave(g);             break;
			case AD_OPENLOAD:   PaintIdle(g);              break;
			default: break;
		}
	}
	
	private void PaintLeave(Graphics g)
	{
		PaintIdle(g);
	 	g.drawRect(new Rect(0, 0,  CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT), ColorUtil.SetOpacity(m_animCtr[0], Color.WHITE)); 
	}
	 
	 
	public void PaintIdle(Graphics g)
	{ 	   
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb(30, 0, 0, 0)); 
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb(170, 255, 255, 255));  
		
		genSet.setTextProperties(Constants.GAME_OVER_LS, Color.argb(25, 0, 0, 0), Align.CENTER);  
		g.drawStringFont(W.GAME_OVER, CONFIG.SCREEN_MID+5, Constants.ScaleY(0.3203125f)+5, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(Constants.GAME_OVER_LS,   Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.GAME_OVER, CONFIG.SCREEN_MID,   Constants.ScaleY(0.3203125f), AssetManager.Comforta,genSet.paint);  
		  
		drawRoundRect(g,CONFIG.SCREEN_MID-Constants.RESTART_BS, Constants.ScaleY(0.5078125f)-Constants.RESTART_BS*2,  Constants.RESTART_BS*2, Constants.RESTART_BS*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), RESTART_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+6, Constants.ScaleY(0.5078125f)+8,Constants.RESTART_BS, Color.argb(20, 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.5078125f),   Constants.RESTART_BS, RESTART_BTN==buttonPressed?GameScreen.GetThemeColorLight():GameScreen.GetThemeColor()); 
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -75, Constants.ScaleY(0.5078125f)-150/2, 427,191,150,150);//'restart' icon

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f)+4,SUB_BTN_DIM_1, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.703125f),  SUB_BTN_DIM_1, HOME_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight()); 
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID-87/2, Constants.ScaleY(0.703125f)-70/2,  9,463,87,70); // 'home' icon 

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SEL_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+4,SUB_BTN_DIM_2, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,    Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13),  SUB_BTN_DIM_2, LEVEL_SEL_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight());  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -78/2, Constants.ScaleY(0.703125f)+(SUB_BTN_DIM_1*2+13)-75/2 ,  102,459,78,75); // 'levels' icon 
 
	}
	
	public void PaintEntry(Graphics g)
	{       
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb((int)(30*m_animCtr[4]), 0, 0, 0)); 
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb((int)(170*m_animCtr[4]), 255, 255, 255));  
		 
		int l_textSize = (int)(Constants.GAME_OVER_LS*m_animCtr[1]);
		int l_adjust_y = (int)(100*(1-m_animCtr[1]));
		genSet.setTextProperties(l_textSize, Color.argb(25, 0, 0, 0), Align.CENTER);  
		g.drawStringFont(W.GAME_OVER, CONFIG.SCREEN_MID+5,  Constants.ScaleY(0.3203125f)+5+l_adjust_y, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(l_textSize, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.GAME_OVER, CONFIG.SCREEN_MID, Constants.ScaleY(0.3203125f)+l_adjust_y, AssetManager.Comforta,genSet.paint);  
		 
		int additional_y = (int)((1-m_animCtr[3])*CONFIG.SCREEN_HEIGHT); 
		g.drawCircle(CONFIG.SCREEN_MID+6, Constants.ScaleY(0.5078125f)+8 + additional_y,Constants.RESTART_BS, Color.argb(20, 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.5078125f)+ additional_y,   Constants.RESTART_BS, RESTART_BTN==buttonPressed?GameScreen.GetThemeColorLight():GameScreen.GetThemeColor());
 		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -75, Constants.ScaleY(0.5078125f)-150/2+ additional_y, 427,191,150,150);//'restart' icon

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1, Constants.ScaleY(0.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f)+4+ additional_y,SUB_BTN_DIM_1, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.703125f)+ additional_y,  SUB_BTN_DIM_1, HOME_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight()); 
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID-87/2, Constants.ScaleY(0.703125f)-70/2+ additional_y,  9,463,87,70); // 'home' icon 

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1, Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SEL_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+4+ additional_y,SUB_BTN_DIM_2, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+ additional_y,  SUB_BTN_DIM_2, LEVEL_SEL_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight());  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -78/2, Constants.ScaleY(0.703125f)+(SUB_BTN_DIM_1*2+13)-75/2 + additional_y,  102,459,78,75); // 'levels' icon 
	}
	
 
	public void SetState(State p_state)
	{
		m_state= p_state; 
	}
	
	private void resetAnim()
	{
		m_animCtr = new float[ANIM_CTR_SIZE];
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx]=0;
		}
	}
	
	@Override
	public void Updates(float p_deltaTime)
	{
		switch(m_state)
		{
			case IDLE: break;  
			case ENTRY:        GameOverEntryUpdates(p_deltaTime);   break;
			case LEAVE:        LeaveUpdates(p_deltaTime);	        break;
			case RESTART:      GameOverRestartUpdates(p_deltaTime); break;
			case CONTINUE_ASK: ContinueAskUpdates(p_deltaTime);     break;
			default: 	break;
		} 
	} 
	
	@Override
	public void TouchUpdates(TouchEvent g)
	{
		switch(m_state)
		{ 
			case IDLE:        GameOverButtonUpdates(g);         break;
			case CONTINUE_ASK: m_askUserWindow.TouchUpdates(g); break;
			case ENTRY: break;
			case LEAVE: break;
			default:break;
		}
	}
	
	private void ContinueAskUpdates(float p_deltaTime)
	{
		m_askUserWindow.Updates(p_deltaTime);
		if(m_askUserWindow.isClosed())
		{
			m_state = State.ENTRY;
			resetAnim(); 
		}
	}
	
	public  static void ContinueGame()
	{
		m_adProvider.ShowVideoAds(); 
	}
	
 
	private void LeaveUpdates(float p_deltaTime)
	{ 
		//0 - Fade in white
		//1 - Stay sa white
		
		if(m_animCtr[0]<1)
		{
			m_animCtr[0] += 0.05f*p_deltaTime;
			if(m_animCtr[0]>=1)
				m_animCtr[0]=1;
		}
		if(m_animCtr[0]>=1.0f)
		{
			if(m_animCtr[1]<1)
			{
				m_animCtr[1]  += 0.05f *p_deltaTime;
				
				if(m_animCtr[1]>=1)
				{ 
					switch(m_dest)
					{ 
						case LEVEL_SELECT:GameScreen.GoToLevelSelect(m_currentLevel);break;
						case MENU: GameScreen.GoToMenu();break;
					}
				}
			}
		} 
	}
	
	private void GameOverEntryUpdates(float p_deltaTime)
	{  
		// 1 - Expand ng 'Stage Complete'/Show ng buttons 
		// 3 - 
		// 4 - fade in ng white 
		  if(m_animCtr[1]<1)
		{
			m_animCtr[1] += 0.032f*p_deltaTime;
			if(m_animCtr[1]>=1)
			{
				m_animCtr[1]=1;  
			}  
		}
		  
		if(m_animCtr[3]<=1)
		{ 
			m_animCtr[3] += 0.025f*p_deltaTime;
			if(m_animCtr[3]>=1)
			{
				m_animCtr[3]=1;  
			}  
		} 
		if(m_animCtr[3]>=0.4f)
		{ 
			m_animCtr[4] += 0.02f*p_deltaTime;
			if(m_animCtr[4]>=1)
			{
				m_animCtr[4]=1;   
				m_state            = State.IDLE;
			}  
		}  
	}
	
	private void GameOverRestartUpdates(float p_deltaTime)
	{  
		// 1 - Expand ng 'Stage Complete'/Show ng buttons 
		// 3 - 
		// 4 - fade in ng white
		// 5 - Back Btn
		  if(m_animCtr[1]>0)
		{
			m_animCtr[1] -= 0.042f*p_deltaTime;
			if(m_animCtr[1]<0)
			{
				m_animCtr[1]=0;  
			}  
		}
		  
		if(m_animCtr[3]>0)
		{ 
			m_animCtr[3] -= 0.035f*p_deltaTime;
			if(m_animCtr[3]<=0)
			{
				m_animCtr[3]=0;  
			}  
		} 
		if(m_animCtr[3]<=0.6f)
		{ 
			m_animCtr[4] -= 0.03f*p_deltaTime;
			if(m_animCtr[4]<0)
			{
				m_animCtr[4]=0;   
				GameScreen.RestartGame();
			}  
		}  
	}
	 
	public void GameOverButtonUpdates(TouchEvent g) 
	{
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g,CONFIG.SCREEN_MID-Constants.RESTART_BS, Constants.ScaleY(0.5078125f)-Constants.RESTART_BS*2,  Constants.RESTART_BS*2, Constants.RESTART_BS*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), RESTART_BTN);
			drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(255, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
			drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(255, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SEL_BTN);
		 
		}
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case AD_BTN:     
					m_adProvider.SendClick();
					m_state = State.AD_OPENLOAD; 
				    genSet.game.ShowDialog("Loading. Please wait...");
					buttonPressed = 0;
					break;
				case HOME_BTN:     LeaveWindow(LeaveDest.MENU);         break; 
				case RESTART_BTN:  Restart(); break; 
				case CONTINUE_BTN: break;	 
				case LEVEL_SEL_BTN:LeaveWindow(LeaveDest.LEVEL_SELECT); break; 
			}
			
			buttonPressed=0;
		} 
		else if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.click.play(1.0f);
		}
	} 

	private void Restart()
	{ 
		genSet.game.HideBannerAd();
		SetState(State.RESTART); 
		m_animCtr[1]=1;  
		m_animCtr[3]=1;  
		m_animCtr[4]=1;  
		m_animCtr[5]=1;   
	}
	private void LeaveWindow(LeaveDest p_leaveDest)
	{ 
		m_dest  = p_leaveDest;
		m_state = State.LEAVE;
		resetAnim();
	}
	
	@Override
	public void backButton()
	{
		switch(m_state)
		{
			case CONTINUE_ASK:  
				m_state = State.ENTRY;
				resetAnim(); 
				break;
			case ENTRY: break;
			case IDLE: LeaveWindow(LeaveDest.LEVEL_SELECT);break;
			case LEAVE: break;
			case AD_OPENLOAD:  
				m_state = State.IDLE; 
			    genSet.game.HideDialog();
				break;
			default: break;
		}
	} 
}
