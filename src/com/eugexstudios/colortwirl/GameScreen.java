package com.eugexstudios.colortwirl;

/*EugeX framework
 *April 12, 2015
 *Eugene C. Caranto
 * **/
//


/*
 * PROJECT NAME: COLOR WHEEL
 * DATE STARTED: 02/27/2018 8:15pm
 * 
 * 
 * */
 

import java.util.List;   
import java.util.Vector;
 
import android.graphics.Color;
import android.graphics.Paint;    
import android.graphics.Paint.Align; 

import com.eugexstudios.colortwirl.levels.Level;
import com.eugexstudios.colortwirl.levels.LevelManager; 
import com.eugexstudios.framework.Game;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Image;
import com.eugexstudios.framework.Screen;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.EugexStudiosSplash; 
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet; 

public class GameScreen extends Screen {
	enum androidState {
		Ready, Running, Paused, GameOver
	}

	public static enum LANGUAGE 
	{
		  ENGLISH (0)
		, JAPANESE (1)
		, SPANISH (2)
		, GERMAN(3)
		, KOREAN(4)
		, PORTUGUESE (5);
		
		private final int idx;
	    private LANGUAGE(int p_idx)
	    {
	        this.idx = p_idx;
	    }

	    public int getIdx() 
	    {
	        return idx;
	    }
	}
  
	public enum AppState {
		onSplashscreen, onMenu, onPlay,   onLevelSelect, SHOW_EXIT_AD,  stateSlot_7, stateSlot_8,
	}
	androidState state = androidState.Ready;
	
	private static EugexStudiosSplash  m_eugexSplashScreen;
	private static MenuScreen		   m_menuScreen;
	private static PlayScreen 		   m_playScreen; 
	private static LevelSelectScreen   	   m_levelSelect;
	private static AppState 		   m_currentState;  
	private static int 			  	   m_currentLevel;  
	private static boolean			   m_vibrateOn;
	private static boolean			   m_soundOn;
	private static boolean			   m_musicOn;
	private static LANGUAGE 		   m_lang; 
	private static int     			   m_gamesPlayed;
	private static boolean     		   m_AdFailedOnLast;
	
	public GameScreen(Game game) 
	{ 
		super(game);  
		genSet.paint     = new Paint(); 
		genSet.game      = game;  
		m_gamesPlayed    = 0;
		m_AdFailedOnLast = false; 
	    m_background     = new background();
 
 		if( game.GetSharedPrefBoolean(SharedPref.HAS_GAME_DATA)==false)
 		{ 
 			SetPrefDataForInitInstall();
 		}
 		else
 		{  
 			LoadPrefData();  
		    genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, genSet.game.GetSharedPrefInt(SharedPref.APP_OPENED_CTR)+1);
		    if(!genSet.game.GetVersionName().equals(genSet.game.GetSharedPrefString(SharedPref.LAST_GAME_VERSION )))
		    { 
			    genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION, genSet.game.GetVersionName()); 
//			    genSet.ShowToast("Version Changed");
		    }   
 		}  
 		
 		 if(Debugger.has_no_splash && !Debugger.DEMO_MODE)
 		 {
 	 		GoToMenu(); 
 	 	 }
 		 else
 		 {
 			 GoToSplashScreen();
 		 }

		if(Debugger.play_immediate && !Debugger.DEMO_MODE)
		{ 
			SetState(AppState.onPlay); 
			m_currentLevel=  genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA) ;
			m_playScreen = new PlayScreen(LevelManager.GetLevel(m_currentLevel),(short)m_currentLevel); 
			if(LevelManager.GetLevel(m_currentLevel).hasThemeOveride()){ 
			m_background.setTheme(LevelManager.GetLevel(m_currentLevel).getTheme());  
			}
		}    
		if(Debugger.win_immediate && !Debugger.DEMO_MODE)
		{  
			SetState(AppState.onPlay); 
			m_currentLevel=  (genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA)) ;
			m_playScreen = new PlayScreen(LevelManager.GetLevel(m_currentLevel),(short)m_currentLevel); 
			
		}    
//		genSet.ShowToast(CONFIG.SCREEN_WIDTH + "," + CONFIG.SCREEN_HEIGHT + ": " + Constants.m_reso.toString()); 

	}
	 
	 
	int MY_PERMISSIONS_REQUEST_READ_CONTACTS;
	
	
	public static int GetLevelPlaying()
	{
		return m_currentLevel;
	}
	
	public static int GetLevelReached()
	{
		return genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA);
	}
	
	public static void SetPrefDataForInitInstall()
	{ 
		genSet.game.SetSharedPrefBoolean(SharedPref.HAS_GAME_DATA, true);  
		genSet.game.SetSharedPrefInt(SharedPref.APP_OPENED_CTR, 1); 
		genSet.game.SetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA, 1); 
		genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON, true);
		genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON, true); 
		genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON, true);  
		genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED,  LANGUAGE.ENGLISH.getIdx());  
	 	genSet.game.SetSharedPrefString(SharedPref.LAST_GAME_VERSION,  genSet.game.GetVersionName());  
		 
		for(int idx=0;idx<LevelManager.GetMaxLevel();idx++)
		{ 
			genSet.game.SetSharedPrefInt(Constants.BETA_LVL_TRY_+ (idx+1), 0); 
		}
		
		LoadPrefData(); 
	} 
	
	private static void LoadPrefData()
	{
		m_currentLevel = genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA);
		m_soundOn      = genSet.game.GetSharedPrefBoolean(SharedPref.SOUND_ON);
		m_musicOn      = genSet.game.GetSharedPrefBoolean(SharedPref.MUSIC_ON);
		m_vibrateOn    = genSet.game.GetSharedPrefBoolean(SharedPref.VIBRATE_ON);   
		m_lang         = LANGUAGE.values()[genSet.game.GetSharedPrefInt(SharedPref.LANGUAGE_USED)]; 	 
	}
	
	public static void RateApp()
	{ 		
		genSet.game.RateApp();
	} 
	
	public static void SetLanguage(LANGUAGE p_lang)
	{
		m_lang         = p_lang;
		genSet.game.SetSharedPrefInt(SharedPref.LANGUAGE_USED, m_lang.getIdx()); 
	}
	public static void ToggleSoundSettings()
	{ 
		m_soundOn=!m_soundOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.SOUND_ON,m_soundOn);
	} 
	public static void ToggleMusicSettings()
	{ 
		m_musicOn=!m_musicOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.MUSIC_ON,m_musicOn);
	} 
	public static void ToggleVirbrateSettings()
	{ 
		m_vibrateOn=!m_vibrateOn; 
		genSet.game.SetSharedPrefBoolean(SharedPref.VIBRATE_ON,m_vibrateOn);
	} 
	
	public static boolean IsSoundOn()
	{
		return m_soundOn;
	}
	
	public static boolean IsMusicOn()
	{
		return m_musicOn;
	}	
	
	public static boolean IsVibrateOn()
	{
		return m_vibrateOn;
	}
	
	public static LANGUAGE GetLanguage()
	{
		return m_lang; 
	}
	 
	public static String GetLanguageCode()
	{
		switch(m_lang)
		{
		case ENGLISH: return "en";
		case JAPANESE: return "ja";
		case SPANISH: return "es";
		case GERMAN: return "de";
		case KOREAN: return "ko";
		case PORTUGUESE: return "pt";
		
		}
		return "en";
	}
	public static LanguageManager.W GetLanguageT()
	{
		switch(m_lang)
		{
		case ENGLISH: return LanguageManager.W.ENGLISH;
		case JAPANESE: return LanguageManager.W.JAPANESE;
		case SPANISH: return LanguageManager.W.SPANISH;
		case GERMAN: return LanguageManager.W.GERMAN;
		case KOREAN: return LanguageManager.W.KOREAN;
		case PORTUGUESE: return LanguageManager.W.PORTUGUESE; 
		
		}
		return null;
	} 
	public static int GetThemeColor()
	{
		return m_background.GetThemeColor();
	}	
	public static int GetThemeColorLight()
	{
		return m_background.GetThemeColorLight();
	}
	public static void GoToLevelSelect(int p_fromLvl) 
	{
		SetState(AppState.onLevelSelect);   
//		genSet.game.ShowBannerAd();
		genSet.game.HideBannerAd();
		m_levelSelect = new LevelSelectScreen(p_fromLvl);
		m_background.SwitchColor();
	}
	public static void PlayGame(int p_level) 
	{
		SetState(AppState.onPlay);
		genSet.game.HideBannerAd(); 
		m_currentLevel=  p_level ;
		Level l_levelToPlay = LevelManager.GetLevel(m_currentLevel);
		m_playScreen = new PlayScreen(l_levelToPlay,(short)m_currentLevel);
		if(Debugger.level_design&&!Debugger.DEMO_MODE)
		{ 
			genSet.game.SetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA,m_currentLevel) ;
		} 
		if(l_levelToPlay.hasThemeOveride())
		{
			m_background.setTheme(l_levelToPlay.getTheme());  
		}
		else
		{
			m_background.SwitchColor();  
		}
		if(Debugger.aquires_level_prog)
		{ 
			genSet.game.SetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel), genSet.game.GetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel))+1);  
		} 
	}
	  
	public static void RestartGame() 
	{
		SetState(AppState.onPlay);  
		m_gamesPlayed++;
		boolean hasAd=m_gamesPlayed>=Constants.SHOW_AD_AFTER_N_PLAY || m_AdFailedOnLast;
		
		if(m_gamesPlayed==Constants.SHOW_AD_AFTER_N_PLAY)
		{ 
			m_gamesPlayed=0;
		}
		m_playScreen.RestartGame(false, LevelManager.GetLevel(m_currentLevel),hasAd );  
		
		if(Debugger.aquires_level_prog)
		{ 
			genSet.game.SetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel), genSet.game.GetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel))+1); 
		}
	}
	public static void AdFailed() 
	{
		m_AdFailedOnLast=true;
	}
	
	public static void AdShowed() 
	{
		m_AdFailedOnLast=false;
		m_gamesPlayed=0;
	}
	
//	public static void SaveLevelReached()
//	{ 
//		int levelReached = genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA);
//		if(m_currentLevel>levelReached)
//		{
//			genSet.game.SetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA,m_currentLevel) ;
//		}
//	}
	
	public static void UnlockNextLevel()
	{ 
		int levelReached = genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA);
		if(m_currentLevel+1>levelReached)
		{
			genSet.game.SetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA,m_currentLevel+1) ;
		}
	}
	
	public static void ResetAllData()
	{  
		SetPrefDataForInitInstall();
		genSet.ShowToast("Data successfully cleared.");
	}
	
	public static void ContinueGame()
	{   
		SetState(AppState.onPlay); 
		PlayScreen.ContinueGame();
	}
	public static void GoToNextLevel()
	{ 
		if(m_currentLevel<LevelManager.GetMaxLevel())
		{
			SetState(AppState.onPlay); 
			m_currentLevel++; 
			m_gamesPlayed++; 
			m_playScreen = new PlayScreen(LevelManager.GetLevel(m_currentLevel), (short)m_currentLevel);  
			
			if(Debugger.aquires_level_prog)
			{ 
				genSet.game.SetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel), genSet.game.GetSharedPrefInt(Constants.BETA_LVL_TRY_+ (m_currentLevel))+1); 
			}
		}
		else
		{
			genSet.ShowToast("Congratulations! You have completed all levels.");
			GoToLevelSelect(m_currentLevel);	
		}
	 }
	float m_deltatime=0; 
	float m_aveDT=0;
	Vector<float[]> m_deltaHist= new Vector<float[]>();
	 
	public void gameUpdates(float p_deltaTime) 
	{  							 
		m_deltatime= p_deltaTime; 
		 
		m_deltaHist.add(new float[]{m_deltatime});
		
		if(m_deltaHist.size()>50)
		{
			m_deltaHist.remove(0);
			float sum=0;
			for(int idx=0;idx<m_deltaHist.size();idx++)
			{
				sum+=m_deltaHist.get(idx)[0];
			}
			m_aveDT= sum/m_deltaHist.size();
		}

		if(m_currentState!=AppState.onSplashscreen)
			m_background.Updates(p_deltaTime);
		switch (m_currentState) 
			{

			case onSplashscreen: m_eugexSplashScreen.Updates(p_deltaTime);  break;
			case onMenu:        m_menuScreen.Updates(p_deltaTime); break;
			case onPlay:	    m_playScreen.Updates(p_deltaTime); 	break; 
			case onLevelSelect: m_levelSelect.Updates(p_deltaTime); break;
			default: break;
		}
	}

	private static background				   m_background;
	@Override
	public void paint(float deltaTime)
	{ 
		Graphics g = game.getGraphics(); 
		if(m_currentState!=AppState.onSplashscreen)
			m_background.Paint(g);
		switch (m_currentState)
		{
			case onSplashscreen: m_eugexSplashScreen.Paint(g);  break;
			case onMenu:	     m_menuScreen.Paint(g);    	break; 
			case onPlay:         m_playScreen.Paint(g);  break; 
			case onLevelSelect:  m_levelSelect.Paint(g); 	    break;
			case SHOW_EXIT_AD:break; 
			case stateSlot_7:break;
			case stateSlot_8:break;
		}  
 
//		if(Debugger.show_fps&&!Debugger.DEMO_MODE )
//		{      
//			genSet.setTextProperties( 35, Color.RED, Align.LEFT); 
//			g.drawString("Current:  " + m_deltatime +"" , 2, 50+36*3, genSet.paint);  
//			g.drawString("Average:  " + m_aveDT +"" , 2, 50+36*4, genSet.paint);   
//		} 
// 
//		if(Debugger.show_touches&&!Debugger.DEMO_MODE &&m_lastTouchEvent!=null)
//		{      
//			genSet.setTextProperties( 35, Color.RED, Align.LEFT);
//			g.drawString(m_lastTouchEvent.type+"", 50, 50+36*0, genSet.paint); 
//			g.drawString(m_lastTouchEvent.x + "," + m_lastTouchEvent.y, 50, 50+36*1, genSet.paint); 
//		} 

//		genSet.setTextProperties( 35, Color.RED, Align.LEFT);
//		g.drawString(genSet.game.getFPS()+"", 200,200, genSet.paint); 
	}
	TouchEvent m_lastTouchEvent;
	private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
		int len = touchEvents.size();

		for (int i = 0; i < len; i++)
		{
			TouchEvent event = touchEvents.get(i);
			switch (m_currentState) 
			{
				case onMenu: 	    m_menuScreen.TouchUpdates(event); 	     break; 
				case onPlay:        m_playScreen.TouchUpdates(event);	     break; 
				case onLevelSelect: m_levelSelect.TouchUpdates(event);	break;
				case SHOW_EXIT_AD: 	break; 
				case stateSlot_7: 	break;
				case stateSlot_8: 	break;
				default: break;  
				
			}  
			
//			if(event.type == TouchEvent.TOUCH_DOWN)
//			{
//	 			game.hideSystemUI();
//			}
//				m_postool.positioningToolTouch(event);
			m_lastTouchEvent = event;
		}
	} 
	public static void RemoveAdRequest()
	{
		
	}
	
	public static void ResumeGame()
	{
		SetState(AppState.onPlay);
	}

	public static void CloseGame() 
	{
		android.os.Process.killProcess(android.os.Process.myPid());
	} 

	public static void GoToMenu() 
	{ 
		SetState(AppState.onMenu);
		m_menuScreen = new MenuScreen(m_currentLevel);
//		genSet.game.ShowBannerAd();
		m_background.SwitchColor();
	}
	public static void GoToSplashScreen() 
	{
		SetState(AppState.onSplashscreen);
		m_eugexSplashScreen = new EugexStudiosSplash();
	}

	@Override
	public void backButton() 
	{ 
		switch (m_currentState)
		{
			case onMenu:  	        m_menuScreen.backButton();  	break;
			case onPlay:  		    m_playScreen.backButton(); break; 
			case onLevelSelect:     m_levelSelect.backButton(); break;  
			case stateSlot_7: break;
			case stateSlot_8: break;
			default: break;
		} 
	}
	 
	public static void SetState(AppState newState) 
	{ 
		m_currentState = newState;
	}	public static AppState GetState() 
	{ 
		return m_currentState ;
	}
 

	public int[] paintButton(int g, Image buttonImage, int xPos, int yPos, int sourceX, int sourceY, int l, int w, int buttonValue, int d1, int d2) {

		return new int[] { xPos, yPos, l, w, buttonValue };
	}
 
	public boolean inBounds(TouchEvent event, int x, int y, int width, int height) 
	{
		if (event.x > x && event.x < x + width - 1 && event.y > y && event.y < y + height - 1)
			return true;
		else
			return false;
	}

	@Override
	public void pause() {
		if (state == androidState.Running)
			state = androidState.Paused;

		// if(gameState!=onPlayOver)
		// pauseGame();

	}

	@Override
	public void resume() 
	{
		if (state == androidState.Paused)
			state = androidState.Running;

	}

	public void update(float deltaTime) 
	{
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		if (state == androidState.Ready)
			updateReady(touchEvents);
		if (state == androidState.Running)
			updateRunning(touchEvents, deltaTime);
		if (state == androidState.Paused)
			updateRunning(touchEvents, deltaTime);


		gameUpdates(deltaTime);
	}

	private void updateReady(List<TouchEvent> touchEvents)
	{
		if (touchEvents.size() > 0)
			state = androidState.Running;
	} 
	
	@Override
	public void dispose()
	{ 
		game.showToast("dispose!"); 
	}
}