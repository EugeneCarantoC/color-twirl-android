package com.eugexstudios.colortwirl;
 
import java.util.Random;

import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Paint.Align;
     

import com.eugexstudios.adprovider.AdProvider;
import com.eugexstudios.adprovider.StartApp.VideoAdType;
import com.eugexstudios.colortwirl.AskUserWindow.QUESTION;  
import com.eugexstudios.colortwirl.levels.LevelManager;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;

public class LevelCompleteWindow extends Screen{

	private final int SUB_BTN_DIM_1 =(int)(CONFIG.SCREEN_WIDTH*0.09f); //(int)(CONFIG.SCREEN_WIDTH*0.15f);
	private final int SUB_BTN_DIM_2 =(int)(CONFIG.SCREEN_WIDTH*0.08f); //(int)(CONFIG.SCREEN_WIDTH*0.15f);
	
	private final byte    
	  BACK_BTN	   = 5 
	, GO_TO_NEXT_LVL_BTN =6
	, HOME_BTN =7
	, LEVEL_SELECT_BTN =8
	, SETTINGS_BTN =9
	;
	 
	private final int ANIM_CTR_SIZE=7;
	
	private enum LeaveDest {
	 	  NEXT_LEVEL
		, LEVEL_SELECT
		, MENU
		, SETTINGS 
	};
	
	public enum State 
	{
		  ENTRY
		, IDLE
		, ASK_TO_RATE
		, SETTINGS
		, LEAVE
		, COMPLETE
	};
	private State 				       m_state;
	private LeaveDest 				   m_dest;
    private float[]					   m_animCtr; 
    private Dot						   m_exploderDot;
    private int[] 			 		   m_explodeColors;
    private SettingsWindow			   m_settingsWindow; 
    private AskUserWindow		   	   m_askAskUserWindow;
    private boolean					   m_askedForFeedback;
    private boolean					   m_videoAdShown;
	private AdProvider 				   m_adProvider; 
	private Random					   m_randomer;
	private boolean 			       m_completeShown;
	private boolean 			       m_completePlayed;
	
	public State getState()
	{
		return m_state;
	}
	public LevelCompleteWindow(Dot p_exploderDot,int[] p_explodeColors)
	{
		m_state            = State.ENTRY;
		m_randomer	       = new Random();
		m_explodeColors    = p_explodeColors.clone();
		m_settingsWindow   = new SettingsWindow();
		m_exploderDot      = p_exploderDot;  
		m_askedForFeedback = false;
		m_completePlayed   =false;
		m_exploderDot.SetPosition(Constants.WHEEL_POSITION);  
		m_adProvider       = new AdProvider();
		m_adProvider.RequestVideoAd(Constants.VIDEO_AD_LVLCOMP,VideoAdType.NextLevel);
		m_adProvider.RequestOfferWallAd();
		m_videoAdShown    = false;
		m_completeShown   = false;
		resetAnim();
		 
	}
	@Override
	public void Paint(Graphics g) 
	{ 
		switch(m_state)
		{	
			case ASK_TO_RATE: 
				PaintIdle(g);
				m_askAskUserWindow.Paint(g);
				PaintManualBackBtn(g);
				break;
			case ENTRY:    PaintEntry(g); break;
			case IDLE:     PaintIdle(g);  break;
			case SETTINGS: m_settingsWindow.Paint(g);break;	   
			case LEAVE:    PaintLeave(g); break; 
			case COMPLETE: PaintAllLevelComplete(g); break; 
			
		} 
	}

	@Override
	public void Updates(float p_deltaTime) 
	{
		switch(m_state)
		{	
			case COMPLETE:
				if(m_askAskUserWindow.isClosed())
				{ 
					m_state= State.IDLE;
				} 
			break;
			case ASK_TO_RATE:
				if(m_askAskUserWindow.isClosed())
				{ 
					m_state= State.IDLE;
				}
				break;
		
			case ENTRY: 
				EntryUpdates(p_deltaTime);
				m_exploderDot.UpdateShatter(p_deltaTime);
			break;
			case IDLE: 
				if(GameScreen.GetLevelPlaying()>=LevelManager.GetMaxLevel() && m_completeShown==false)
				{
					m_state= State.COMPLETE;
					m_completeShown=true;
					m_askAskUserWindow = new AskUserWindow(QUESTION.CONGRATS_NOTIF);
//					m_askAskUserWindow.SetQuestionType(QUESTION.CONGRATS_NOTIF);
				}
				break;
			case LEAVE:
				// 0 - Fade in White
				// 1 - Pause ng white
				if(m_animCtr[0]<1)
				{
					m_animCtr[0] += 0.04f*p_deltaTime;
					if(m_animCtr[0]>=1)
					{ 
						m_animCtr[0]=1;
					} 
				} 
				else
				if(m_animCtr[1]<1)
				{
					m_animCtr[1] += 0.07f*p_deltaTime;
					if(m_animCtr[1]>=1)
					{  
						switch(m_dest)
						{
							case MENU:		   GameScreen.GoToMenu();	     break;
							case LEVEL_SELECT: GameScreen.GoToLevelSelect(GameScreen.GetLevelPlaying()); break;
							case NEXT_LEVEL:   GameScreen.GoToNextLevel();    break; 
							case SETTINGS:     GoToSettings();    break; 
							
						} 
					} 
				} 
				break;

			case SETTINGS:  m_settingsWindow.Updates(p_deltaTime);    break;
		default:
			break; 
		}  
		 
	}
	private void EntryUpdates(float p_deltaTime)
	{
		// 6 - Delay before anim ng lahat
		// 0 - Delay dahil sa shrink ng wheel
		// 1 - Expand ng 'Stage Complete'/Show ng buttons
		// 2 - Expand/Fading away ng text explosion/ 
		// 3 - 
		// 4 - fade in ng white
		// 5 - Back Btn
		if(m_animCtr[6]<1)
		{
			m_animCtr[6] += 0.1f*p_deltaTime;
			if(m_animCtr[6]>=1)
			{
				m_animCtr[6]=1;    
			}  
		}
		else if(m_animCtr[0]<1)
		{
			m_animCtr[0] += 0.03f*p_deltaTime;
			if(m_animCtr[0]>=1)
			{
				m_animCtr[0]=1; 
				m_exploderDot.ShatterFinish(m_explodeColors);    
				if(m_completePlayed==false)
				{
					AssetManager.level_complete.play();	
					m_completePlayed=true;
				}
			}  
		} 
		else if(m_animCtr[0]>=1 && m_animCtr[1]<1)
		{ 
			m_animCtr[1] += 0.032f*p_deltaTime;
			if(m_animCtr[1]>=1)
			{
				m_animCtr[1]=1;  
			}  
		}
		 
		if(m_animCtr[1]>=1)
		{ 
			m_animCtr[2] += 0.015f*p_deltaTime;
			if(m_animCtr[2]>=1)
			{
				m_animCtr[2]=1;  
			}  
		} 
		
		if(m_animCtr[0]>=0.7f)
		{ 
			m_animCtr[3] += 0.025f*p_deltaTime;
			if(m_animCtr[3]>=1)
			{
				m_animCtr[3]=1;  
			}  
		} 
		if(m_animCtr[3]>=1)
		{ 
			m_animCtr[4] += 0.02f*p_deltaTime;
			if(m_animCtr[4]>=1)
			{
				m_animCtr[4]=1;   
				m_state            = State.IDLE;
			}  
		} 
		if(m_animCtr[4]>=0.5f)
		{ 
			m_animCtr[5] += 0.05f*p_deltaTime;
			if(m_animCtr[5]>=1)
			{
				m_animCtr[5]=1;  
			}  
		} 
	}
	private void GoToSettings()
	{
		m_state = State.SETTINGS;
		m_settingsWindow = new SettingsWindow();  
	}
	@Override
	public void TouchUpdates(TouchEvent g)
	{  
		switch(m_state)
		{	
			case ASK_TO_RATE: 
				m_askAskUserWindow.TouchUpdates(g);
				drawRoundRect(g, 70-50, 70-100, 100, 100, Color.WHITE, Color.rgb(200, 200, 200), BACK_BTN);     
				
				if(g.type == TouchEvent.TOUCH_UP)
				{
					switch(buttonPressed)
					{ 
						case BACK_BTN: m_state = State.IDLE; break;
					} 
				}
				break; 

			case ENTRY:  
			case IDLE:     LevelCompleteBtnUpdates(g);  break;
			case SETTINGS:
				
				m_settingsWindow.TouchUpdates(g);
				if(m_settingsWindow.isVisib()==false)
				{
					m_state = State.IDLE;
				}
				
				break;
			case LEAVE: break;
			case COMPLETE: m_askAskUserWindow.TouchUpdates(g); break;
		default:
			break; 
		}  
		

		if(g.type == TouchEvent.TOUCH_DOWN &&buttonPressed!=0)  
			AssetManager.click.play(1.0f);
	}

	@Override
	public void backButton() 
	{
		switch(m_state)
		{	
			case COMPLETE: m_state = State.IDLE;break;
			case ENTRY: break;
			case ASK_TO_RATE:  	m_state = State.IDLE;break;
			case IDLE:  GameScreen.GoToMenu();break;
			case SETTINGS:
				if(m_settingsWindow.isIdled()) 
					m_state = State.IDLE;
				else
					m_settingsWindow.backButton(); break;
			case LEAVE: break; 
		}  
	} 
	public void PaintAllLevelComplete(Graphics g)
	{   
		PaintIdle(g); 
		m_askAskUserWindow.Paint(g);	 
	}
	public void PaintIdle(Graphics g)
	{   
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb(15, 0, 0, 0)); 
		g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), Color.argb(130, 255, 255, 255)); 
		PaintManualBackBtn(g);

		genSet.setTextProperties(Constants.STAGE_COMP_LS, Color.argb(30, 0, 0, 0), Align.CENTER);  
		g.drawStringFont(W.STAGE_COMPLETE, CONFIG.SCREEN_MID+5, Constants.ScaleY(0.3203125f)+5, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(Constants.STAGE_COMP_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.STAGE_COMPLETE, CONFIG.SCREEN_MID,  Constants.ScaleY(0.3203125f), AssetManager.Comforta,genSet.paint);  
		 
		drawRoundRect(g,CONFIG.SCREEN_MID-Constants.RESTART_BS, Constants.ScaleY(0.5078125f)-Constants.RESTART_BS*2,  Constants.RESTART_BS*2, Constants.RESTART_BS*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), GO_TO_NEXT_LVL_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+6,      Constants.ScaleY(0.5078125f)+8,      Constants.RESTART_BS, Color.argb(15, 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,        Constants.ScaleY(0.5078125f),        Constants.RESTART_BS, GO_TO_NEXT_LVL_BTN==buttonPressed?GameScreen.GetThemeColorLight():GameScreen.GetThemeColor());
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -210/2, Constants.ScaleY(0.5078125f)-120/2,  130,324,210,120); // 'play' icon

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f)+4,SUB_BTN_DIM_1, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,    Constants.ScaleY(0.703125f),  SUB_BTN_DIM_1, HOME_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight()); 
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID-87/2,  Constants.ScaleY(0.703125f)-70/2,  9,463,87,70); // 'home' icon 

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SELECT_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+4,SUB_BTN_DIM_2, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,    Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13),  SUB_BTN_DIM_2, LEVEL_SELECT_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight());  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -78/2,  Constants.ScaleY(0.703125f)+(SUB_BTN_DIM_1*2+13)-75/2 ,  102,459,78,75); // 'levels' icon 
 	
	}
	
 	
	public void PaintEntry(Graphics g)
	{      
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(15*m_animCtr[4]), 0, 0, 0)); 
		g.drawRect(Constants.SCREEN_RECT, Color.argb((int)(130*m_animCtr[4]), 255, 255, 255)); 
		PaintManualBackBtn(g,m_animCtr[5]); 
		 
		if(m_animCtr[0]>0)
		{
			int l_textSize = (int)(Constants.STAGE_COMP_LS*m_animCtr[1]);
			int l_adjust_y = (int)(100*(1-m_animCtr[1]));
			genSet.setTextProperties(l_textSize, Color.argb(25, 0, 0, 0), Align.CENTER);  
			g.drawStringFont(W.STAGE_COMPLETE, CONFIG.SCREEN_MID+5,  Constants.ScaleY(0.3203125f)+5+l_adjust_y, AssetManager.Comforta,genSet.paint);  
			genSet.setTextProperties(l_textSize, Color.DKGRAY, Align.CENTER);  
			g.drawStringFont(W.STAGE_COMPLETE, CONFIG.SCREEN_MID, Constants.ScaleY(0.3203125f)+l_adjust_y, AssetManager.Comforta,genSet.paint);  
		}
		
		int additional_y = (int)((1-m_animCtr[3])*CONFIG.SCREEN_HEIGHT);
		drawRoundRect(g, CONFIG.SCREEN_MID-Constants.RESTART_BS ,  Constants.ScaleY(0.5078125f)-Constants.RESTART_BS*2,  Constants.RESTART_BS*2, Constants.RESTART_BS*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), GO_TO_NEXT_LVL_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+6, Constants.ScaleY(0.5078125f)+8 + additional_y,Constants.RESTART_BS, Color.argb(15, 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.5078125f)+ additional_y,   Constants.RESTART_BS, GO_TO_NEXT_LVL_BTN==buttonPressed?GameScreen.GetThemeColorLight():GameScreen.GetThemeColor());
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -210/2, Constants.ScaleY(0.5078125f)-120/2+ additional_y,  130,324,210,120); // 'play' icon

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1, Constants.ScaleY(00.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f)+4+ additional_y,SUB_BTN_DIM_1, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.703125f)+ additional_y,  SUB_BTN_DIM_1, HOME_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight()); 
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID-87/2, Constants.ScaleY(0.703125f)-70/2+ additional_y,  9,463,87,70); // 'home' icon 

		drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1, Constants.ScaleY(00.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SELECT_BTN);
		g.drawCircle(CONFIG.SCREEN_MID+4, Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+4+ additional_y,SUB_BTN_DIM_2, Color.argb(20, 0, 0, 0));  
		g.drawCircle(CONFIG.SCREEN_MID,   Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)+ additional_y,  SUB_BTN_DIM_2, LEVEL_SELECT_BTN==buttonPressed?GameScreen.GetThemeColor():GameScreen.GetThemeColorLight());  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_MID -78/2, Constants.ScaleY(0.703125f)+(SUB_BTN_DIM_1*2+13)-75/2 + additional_y,  102,459,78,75); // 'levels' icon 
		
		if(m_animCtr[2]>0)
		{
			int l_bigTextSize = Constants.STAGE_COMP_LS+(int)(((Constants.STAGE_COMP_LS*2.5f)*m_animCtr[2])); 
			genSet.setTextProperties(l_bigTextSize, ColorUtil.SetOpacity(1-m_animCtr[2], Color.GRAY), Align.CENTER);  
			g.drawStringFont(W.STAGE_COMPLETE, CONFIG.SCREEN_MID, Constants.ScaleY(0.3203125f)+l_bigTextSize/6, AssetManager.Comforta,genSet.paint);  
		}
	}
	public void PaintManualBackBtn(Graphics g)
	{
		g.drawCircle(70+(buttonPressed==BACK_BTN?4:0),70+(buttonPressed==BACK_BTN?4:0),50,  GameScreen.GetThemeColor());  
    
		genSet.setTextProperties( 65, Color.WHITE, Align.CENTER);   
		g.drawStringFont("<",70+(buttonPressed==BACK_BTN?4:0),93+ (buttonPressed==BACK_BTN?4:0), AssetManager.Comforta,genSet.paint);  
	} 
	
	 public void PaintManualBackBtn(Graphics g, float p_scale)
	{
		g.drawCircle(70+(buttonPressed==BACK_BTN?4:0),70+(buttonPressed==BACK_BTN?4:0),(int)(50*p_scale),  GameScreen.GetThemeColor());  
    
		genSet.setTextProperties( (int)(65*p_scale), Color.WHITE, Align.CENTER);   
		g.drawStringFont("<",70+(buttonPressed==BACK_BTN?4:0),93+ (buttonPressed==BACK_BTN?4:0), AssetManager.Comforta, genSet.paint);  
	} 
	 
	public void PaintLeave(Graphics g)
	{ 
		PaintIdle(g);
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, ColorUtil.SetOpacity(m_animCtr[0], Color.WHITE));    
	}
	private void LeaveWindow(LeaveDest p_leaveDest)
	{
		m_dest = p_leaveDest;
		m_state = State.LEAVE;
		resetAnim();
	}
	private void resetAnim()
	{
		m_animCtr = new float[ANIM_CTR_SIZE];
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx]=0;
		}
	}
	 
	public void LevelCompleteBtnUpdates(TouchEvent g) 
	{  		    
		if(g.type == TouchEvent.TOUCH_DOWN)
		{   
			drawRoundRect(g, 70-50, 70-100, 100, 100, Color.WHITE, Color.rgb(200, 200, 200), BACK_BTN);   
			drawRoundRect(g, CONFIG.SCREEN_MID-Constants.RESTART_BS, 650-Constants.RESTART_BS*2,  Constants.RESTART_BS*2, Constants.RESTART_BS*2, Color.argb(100, 0, 0, 0), Color.argb(0, 0, 0, 0), GO_TO_NEXT_LVL_BTN);
			drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), HOME_BTN);
			drawRoundRect(g, CONFIG.SCREEN_MID-SUB_BTN_DIM_1,  Constants.ScaleY(0.703125f) +(SUB_BTN_DIM_1*2+13)-SUB_BTN_DIM_1*2,  SUB_BTN_DIM_1*2, SUB_BTN_DIM_1*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0, 0), LEVEL_SELECT_BTN);	
		}
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case SETTINGS_BTN:	     LeaveWindow(LeaveDest.SETTINGS);     break; 
				case HOME_BTN:           LeaveWindow(LeaveDest.MENU);         break; 
				case LEVEL_SELECT_BTN:   LeaveWindow(LeaveDest.LEVEL_SELECT); break; 
				case GO_TO_NEXT_LVL_BTN:
					
					
					boolean willAskToRate =((GameScreen.GetLevelPlaying()==4 || GameScreen.GetLevelPlaying()==11) && m_askedForFeedback==false);
					
					if(willAskToRate)
					{
						m_askedForFeedback=true;
						m_state = State.ASK_TO_RATE;
						m_askAskUserWindow = new AskUserWindow(QUESTION.GET_STARS);
					}
					else
					{  
						if(GameScreen.GetLevelPlaying()%2==0 && m_adProvider.isOfferWallReady())
						{
							m_adProvider.ShowOfferWall(
									new AdDisplayListener() {
									    @Override
									    public void adHidden(Ad ad) {
									    	GameScreen.GoToNextLevel();
									    }
									    @Override
									    public void adDisplayed(Ad ad) {
									    }
									    @Override
									    public void adClicked(Ad ad) {
									    	GameScreen.GoToNextLevel();
									    }
									    @Override
									    public void adNotDisplayed(Ad ad) {
									    }
									}
									);
						}
						else if(m_adProvider.isVideoAdAvailable() && m_randomer.nextInt(Constants.VIDEO_AD_CHANCE)==1 && m_videoAdShown==false)
						{
							m_adProvider.ShowVideoAds();
							m_videoAdShown=true;
						}
						else
						{ 
							LeaveWindow(LeaveDest.NEXT_LEVEL);
						}
					}
					
					break; 
				case BACK_BTN:backButton(); break;
			}
			
			if(buttonPressed!=0)
				buttonPressed=0;
		} 
		 
	} 
}
