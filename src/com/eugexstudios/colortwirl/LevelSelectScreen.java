package com.eugexstudios.colortwirl;

import android.graphics.Color; 
import android.graphics.Rect;
  
import android.graphics.Paint.Align;

import com.eugexstudios.colortwirl.levels.LevelManager;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.SharedPref;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class LevelSelectScreen extends Screen{

	private enum ANIM {
		 NONE
		,ENTER
		,EXIT
		};
		
	private enum EXIT_DEST{
		 PLAY
		,MENU
		};
		
	private final int 				   ANIM_SIZE       = 5;
	private final int                  XLIMIT          = 4; 
	private final int                  YLIMIT 		   = 4; 
	private final int			       MARGIN_X        = (int)(CONFIG.SCREEN_WIDTH*0.0763880f);
	private final int			       SELECT_LVL_DIST = (int)(CONFIG.SCREEN_WIDTH*0.02778f);
	private final int 				   SELECT_LVL_SIZE = (CONFIG.SCREEN_WIDTH-(MARGIN_X*2)-(SELECT_LVL_DIST*(XLIMIT-1)))/XLIMIT ;  
	private final int			       PAGE_BTN        = 600;
	private final byte				   BACK_BTN        = 100;

	private int 			           m_levelUnlocked;
	private boolean				       m_draggingLeft;
	private int 					   m_level_dest;
	private int		 		           m_pageLimit;
	private int 					   m_dragPosX;
	private int 			           m_dragSize;
	private EXIT_DEST 			       m_exitDest;
	private float[]					   m_animCtr;
	private float 					   m_xAnim; 
	private int 					   m_page;
	private ANIM					   m_anim; 

	private Rect c_overlay = new Rect(0
										 ,(genSet.game.IsBannerAdEnabled()?35:0)+(int)(CONFIG.SCREEN_HEIGHT*0.1484375f)
										 ,CONFIG.SCREEN_WIDTH
										 ,(genSet.game.IsBannerAdEnabled()?35:0)+(int)(CONFIG.SCREEN_HEIGHT*0.1484375f)+(int)(CONFIG.SCREEN_HEIGHT*0.75f));
	
	private Rect c_labelOverlay      = new Rect(0
										  ,(genSet.game.IsBannerAdEnabled()?35:0)+(int)(CONFIG.SCREEN_HEIGHT*0.1484375f)
										  ,CONFIG.SCREEN_WIDTH
										  ,(genSet.game.IsBannerAdEnabled()?35:0)+(int)(CONFIG.SCREEN_HEIGHT*0.1484375f)+(int)(CONFIG.SCREEN_HEIGHT*0.09375f));

	int c_buttonsPosY = (genSet.game.IsBannerAdEnabled()?25:0)+(int)(CONFIG.SCREEN_HEIGHT*0.234375f);

	int c_pageBtnPosY = c_buttonsPosY+(YLIMIT+1)*(SELECT_LVL_SIZE + SELECT_LVL_DIST);
	int c_labelPosY = (genSet.game.IsBannerAdEnabled()?35:0)+(int)(CONFIG.SCREEN_HEIGHT*0.21875f);
	
	
	public LevelSelectScreen(int p_fromLevel)
	{
		m_level_dest	= -1;
		m_dragPosX		= -1;
		m_dragSize      = 0;
		m_draggingLeft  = false;
		m_pageLimit     = LevelManager.GetMaxLevel()/(XLIMIT*YLIMIT) + (LevelManager.GetMaxLevel()%(XLIMIT*YLIMIT)>0?1:0);
		m_xAnim		    = 0;
		m_levelUnlocked = genSet.game.GetSharedPrefInt(SharedPref.LEVELS_REACHED_DATA);
		m_page          = (p_fromLevel-1)/(XLIMIT*YLIMIT); 
		SetAnimation(ANIM.ENTER);
	}
	 
	private void SetAnimation(ANIM p_anim)
	{
		m_anim    = p_anim;
		m_animCtr = new float[ANIM_SIZE];
		
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx] = 0;
		}
	}
	@Override
	public void Paint(Graphics g) 
	{
		switch(m_anim)
		{
		case NONE: PaintDefault(g);break;
		case ENTER: 
			PaintDefault(g);
			g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), ColorUtil.SetOpacity(1-m_animCtr[0], Color.WHITE)); 
			break;
		case EXIT:  
			PaintDefault(g);
			g.drawRect(new Rect(0,0,CONFIG.SCREEN_WIDTH,CONFIG.SCREEN_HEIGHT), ColorUtil.SetOpacity(m_animCtr[0], Color.WHITE)); 
			break;
		}
	}

	private void PaintDefault(Graphics g) 
	{  
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Constants.PASTELIZER_COLOR);   
		g.drawRect(c_labelOverlay, ColorUtil.SetOpacity(0.30f,  GameScreen.GetThemeColorLight())); 
		g.drawRect(c_overlay,ColorUtil.SetOpacity(0.2f,  GameScreen.GetThemeColorLight()));
		 
		genSet.setTextProperties(Constants.SELECT_STAGE_LS, Color.argb(45, 0, 0, 0), Align.CENTER);  
		g.drawStringFont(W.SELECT_STAGE, CONFIG.SCREEN_MID+5, c_labelPosY+5, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(Constants.SELECT_STAGE_LS, Color.WHITE, Align.CENTER);  
		g.drawStringFont(W.SELECT_STAGE, CONFIG.SCREEN_MID,  c_labelPosY,     AssetManager.Comforta,genSet.paint);  
		
		int l_posX =MARGIN_X;
		
		if(m_dragPosX>0)
		{
			l_posX =MARGIN_X + (m_draggingLeft?-m_dragSize:m_dragSize); 
		} 
		if(m_xAnim!=0)
		{ 
			l_posX =MARGIN_X + (int)(m_draggingLeft?-m_xAnim:m_xAnim); 
		}
		 
		genSet.setTextProperties(Constants.LEVEL_NUM_LS, Color.DKGRAY, Align.CENTER); 
		 
		PaintLevels(g, l_posX, m_page);
		
		if(m_page>0)
		{
			int posX=  l_posX - CONFIG.SCREEN_WIDTH; 
			PaintLevels(g,posX, m_page-1); 
		}
		
		if(m_page+1<m_pageLimit)
		{
			int posX = l_posX + CONFIG.SCREEN_WIDTH;
			PaintLevels(g,posX, m_page+1);  
		}

		int l_gap =20*2+25;
		for(int idx_x=0;idx_x<m_pageLimit;idx_x++)
		{
			if(idx_x==m_page)
			{
				g.drawCircle( CONFIG.SCREEN_MID + idx_x*l_gap  - (l_gap*(m_pageLimit-1))/2, c_pageBtnPosY, 25, GameScreen.GetThemeColor());
			}
			g.drawCircle( CONFIG.SCREEN_MID + idx_x*l_gap  - (l_gap*(m_pageLimit-1))/2, c_pageBtnPosY	, 20, Color.argb(50, 0, 0, 0));
		} 
		 
		PaintManualBackBtn(g); 
	}
	

	public void PaintLevels(Graphics g, int p_posX,int l_page)
	{
		for(int idx_x=0;idx_x<XLIMIT;idx_x++)
		{
			for(int idx_y=0;idx_y<YLIMIT;idx_y++)
			{
				int l_level = (l_page*(XLIMIT*YLIMIT))+ ((idx_y*XLIMIT+idx_x )+1);
				
				if(l_level<= LevelManager.GetMaxLevel())
				{
					drawRoundRect(g, p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST), SELECT_LVL_SIZE, SELECT_LVL_SIZE, GameScreen.GetThemeColorLight(),  GameScreen.GetThemeColor(),l_level); 
					drawRoundRect(g, p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST), SELECT_LVL_SIZE, SELECT_LVL_SIZE, ColorUtil.SetOpacity(0.15f,  Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY),l_level); 
					drawRoundRect(g, p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST)+3, c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST)+3, SELECT_LVL_SIZE-6, SELECT_LVL_SIZE-6,  GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(),l_level); 
					
					genSet.setTextProperties(Constants.LEVEL_NUM_LS, Color.DKGRAY, Align.CENTER); 
				
					if(l_level>=m_levelUnlocked+1)
					{ 
						drawRoundRect(g, p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST), SELECT_LVL_SIZE, SELECT_LVL_SIZE, ColorUtil.SetOpacity(0.22f,  Color.GRAY), Color.rgb(180, 180, 180),l_level); 
//						g.drawImage(AssetManager.sprites, p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST) + (SELECT_LVL_SIZE/2-72/2), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST) +(SELECT_LVL_SIZE-80/2),  176,214,72,80); // lock icon
						g.drawImage(AssetManager.sprites
								 , p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST) + (SELECT_LVL_SIZE/2)
								 , c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST) +(SELECT_LVL_SIZE-80/2)
								 ,  Constants.LOCK_IMG); // lock icon
							}
					else
					{
						g.drawStringFont( l_level+ "", p_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST)+((int)(SELECT_LVL_SIZE/2)), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST)+((int)(SELECT_LVL_SIZE+Constants.LEVEL_NUM_LS/2)), AssetManager.Comforta,genSet.paint); 
					}
				}
			}
		} 
	}
	
	public void PaintManualBackBtn(Graphics g)
	{	 
//		drawRoundRect(g, (int)(Constants.BACK_BTN_CIR*0.2f), -(int)(Constants.BACK_BTN_CIR*0.8f), (int)(Constants.BACK_BTN_CIR*1.0f),(int)( Constants.BACK_BTN_CIR*1.5f),  Color.rgb(200, 200, 200), Color.rgb(200, 200, 200), BACK_BTN);     
	    g.drawCircle(    (int)(Constants.BACK_BTN_CIR*0.2f)+(int)(Constants.BACK_BTN_CIR*0.5f),  (int)(Constants.BACK_BTN_CIR*0.2f)+ (int)(Constants.BACK_BTN_CIR*0.5f), Constants.BACK_BTN_CIR/2,   GameScreen.GetThemeColor());  
	    g.drawImage(AssetManager.sprites,(int)(Constants.BACK_BTN_CIR*0.2f),  (int)(Constants.BACK_BTN_CIR*0.2f),Constants.BACK_IMG); 
	} 
		
	@Override
	public void Updates(float p_deltaTime) 
	{ 
		switch(m_anim)
		{
		case NONE: 
			
			if(m_xAnim!=0)
			{
				float incrementor = Math.abs(m_xAnim/15) ;
				if(incrementor<0.1f)
					incrementor=0.1f;
				
				m_xAnim+=incrementor *p_deltaTime ;
			
				if(m_xAnim>=CONFIG.SCREEN_WIDTH)
				{
					m_xAnim=0;	
					if(m_draggingLeft)
					{
						m_page++;
					}
					else
					{
						m_page--;
					}
				}
			}
			
			break;
		case ENTER:   
			//0 - White overlay
			
			if(m_animCtr[0]<1)
			{
				m_animCtr[0]+=0.03f  * p_deltaTime;
	 			
	 			if(m_animCtr[0]>1)
	 			{ 
	 				this.SetAnimation(ANIM.NONE);
	 			}
			}
			
			break;
		case EXIT: 
			if(m_animCtr[0]<1)
			{
				m_animCtr[0]+=0.03f  * p_deltaTime;
	 			
	 			if(m_animCtr[0]>1)
	 			{ 
					switch(m_exitDest)
					{
					case PLAY: 	GameScreen.PlayGame(m_level_dest);break;
					case MENU: 	GameScreen.GoToMenu(); break;
					}
	 			}
			}
			break;
		}
 
	}
	@Override
	public void TouchUpdates(TouchEvent g) { 
		
		if(g.type == TouchEvent.TOUCH_DOWN && m_anim == ANIM.NONE)
		{
			 
			for(int idx_x=0;idx_x<m_pageLimit;idx_x++)
			{
				int Gap =20*2+25;
				if(idx_x!=m_page)
				drawRoundRect(g, CONFIG.SCREEN_MID + idx_x*Gap  - (Gap*(m_pageLimit-1))/2-30, c_pageBtnPosY-70, 60,70, Color.WHITE, Color.WHITE, PAGE_BTN+idx_x);
		 	 
			} 
		 	drawRoundRect(g, (int)(Constants.BACK_BTN_CIR*0.2f), -(int)(Constants.BACK_BTN_CIR*0.8f), (int)(Constants.BACK_BTN_CIR*1.0f),(int)( Constants.BACK_BTN_CIR*1.5f),  Color.rgb(200, 200, 200), Color.rgb(200, 200, 200), BACK_BTN);     
			  
			m_dragPosX = g.x;  
			
			int l_posX = MARGIN_X;   
			
			for(int idx_x=0;idx_x<XLIMIT;idx_x++)
			{
				for(int idx_y=0;idx_y<YLIMIT;idx_y++)
				{
					// ENSURE THAT THIS WILL NOT EXCEED TO 127! (byte: 0-127)
					byte l_level = (byte)(((m_page)*(YLIMIT*YLIMIT))+ (idx_y*XLIMIT+idx_x)+1);
					if(l_level<= LevelManager.GetMaxLevel())
					{
						if(l_level<m_levelUnlocked+1)
						{ 
							drawRoundRect(g, l_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST), SELECT_LVL_SIZE, SELECT_LVL_SIZE, Color.WHITE, Color.rgb(180, 180, 180),l_level); 
					 	}
						if(Debugger.level_design&& !Debugger.DEMO_MODE)
						{ 
							drawRoundRect(g, l_posX+idx_x*(SELECT_LVL_SIZE + SELECT_LVL_DIST), c_buttonsPosY+idx_y*(SELECT_LVL_SIZE + SELECT_LVL_DIST), SELECT_LVL_SIZE, SELECT_LVL_SIZE, Color.WHITE, Color.rgb(180, 180, 180),l_level); 
						}
					}
				}
			}
			

			if(buttonPressed!=0&&buttonPressed!=BACK_BTN)  
				AssetManager.click.play(1.0f);
		}
		if(g.type == TouchEvent.TOUCH_DRAGGED)
		{
			m_draggingLeft = g.x<m_dragPosX; 
			if(Math.abs(g.x-m_dragPosX)>40)
			{
				m_dragSize    = Math.abs(g.x-m_dragPosX); 
				buttonPressed=0;
			}
			
		}
		
		if(g.type == TouchEvent.TOUCH_UP)
		{ 
			if(m_dragSize>130)
			{
				if(m_draggingLeft)
				{ 
					if(m_page+1<m_pageLimit)
					{ 
						m_xAnim =  m_dragSize;
					}
				}
				else
				{ 
					if(m_page>0)
					{ 
						m_xAnim =  m_dragSize;
					}
					
				}
			} 
			
			switch(buttonPressed)
			{
				case BACK_BTN: backButton(); buttonPressed=0; break;
			}
			
			if(buttonPressed>=PAGE_BTN)
			{
				m_page = buttonPressed-PAGE_BTN; 
			}
			else if(buttonPressed!=0)
			{  
				m_level_dest = buttonPressed;
				SetAnimation(ANIM.EXIT);  
				m_exitDest = EXIT_DEST.PLAY; 
			}
			
			buttonPressed=0;
			m_dragPosX=-1;
			m_dragSize=0;
 
		}
	}

	@Override
	public void backButton() 
	{  
		SetAnimation(ANIM.EXIT); 
		m_exitDest = EXIT_DEST.MENU; 
		AssetManager.click.play(1.0f);
	}

}
