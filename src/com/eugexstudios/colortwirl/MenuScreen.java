package com.eugexstudios.colortwirl;
     
import android.graphics.Color; 
import android.graphics.Rect; 
import android.graphics.Paint.Align; 

import com.eugexstudios.adprovider.AdProvider;
import com.eugexstudios.colortwirl.AskUserWindow.QUESTION;  
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent; 
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;
public class MenuScreen extends Screen
{   
	enum LeaveDestination
	{
		  Settings
		, levelSelect
		, LevelOne
	}; 
		
     public	enum State { 
		  IDLE 
		, ASK_EXIT 
		, SETTINGS_MENU
		, ABOUT 
		, ENTRY
		, LEAVE  
		, ABOUT_ENTRY
		, ABOUT_LEAVE 
		, AD_OPENLOAD
		, SAY_UPDATES
		, PRIVACY_POLICY
		 
	}; 

	final byte 
		  START_BTN            = 1
		, SETTINGS_BTN         = 2
		, REMOVE_ADS_BTN       = 3 
		, SHARE_APP_BTN        = 4
		, OPEN_FB_PAGE_BTN     = 5
		, SEND_FEEDBACK_BTN    = 6 
		, ABOUT_BTN            = 7 
		, CLOSE_ABOUT_BTN      = 8
		, SEND_BETA_RESULT     = 9 
		, GOTO_PUBLISHER_PAGE  = 10
		, AD_BTN               = 11 
		, OFFER_BTN            = 12 
		, PRIVACY_POLICY_BTN   = 13 
				;
	 
	 
	private final int                  ANIM_SIZE =5; 
	private final String			   VERSION_NAME;
	private AskUserWindow              m_askWindow;
	private static State		       m_state;   
	private SettingsWindow 			   m_settings;
	private PrivacyPolicyScreen 	   m_privacyPolicy;
	private LeaveDestination 		   m_leaveDest;
	private AdProvider 				   m_adProvider;   
	private int						   m_hiddenBtnTapCtr;
	private float					   m_anim[]; 
	private int                        m_yAdjust;
	 
	public MenuScreen(int p_levelPreview)
	{   
		 m_yAdjust         = 0;
		 m_hiddenBtnTapCtr = 0;  
	     m_state           = State.ENTRY;  
	     VERSION_NAME      = genSet.game.GetVersionName();
	     resetAnim();
	     m_settings        = new SettingsWindow(); 
	     m_adProvider      = new AdProvider();     
	     m_adProvider.RequestOfferWallAd();  
	}  	 
	 
	private void GoToPrivacyPolicy() 
	{
	     m_state           = State.PRIVACY_POLICY;  
	     m_privacyPolicy   = new PrivacyPolicyScreen();
		
	}
	@Override
	public void Paint(Graphics g)
	{ 
		if(m_state!=State.SETTINGS_MENU)
		{
			PaintBackGround(g); 
		}
	 	switch(m_state)
		{ 
			case ENTRY:			  PaintMenuAnim(g);	   break;
	 		case IDLE: 			  PaintMenuIdle(g);    break;
			case LEAVE:			  PaintMenuAnim(g);    break;
			case ABOUT: 		  PaintAbout(g);	   break; 
			case ABOUT_ENTRY:     PaintAboutEntry(g);  break;
			case ABOUT_LEAVE:     PaintAboutLeave(g);  break; 
			case SETTINGS_MENU:	  m_settings.Paint(g); break;  
			case ASK_EXIT: 		  PaintAskToExit(g);   break;
			case AD_OPENLOAD:	  PaintMenuIdle(g);    break;
			case PRIVACY_POLICY:  PaintPrivacyPolicy(g);break;
		    default: break; 
		}  
	}   
	
	private void PaintPrivacyPolicy(Graphics g)
	{
		PaintMenuIdle(g);  
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(100, 0,0, 0)); 
		m_privacyPolicy.Paint(g);	 
	}
 
	
	private void PaintAskToExit(Graphics g)
	{
		PaintMenuIdle(g); 
		m_askWindow.Paint(g);	 
	}
 
	private void PaintMenuAnim(Graphics g)
	{  
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, ColorUtil.SetOpacity(1- (m_anim[0]>0.5f?((m_anim[0]-0.5f)/0.5f):0), Color.WHITE));
		 
		int StartBtnY= Constants.ScaleY(0.765625f);
		int StartButtonAdjustY =  (int)((Constants.START_BTN*4) *(1-m_anim[1]));
  
		g.drawRect(new Rect(0,Constants.ScaleY(0.765625f)-Constants.ScaleX(0.131944444f)+StartButtonAdjustY,CONFIG.SCREEN_WIDTH,Constants.ScaleY(0.765625f)+StartButtonAdjustY+Constants.ScaleX(0.131944444f)), ColorUtil.SetOpacity(0.15f*m_anim[1], GameScreen.GetThemeColor()) );   
		g.drawCircle(CONFIG.SCREEN_MID+10, StartBtnY+11+StartButtonAdjustY,Constants.START_BTN, Color.argb((int)(13*m_anim[1]), 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,    StartBtnY+StartButtonAdjustY,   Constants.START_BTN, ColorUtil.SetOpacity(m_anim[1],GameScreen.GetThemeColor()));
 		g.drawImage(AssetManager.sprites, 0, StartButtonAdjustY, Constants.START_BTN_IMG);  
		 if(m_anim[2]>0)
		 {
			drawRoundRect(g,  CONFIG.SCREEN_WIDTH-((int)(Constants.ScaleX(0.159722222f)*(m_anim[2]<0.7f?m_anim[2]+0.3f:1 ))),  m_yAdjust+0,     Constants.ScaleX(0.1388888889f), Constants.ScaleX(0.1388888889f), ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), SETTINGS_BTN);  
 			g.drawImage(AssetManager.sprites,  CONFIG.SCREEN_WIDTH-((int)(Constants.MENU_SETTINGS_BTN.x*(m_anim[2]<0.7f?m_anim[2]+0.3f:1)))  , Constants.MENU_SETTINGS_BTN.y +m_yAdjust,Constants.MENU_SETTINGS_BTN.src_x,Constants.MENU_SETTINGS_BTN.src_y,Constants.MENU_SETTINGS_BTN.width,Constants.MENU_SETTINGS_BTN.height); //Settings icon //size_c
		
		 }
		drawRoundRect(g, CONFIG.SCREEN_WIDTH-((int)(Constants.ScaleX(0.131944444f)*m_anim[2])),  Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*0+m_yAdjust,  Constants.ScaleX(0.1111111f), Constants.ScaleX(0.1111111f),  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), ABOUT_BTN);  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_WIDTH-((int)(Constants.MENU_ABOUT_BTN.x*m_anim[2])),  Constants.MENU_ABOUT_BTN.y+m_yAdjust, Constants.MENU_ABOUT_BTN.src_x,Constants.MENU_ABOUT_BTN.src_y,Constants.MENU_ABOUT_BTN.width,Constants.MENU_ABOUT_BTN.height); // 'i' icon
 	
		drawRoundRect(g, CONFIG.SCREEN_WIDTH-((int)(Constants.ScaleX(0.131944444f)*m_anim[2])), Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*1+m_yAdjust, Constants.ScaleX(0.1111111f),  Constants.ScaleX(0.1111111f),  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), OFFER_BTN);  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_WIDTH-((int)(Constants.MENU_GIFT_BTN.x*m_anim[2])),  Constants.MENU_GIFT_BTN.y+m_yAdjust, Constants.MENU_GIFT_BTN.src_x,Constants.MENU_GIFT_BTN.src_y,Constants.MENU_GIFT_BTN.width,Constants.MENU_GIFT_BTN.height); // 'gift' icon
 	
		//Exact copy dapat ng PaintMenuIdle yung below nitong comment!   
		int p_y= (genSet.game.IsBannerAdEnabled()?25:0)+Constants.ScaleY(0.38125f);
		int colorX = CONFIG.SCREEN_MID-30-(int)(CONFIG.SCREEN_WIDTH*(1-m_anim[0]));
		int WheelX = CONFIG.SCREEN_MID+70+(int)(CONFIG.SCREEN_WIDTH*(1-m_anim[0])); 
		
		int LOGO_COLOR =Color.rgb(40, 40, 40);
		
		genSet.setTextProperties(Constants.TITLE_1_LS, Color.argb(30, 0, 0, 0), Align.CENTER);  
		g.drawStringFont("Color", colorX+10, p_y+11, AssetManager.Comforta,genSet.paint); 
		genSet.setTextProperties(Constants.TITLE_2_LS,  Color.argb(30, 0, 0, 0));  
		g.drawStringFont("Twirl", WheelX+10, p_y+(int)(Constants.TITLE_1_LS*0.76f)+11, AssetManager.Comforta,genSet.paint);
 
		genSet.setTextProperties(Constants.TITLE_1_LS,  LOGO_COLOR);  
		g.drawStringFont("Color", colorX, p_y, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(Constants.TITLE_2_LS,  LOGO_COLOR);   
		g.drawStringFont("Twirl", WheelX, p_y+(int)(Constants.TITLE_1_LS*0.76f), AssetManager.Comforta,genSet.paint); 
		
	} 
	
	private void PaintMenuIdle(Graphics g)
	{     
		drawRoundRect(g, CONFIG.SCREEN_MID-Constants.START_BTN, Constants.ScaleY(0.765625f)-Constants.START_BTN*2,   Constants.START_BTN*2, Constants.START_BTN*2, Color.argb(0, 0, 0, 0), Color.argb(0, 0, 0,0), START_BTN);  	
		g.drawRect(new Rect(0,Constants.ScaleY(0.765625f)-Constants.ScaleX(0.131944444f),CONFIG.SCREEN_WIDTH,Constants.ScaleY(0.765625f)+Constants.ScaleX(0.131944444f)), ColorUtil.SetOpacity(0.15f, GameScreen.GetThemeColor()));    
		g.drawCircle(CONFIG.SCREEN_MID+10, Constants.ScaleY(0.765625f)+11,Constants.START_BTN, Color.argb(13, 0, 0, 0));
		g.drawCircle(CONFIG.SCREEN_MID,    Constants.ScaleY(0.765625f),   Constants.START_BTN, START_BTN==buttonPressed?GameScreen.GetThemeColorLight():GameScreen.GetThemeColor());
		g.drawImage(AssetManager.sprites, Constants.START_BTN_IMG);  
			
		drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.159722222222f), 0+m_yAdjust,           Constants.ScaleX(0.13888888889f),		 Constants.ScaleX(0.13888888889f),	  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), SETTINGS_BTN);  	
 		g.drawImage(AssetManager.sprites,  CONFIG.SCREEN_WIDTH-Constants.MENU_SETTINGS_BTN.x, Constants.MENU_SETTINGS_BTN.y+m_yAdjust,Constants.MENU_SETTINGS_BTN.src_x,Constants.MENU_SETTINGS_BTN.src_y,Constants.MENU_SETTINGS_BTN.width,Constants.MENU_SETTINGS_BTN.height); //Settings icon //size_c
 		
		drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.131944444f), Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*0+m_yAdjust,   Constants.ScaleX(0.1111111111f), 		 Constants.ScaleX(0.1111111111f), 	  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), ABOUT_BTN );  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_WIDTH-Constants.MENU_ABOUT_BTN.x, Constants.MENU_ABOUT_BTN.y+m_yAdjust, Constants.MENU_ABOUT_BTN.src_x,Constants.MENU_ABOUT_BTN.src_y,Constants.MENU_ABOUT_BTN.width,Constants.MENU_ABOUT_BTN.height); // 'i' icon //default 
 
		drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.131944444f), Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*1+m_yAdjust, Constants.ScaleX(0.1111111111f),  Constants.ScaleX(0.1111111111f),  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), OFFER_BTN);  
		g.drawImage(AssetManager.sprites, CONFIG.SCREEN_WIDTH-Constants.MENU_GIFT_BTN.x, Constants.MENU_GIFT_BTN.y+m_yAdjust, Constants.MENU_GIFT_BTN.src_x,Constants.MENU_GIFT_BTN.src_y,Constants.MENU_GIFT_BTN.width,Constants.MENU_GIFT_BTN.height); // 'gift' icon//size_c
		
		if(m_adProvider.isOfferWallReady())
		{
			g.drawCircle(CONFIG.SCREEN_WIDTH-10,  m_yAdjust+Constants.ScaleY(0.2f), 5, Color.RED);
		}
		 
		PaintLogo(g,CONFIG.SCREEN_MID, (genSet.game.IsBannerAdEnabled()?25:0)+Constants.ScaleY(0.38125f) );
 
		genSet.setTextProperties(20, Color.rgb(40, 40, 40), Align.LEFT);  
		drawRoundRect(g, 5,CONFIG.SCREEN_HEIGHT-95, 290, 100, ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()),ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()),  PRIVACY_POLICY_BTN);
		g.drawString(LanguageManager.GetLocalized(W.VIEW_OUR_PRIVACY_POLICY), 25,CONFIG.SCREEN_HEIGHT-10, genSet.paint);
	}
	
 
	private void PaintAboutEntry(Graphics g)
	{ 
		PaintMenuIdle(g);
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb((int)(66*m_anim[0]), 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb((int)(130*m_anim[0]), 255,255, 255)); 
		PaintAboutWindow(g,-((int)(CONFIG.SCREEN_HEIGHT*(1-m_anim[0]))));
	}	
	
	private void PaintAboutLeave(Graphics g)
	{ 
		PaintMenuIdle(g);
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb((int)(66*(1-m_anim[0])),  0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb((int)(130*(1-m_anim[0])), 255,255, 255)); 
		PaintAboutWindow(g,-((int)(CONFIG.SCREEN_HEIGHT*m_anim[0])));
	}
	
	private void PaintAbout(Graphics g)
	{ 
		PaintMenuIdle(g);
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(66, 0, 0, 0));
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Color.argb(130, 255,255, 255)); 
		PaintAboutWindow(g,0);
		
	}
	
	private void PaintAboutWindow(Graphics g, int p_additionalY)
	{
		g.drawRoundRect((int)(CONFIG.SCREEN_WIDTH*0.1f),  p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.1367f), (int)(CONFIG.SCREEN_WIDTH*0.8f), (int)(CONFIG.SCREEN_HEIGHT*0.7109f), 40, 40, Color.WHITE);

		genSet.setTextProperties(Constants.ABOUT_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.ABOUT, CONFIG.SCREEN_MID, p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.21f)+Constants.ABOUT_LS/2, AssetManager.Comforta,genSet.paint);

		genSet.setTextProperties(Constants.CT_LS, Color.argb(30, 0, 0, 0), Align.CENTER);  
		g.drawStringFont("Color Twirl", CONFIG.SCREEN_MID+6, p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.3125f)+6, AssetManager.Comforta,genSet.paint);
		genSet.setTextProperties(Constants.CT_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Color Twirl", CONFIG.SCREEN_MID, p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.3125f), AssetManager.Comforta,genSet.paint);
		
		
		genSet.setTextProperties(Constants.VER_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(LanguageManager.GetLocalized(W.VERSION) + " "+VERSION_NAME , CONFIG.SCREEN_MID, p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.34765625f), AssetManager.Comforta,genSet.paint);
		
		genSet.setTextProperties(Constants.CREATED_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.CREATED_BY, CONFIG.SCREEN_MID, p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.4375f), AssetManager.Comforta,genSet.paint);
		genSet.setTextProperties(Constants.EUGEX_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont("Eugex Studios", CONFIG.SCREEN_MID,p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.484375f), AssetManager.Comforta,genSet.paint);
	 	
		genSet.setTextProperties(Constants.PLAY_MORE_LS, Color.DKGRAY, Align.CENTER);   
		g.drawStringFont(W.PLAY_MORE_FROM, CONFIG.SCREEN_MID,p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.5859375f), AssetManager.Comforta,genSet.paint); 
		drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.2f), p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.557812f) , (int)(CONFIG.SCREEN_WIDTH*0.6f), (int)(CONFIG.SCREEN_HEIGHT*0.0703f), Color.argb(35, 0, 0, 0), Color.argb(60, 0, 0, 0), GOTO_PUBLISHER_PAGE);   
		
		genSet.setTextProperties(Constants.EXPLR_GAMES_LS, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(W.EXPLORE_GAMES, CONFIG.SCREEN_MID, p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.64140625f), AssetManager.Comforta,genSet.paint);
		 
		genSet.setTextProperties(Constants.SHARE_CT_LS, Color.DKGRAY, Align.CENTER);   
		g.drawStringFont(W.SHARE_CW, CONFIG.SCREEN_MID, p_additionalY+ (int)(CONFIG.SCREEN_HEIGHT*0.703125f), AssetManager.Comforta,genSet.paint); 
		
		drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.2f), p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.557812f)+(int)(CONFIG.SCREEN_HEIGHT*0.1172f) , (int)(CONFIG.SCREEN_WIDTH*0.6f), (int)(CONFIG.SCREEN_HEIGHT*0.0703f), Color.argb(35, 0, 0, 0), Color.argb(60, 0, 0, 0), SHARE_APP_BTN);   
		genSet.setTextProperties(Constants.SHARE_NOW_LS, Color.DKGRAY, Align.CENTER);   
		g.drawStringFont(W.SHARE_NOW, CONFIG.SCREEN_MID, p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.75703125f), AssetManager.Comforta,genSet.paint); 

		drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.79f), p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.125f), (int)(CONFIG.SCREEN_WIDTH*0.0833333f), (int)(CONFIG.SCREEN_WIDTH*0.0833333f), Color.argb(40, 0, 0, 0), Color.argb(103, 0, 0, 0), CLOSE_ABOUT_BTN);    
		genSet.setTextProperties(40, Color.WHITE, Align.LEFT);   
		g.drawStringFont("X", (int)(CONFIG.SCREEN_WIDTH*0.79f)+(int)(CONFIG.SCREEN_WIDTH*0.02083333f), p_additionalY+(int)(CONFIG.SCREEN_HEIGHT*0.1851f), AssetManager.Comforta,genSet.paint); 
		  
	}
	
	private void PaintBackGround(Graphics g)
	{  
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT,Constants.PASTELIZER_COLOR);    
		
	}

	private void PaintLogo(Graphics g, int p_x, int p_y)
	{  
		int LOGO_COLOR =Color.rgb(40, 40, 40); 
		genSet.setTextProperties(Constants.TITLE_1_LS, Color.argb(30, 0, 0, 0), Align.CENTER);  
		g.drawStringFont("Color", p_x-30+10, p_y+11, AssetManager.Comforta,genSet.paint); 
		genSet.setTextProperties(Constants.TITLE_2_LS,  Color.argb(30, 0, 0, 0));  
		g.drawStringFont("Twirl", p_x+70+10, p_y+(int)(Constants.TITLE_1_LS*0.76f)+11, AssetManager.Comforta,genSet.paint);
 
		genSet.setTextProperties(Constants.TITLE_1_LS,  LOGO_COLOR);  
		g.drawStringFont("Color", p_x-30, p_y, AssetManager.Comforta,genSet.paint);  
		genSet.setTextProperties(Constants.TITLE_2_LS,  LOGO_COLOR);   
		g.drawStringFont("Twirl", p_x+70, p_y+(int)(Constants.TITLE_1_LS*0.76f), AssetManager.Comforta,genSet.paint); 
	}  

	private void resetAnim()
	{
		m_anim = new float[ANIM_SIZE];
		for(int idx=0;idx<m_anim.length;idx++)
		{
			m_anim[idx] = 0;
		}
	}

	private void AboutButtonUpdates(TouchEvent g)
	{ 
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g,(int)(CONFIG.SCREEN_WIDTH*0.2f), (int)(CONFIG.SCREEN_HEIGHT*0.557812f) , (int)(CONFIG.SCREEN_WIDTH*0.6f), (int)(CONFIG.SCREEN_HEIGHT*0.0703f), Color.argb(35, 0, 0, 0), Color.argb(60, 0, 0, 0), GOTO_PUBLISHER_PAGE);   
			drawRoundRect(g,(int)(CONFIG.SCREEN_WIDTH*0.2f), (int)(CONFIG.SCREEN_HEIGHT*0.557812f)+(int)(CONFIG.SCREEN_HEIGHT*0.1172f) , (int)(CONFIG.SCREEN_WIDTH*0.6f), (int)(CONFIG.SCREEN_HEIGHT*0.0703f), Color.argb(35, 0, 0, 0), Color.argb(60, 0, 0, 0), SHARE_APP_BTN);   
		    drawRoundRect(g,(int)(CONFIG.SCREEN_WIDTH*0.79f),(int)(CONFIG.SCREEN_HEIGHT*0.125f), (int)(CONFIG.SCREEN_WIDTH*0.0833333f), (int)(CONFIG.SCREEN_WIDTH*0.0833333f), Color.argb(40, 0, 0, 0), Color.argb(103, 0, 0, 0), CLOSE_ABOUT_BTN);  
		}
		
		if(g.type == TouchEvent.TOUCH_DOWN && buttonPressed==0){
		
			if(inBounds(g, 0, 0, 100, 100))
				buttonPressed=SEND_BETA_RESULT; 
			if(inBounds(g, 620, 0, 100, 100))
			{
				m_hiddenBtnTapCtr++;
				if(m_hiddenBtnTapCtr==4)
				{
					genSet.game.showToast("Next tap will clear datas");
				}
				if(m_hiddenBtnTapCtr==5)
				{
					GameScreen.SetPrefDataForInitInstall();
					genSet.game.showToast("Data cleared");
				}
				
			}
		
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case SHARE_APP_BTN: genSet.game.ShareApp();break;
				case OPEN_FB_PAGE_BTN: genSet.game.OpenFacebookPage(); break;
				case GOTO_PUBLISHER_PAGE:  genSet.game.GoToPublisherPage(); 	break;
				case CLOSE_ABOUT_BTN: 
					m_state = State.ABOUT_LEAVE;
					resetAnim(); 
					break;
				case SEND_BETA_RESULT: genSet.game.SendBetaDatas();break;
			}
		}
	}
	
	public static void GoToIdle()
	{ 
	    m_state = State.IDLE; 
	}
	
    public static State GetState()
	{ 
    	return m_state;
	}
    
	private void MenuButtonUpdates(TouchEvent g)
	{  
		if(g.type == TouchEvent.TOUCH_DOWN)
		{  
			drawRoundRect(g, CONFIG.SCREEN_MID-Constants.START_BTN, Constants.ScaleY(0.765625f)-Constants.START_BTN*2,   Constants.START_BTN*2, Constants.START_BTN*2, Color.argb(0, 0, 0, 0), Color.RED, START_BTN);  		
			drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.159722222222f), 0+m_yAdjust,           Constants.ScaleX(0.13888888889f),		 Constants.ScaleX(0.13888888889f),	  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), SETTINGS_BTN);  	
	 	 	drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.131944444f), Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*0+m_yAdjust,   Constants.ScaleX(0.1111111111f), 		 Constants.ScaleX(0.1111111111f), 	  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), ABOUT_BTN );  
			drawRoundRect(g, CONFIG.SCREEN_WIDTH-Constants.ScaleX(0.131944444f), Constants.ScaleX(0.1666666667f)+Constants.ScaleY(0.0703125f)*1+m_yAdjust, Constants.ScaleX(0.1111111111f),  Constants.ScaleX(0.1111111111f),  ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()), ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), OFFER_BTN);  

			drawRoundRect(g, 5,CONFIG.SCREEN_HEIGHT-95, 290, 100, ColorUtil.SetOpacity(0.4f , GameScreen.GetThemeColor()),ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()),  PRIVACY_POLICY_BTN);
		 }
		
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case PRIVACY_POLICY_BTN: 	GoToPrivacyPolicy(); 	break;
			case OFFER_BTN: 
				if(m_adProvider.isOfferWallReady())
				{
					m_adProvider.ShowOfferWall(
							new AdDisplayListener() {
							    @Override
							    public void adHidden(Ad ad) {
							    }
							    @Override
							    public void adDisplayed(Ad ad) {
							    }
							    @Override
							    public void adClicked(Ad ad) {
							    }
							    @Override
							    public void adNotDisplayed(Ad ad) {
							    }
							}
							);
				}
				break;
			    case AD_BTN:   
					m_state = State.AD_OPENLOAD;   
				    genSet.game.ShowDialog("Loading. Please wait...");
			    	m_adProvider.SendClick(); 
			    	buttonPressed = 0;
			    	break;
				case START_BTN:        
					if(GameScreen.GetLevelReached() == 1)
					{
						LeaveMenu(LeaveDestination.LevelOne);
					}
					else
					{
						LeaveMenu(LeaveDestination.levelSelect);
					}
					
					break;
				case SETTINGS_BTN:   LeaveMenu(LeaveDestination.Settings);break;
				case REMOVE_ADS_BTN: 
					genSet.game.SendBetaDatas();
//					GameScreen.RemoveAdRequest();  
				break;
				case ABOUT_BTN:   
					resetAnim();
					m_state = State.ABOUT_ENTRY;  
				
				break;
					 
				case SHARE_APP_BTN: genSet.game.ShareApp();break;
				case OPEN_FB_PAGE_BTN: genSet.game.OpenFacebookPage(); break;
				case SEND_FEEDBACK_BTN: genSet.game.SendEmailFeedback(); break; 
				
			}  
			
			buttonPressed=0;
		} 
	} 

	private void WaitForBanner()
	{
		if(m_yAdjust==0&&genSet.game.IsBannerAdEnabled())
	    {
	    	m_yAdjust = genSet.game.GetBannerHeight()-40;
	    }
	}
	
 	@Override
	public void Updates(float p_deltaTime) 
 	{  
 		switch(m_state)
 		{
 		    case IDLE:          WaitForBanner();                           break; 
			case ENTRY: 		menuEntryAnimUpdate(p_deltaTime);    	   break;
 			case LEAVE: 		menuLeaveAnimUpdate(p_deltaTime);  	       break;
 			case ABOUT_ENTRY:   AboutEntryLeaveUpdates(p_deltaTime,true);  break;
 			case ABOUT_LEAVE:   AboutEntryLeaveUpdates(p_deltaTime,false); break;
			case SETTINGS_MENU: m_settings.Updates(p_deltaTime);     	   break;
 			case ASK_EXIT:	    AskExitUpdates(); 						   break;

 			default: break;
 		}
   
	}
 	 
 	private void AskExitUpdates()
 	{
 		if(m_askWindow.isClosed()) 
		{
			m_state = State.IDLE;
		}
 	}
 	
	private void GoToSettings()
	{
		m_state = State.SETTINGS_MENU;
		m_settings = new SettingsWindow();  
	}
	
 	private void AboutEntryLeaveUpdates(float p_deltaTime, boolean p_isEntry) 
 	{ 
 		// 0 - moving down of window and background fade in
		if(m_anim[0]<1)
		{
			m_anim[0] += 0.05*p_deltaTime;
			if(m_anim[0]>=1)
			{
				m_state=p_isEntry?State.ABOUT:State.IDLE;  
			}
		} 
	}

	public void LeaveMenu(LeaveDestination p_leaveDest)
 	{
 		m_leaveDest = p_leaveDest;
 		m_state     = State.LEAVE;
 		m_anim[0]   = 1;
 		m_anim[1]   = 1;
 		m_anim[2]   = 1; 
 	}
	 
	private void menuLeaveAnimUpdate(float p_deltaTime)
 	{
 		// 0- 'Color' and 'Wheel' horizontal entry
 		// 1 - Start button vertical Entry
 		
 		if(m_anim[0]>0)
 		{
 			m_anim[0]-=0.024f  * (p_deltaTime);
 			
 			if(m_anim[0]<=0)
 				m_anim[0]=0;
 		}
 		if(m_anim[0]<0.5f)
 		{
 			if(m_anim[1]>0)
 		 		m_anim[1]-=0.048f  * (p_deltaTime); 

 			if(m_anim[1]<=0)
 			{
 				m_anim[1]=0; 
 			}
 		}
 		
 		if(m_anim[1]<0.7f)
 		{
 			if(m_anim[2]>0)
 		 		m_anim[2]-=0.04f  * (p_deltaTime); 

 			if(m_anim[2]<=0)
 			{
 				m_anim[2]=0;
 				switch(m_leaveDest)
 				{
 					case Settings: GoToSettings(); break;
 					case LevelOne: GameScreen.PlayGame(1); break;
 					case levelSelect: GameScreen.GoToLevelSelect(GameScreen.GetLevelReached()); break;
 				}
 			}
 		}  
 	} 
	
	private void menuEntryAnimUpdate(float p_deltaTime)
 	{
 		WaitForBanner(); 
 		// 0- 'Color' and 'Wheel' horizontal entry
 		// 1 - Start button vertical Entry
 		
 		if(m_anim[0]<1)
 		{
 			m_anim[0]+=0.024f  * p_deltaTime;
 			
 			if(m_anim[0]>1)
 				m_anim[0]=1;
 		}
 		if(m_anim[0]>0.5f)
 		{
 			if(m_anim[1]<1)
 		 		m_anim[1]+=0.048f  * p_deltaTime; 

 			if(m_anim[1]>=1)
 			{
 				m_anim[1]=1; 
 			}
 		}
 		
 		if(m_anim[1]>0.3f)
 		{
 			if(m_anim[2]<1)
 		 		m_anim[2]+=0.04f  * p_deltaTime; 

 			if(m_anim[2]>=1)
 			{
 				m_anim[2]=1;
 				m_state = State.IDLE;
 			}
 		}
 	}
  
	@Override
	public void TouchUpdates(TouchEvent g)
	{ 
		 
		switch(m_state)
		{
			case IDLE:	          MenuButtonUpdates(g);  break; 
			case ABOUT:    		  AboutButtonUpdates(g); break;
			case SETTINGS_MENU:		
				
				m_settings.TouchUpdates(g);	
				if(m_settings.isVisib()==false)
				{
					m_state = State.ENTRY;
				}
				break; 
			case ENTRY:			 break;
			case LEAVE:			 break;
 
			case ASK_EXIT: 		 m_askWindow.TouchUpdates(g);  break; 
			case PRIVACY_POLICY:  m_privacyPolicy.TouchUpdates(g);break;
			default: break;  
			
		} 

		
		if(g.type == TouchEvent.TOUCH_UP)
		buttonPressed =0;	
		else if(buttonPressed!=0 &&g.type == TouchEvent.TOUCH_DOWN)
		{ 
			AssetManager.click.play(1.0f);
		}
	} 
	 
	@Override
	public void backButton() {

		switch(m_state)
		{
			case IDLE:	          
				m_askWindow = new AskUserWindow(QUESTION.EXIT);
				m_state = State.ASK_EXIT; 	
			break; 
			case ASK_EXIT:	        m_askWindow.backButton(); break; 
			case ABOUT:    			m_state = State.ABOUT_LEAVE;
									resetAnim();  
				break;
			case SETTINGS_MENU:	  
				if(m_settings.isIdled())
				{ 
					m_state = State.ENTRY;	
				}
				else
					m_settings.backButton(); 
				
				break; 
			case ENTRY:			 break;
			case LEAVE:			 break;
			case AD_OPENLOAD:	 GoToIdle(); break;
			case PRIVACY_POLICY:  GoToIdle(); break;
			default: break;  
		} 
		 AssetManager.click.play(1.0f);
		
	}

}
