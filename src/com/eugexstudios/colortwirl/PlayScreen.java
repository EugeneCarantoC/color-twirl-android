package com.eugexstudios.colortwirl;
 
import java.util.Vector; 

import android.graphics.Color;  
import android.graphics.Paint.Align;
import android.graphics.Rect;  

import com.eugexstudios.adprovider.AdProvider;     
import com.eugexstudios.colortwirl.levels.Level;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;
import com.startapp.android.publish.adsCommon.Ad;
import com.startapp.android.publish.adsCommon.adListeners.AdDisplayListener;

public class PlayScreen extends Screen {

	final byte ANIM_CTR_SIZE=6; 
	final int DOT_X=CONFIG.SCREEN_MID, DOT_Y = (int)(CONFIG.SCREEN_HEIGHT*0.90625f);
	
	private final byte  
		  BACK_BTN	      = 1   
		, AD_CLICKED      = 2
		, INSTALL_NOW_BTN = 3
		, CLOSE_AD_BTN    = 4 
		, RESTART_BTN     = 5
	;
	  
	final byte 
	  INTRO_INSTR=1
	, COLORED_SHIELD_INSTR=3 
	, DIF_COLORED_SHIELD_INSTR=4
	;

	private enum LeaveDest { 
		  LEVEL_SELECT
		, MENU
		, SETTINGS
		, RESTART
		
	};
	public  enum State 
	{ 
		  SHOW_LEVEL
		, LEAVE
		, PLAYING
		, GAMEOVER 
		, PAUSED
		, AFTER_FAIL_AD 
		, AD_OPENLOAD 
		, LEVEL_COMPLETE 
		, TO_COUNTDOWN 
		, COUNTDOWN 
	}; 
	 
    enum ANIMATION
    {
    	 NO_ANIM
 	   , DOT_RELOAD
 	   , WRONG_HIT 
 	   , QUICK_PAUSE 
 	   , SHOW_AFTERGAME_AD  
 	}
    
	private enum HIT_TYPE
	{
		HIT_SHIELD
	  , HIT_DIF_COLOR
	}; 
   
	private static State               m_state;  
	private static ANIMATION 		   m_anim; 
	protected static Wheel 	           m_colorWheel; 
    private static Level			   m_levelData;
    private static Vector<Dot>		   m_dots;
    private static short		       m_currentLevel; 
    private static float[]			   m_animCtr; 
    private static byte 			   m_ShooterDotIdx; 
	private HIT_TYPE 			       m_hitType;
	private boolean 				   m_hasInstruction; 
	private boolean					   m_showAdAfterGame; 
	private boolean    				   m_impressionSent;
	private boolean    				   m_requestedAd;
	private LevelCompleteWindow 	   m_levelCompWin; 
	private static GameOverWindow      m_gameOverWindow;
	private LeaveDest 				   m_leaveDest;   
	private boolean                    m_canRestart; 
	private AdProvider 				   m_adProvider;
	private AdProvider 				   m_instestitial;
	private int 			           m_restartCtr; 
	private float                      m_leaveFadeOutSpeed;
	private float                      m_leaveStaySpeed; 
	private int                        c_dot_gap;
	
	public PlayScreen(Level p_levelData, short p_lvl)
	{ 
		 m_currentLevel   = p_lvl;   
	     m_adProvider     = new AdProvider();
	     m_instestitial   = new AdProvider();
	     m_instestitial.RequestInstiAd(); 
	     m_requestedAd    = false; 
		 m_animCtr	      = new float[ANIM_CTR_SIZE];   
		 m_gameOverWindow = new GameOverWindow();
		 m_restartCtr     = 0;
		 RestartGame(true, p_levelData,false);    
		 
		 if(Debugger.win_immediate && !Debugger.DEMO_MODE)
		{  
			 m_ShooterDotIdx++;
			 GoToLevelComplete();
		}
		c_dot_gap        = Constants.ScaleY(0.03125f);
	}   
	
	public static void ContinueGame()
	{
		m_state = State.PLAYING; 
		m_anim  =ANIMATION.NO_ANIM;
		m_colorWheel.ResetAnim(); 
 	    m_dots.get(m_ShooterDotIdx).Reactivate(); 
	}
	
	public static boolean isOnGameOverOpenAd()
	{
		return m_gameOverWindow.getState()== GameOverWindow.State.AD_OPENLOAD;
	}
	
   
	public static State GetState()
	{ 
    	return m_state;
	}
    
	public static void IdleGameOver()
	{ 
    	m_gameOverWindow.GoToIdle();
	}
    
	public void RestartGame(boolean p_firstTry, Level p_levelData, boolean p_hasAd)
	{  
		 SetState(p_firstTry?State.SHOW_LEVEL:State.PLAYING);   
		if(Debugger.DEMO_MODE==false&&Debugger.play_immediate&&Debugger.level_design)
		{
			
			p_firstTry=false;
			m_ShooterDotIdx=0;
			SetState( State.PLAYING);
		 }
		

		 m_levelData 	   = new Level(p_levelData); 
		 m_hasInstruction  = m_currentLevel==INTRO_INSTR|| m_currentLevel == COLORED_SHIELD_INSTR || m_currentLevel== DIF_COLORED_SHIELD_INSTR;
		 m_anim 		   = ANIMATION.NO_ANIM; 
		 m_colorWheel      = new Wheel(
									   m_levelData.GetColors()
									 , m_levelData.GetWheelDirection()
									 , m_levelData.GetWheelSpeed()
									 , m_levelData.GetWheelRad()
									 , m_levelData.GetShield()
									  ); 
		  
		 m_dots	           = new Vector<Dot>(); 

		 LoadDots(m_levelData.GetDotColors());    

		 if(p_firstTry)
		 {
			 m_ShooterDotIdx=0;
			 SetAnimation(ANIMATION.NO_ANIM);
		 }
		 else
		 { 
			 m_ShooterDotIdx=-1;
			 ReloadDot();
		 } 
		 m_showAdAfterGame=p_hasAd; 
		 if(m_showAdAfterGame && m_requestedAd==false)
		 { 
			 m_adProvider.RequestNativeAd(Constants.NATIVE_AD_GAMEOVER); 
			 m_impressionSent  = false;
			 m_requestedAd	   = true;
		 }

		 m_animCtr  = new float[ANIM_CTR_SIZE];
	} 
	
	private void LoadDots(int[] p_colors)
	{
		
		 for(int idx=0;idx<p_colors.length;idx++)
		 {
			 m_dots.add(new Dot(p_colors[idx])); 
		 }
	}
	
	@Override 
	public void Paint(Graphics g) 
	{  
		g.drawRect(Constants.SCREEN_RECT, Color.argb(80,255,255,255));
		switch (m_state) 
		{ 
			case PAUSED:        break;
			case LEAVE: 	    PaintLeaveGame(g);	       break;
			case PLAYING:       PaintGame(g);              break;  
			case GAMEOVER:      PaintGameOver(g);  		   break;
			case SHOW_LEVEL:    PaintShowLevel(g);	       break;
			case AFTER_FAIL_AD: PaintAfterGameAd(g);       break; 
			case LEVEL_COMPLETE:PaintLevelComplete(g);	   break;  
			case TO_COUNTDOWN:  PaintCountdown(g);	      break;
			case COUNTDOWN:     PaintCountdown(g);	      break;
			default:break;
		}  
	}
	
	private void PaintCountdown(Graphics g)
	{
		PaintGame(g);    
		g.drawRect(Constants.SCREEN_RECT, ColorUtil.SetOpacity(0.3f, Color.WHITE));
		if(m_state==State.TO_COUNTDOWN)
		{
			g.drawRect(Constants.SCREEN_RECT, ColorUtil.SetOpacity(1-m_animCtr[5], Color.WHITE));
			genSet.setTextProperties(Constants.COUNTDOWN_LS, Color.DKGRAY, Align.CENTER);  
			g.drawStringFont(3+"", CONFIG.SCREEN_MID,CONFIG.SCREEN_HEIGHT/2 , AssetManager.Comforta,genSet.paint);  
		}
		else
		{ 
			genSet.setTextProperties(Constants.COUNTDOWN_LS, Color.DKGRAY, Align.CENTER);  
			g.drawStringFont((3-(int)m_animCtr[4])+"", CONFIG.SCREEN_MID,CONFIG.SCREEN_HEIGHT/2 , AssetManager.Comforta,genSet.paint);  
		}
	}
	
	private void PaintLevelComplete(Graphics g)
	{
		PaintGame(g);   
		m_dots.get(m_ShooterDotIdx).PaintShattered(g);	 
		m_levelCompWin.Paint(g); 
	}
	
	private void PaintGameOver(Graphics g)
	{
		PaintGame(g);  
		m_gameOverWindow.Paint(g); 
	}
	
 
	private void PaintLeaveGame(Graphics g)
	{ 
		PaintGame(g);	 
	 	g.drawRect(Constants.SCREEN_RECT, ColorUtil.SetOpacity(m_animCtr[0], Color.WHITE)); 
	 }
	private void PaintDots(Graphics g)
	{  
		for(int idx=m_ShooterDotIdx+1;idx<m_dots.size();idx++)
		{ 
			m_dots.get(idx).Paint(g, DOT_X, DOT_Y+(idx*c_dot_gap)-(m_ShooterDotIdx*c_dot_gap),  Constants.DOT_RADIUS/2);	
		} 
	}
 
	private void PaintShowLevel(Graphics g)
	{   
		m_colorWheel.Paint(g);
 		g.drawRect(Constants.SCREEN_RECT, ColorUtil.SetOpacity(1-m_animCtr[3], Color.WHITE)); 
		String levelStr="";
		
		switch(GameScreen.GetLanguage())
		{
			case ENGLISH: 	 levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			case JAPANESE: 	 levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			case SPANISH:	 levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			case GERMAN:	 levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			case KOREAN:	 levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			case PORTUGUESE: levelStr= LanguageManager.GetLocalized(W.STAGE) + " " + m_currentLevel; break;
			default: break; 
		}
		
		int textY= CONFIG.SCREEN_HEIGHT/2;   
		int textSize=Constants.LEVEL_LS;
		genSet.setTextProperties( ((int)(textSize*m_animCtr[0])), ColorUtil.SetOpacity(1-m_animCtr[3], Color.DKGRAY), Align.CENTER);  
		g.drawStringFont(levelStr, CONFIG.SCREEN_MID-((int)(m_animCtr[3]*CONFIG.SCREEN_WIDTH)), textY+textSize/2 , AssetManager.Comforta,genSet.paint);  
		
		if(m_animCtr[1]>0)
		{
			int ExpTextSize=textSize+((int)((Constants.LEVEL_LS*2)*m_animCtr[1]));
			genSet.setTextProperties( ExpTextSize, ColorUtil.SetOpacity(1-m_animCtr[1],  Color.DKGRAY), Align.CENTER);  
			g.drawStringFont(levelStr, CONFIG.SCREEN_MID, textY+ExpTextSize/2, AssetManager.Comforta,genSet.paint);  
		}

	}

	private void PaintAfterGameAd(Graphics g) 
	{
		PaintGame(g); 
 		g.drawRect(Constants.SCREEN_RECT, Color.argb(102, 255, 255, 255));  
    	
 		float adWithdLimit = 0.66666667f;
    	int adWidth = (int)(((m_animCtr[4]<adWithdLimit? m_animCtr[4]:adWithdLimit))*m_adProvider.GetNativeAdBitmap().getWidth());
    	int adheight = (int)(((m_animCtr[4]<adWithdLimit? m_animCtr[4]:adWithdLimit))*m_adProvider.GetNativeAdBitmap().getHeight());
    	int adPosY= (int)(( CONFIG.SCREEN_HEIGHT/2)-adheight/2) -((int)(m_animCtr[4]*70)) ;

 		g.drawRect(0, adPosY-160,CONFIG.SCREEN_WIDTH, adheight+180+148 +(m_animCtr[5]>=1?110:0),    Color.argb(51, 0, 0, 0));  
 
 		if(m_anim==ANIMATION.SHOW_AFTERGAME_AD )
		{
			g.drawImage(m_adProvider.GetNativeAdBitmap(), 
						 (int)(CONFIG.SCREEN_MID)-adWidth/2 
						, adPosY
						, m_animCtr[4]<adWithdLimit? m_animCtr[4]:adWithdLimit); 
		 } 

		genSet.setTextProperties( 33, Color.WHITE, Align.CENTER); 
		g.drawStringFont(W.SPONSORED,CONFIG.SCREEN_MID, adPosY-107,AssetManager.Comforta,genSet.paint); 
		
		genSet.setTextProperties( 42, Color.WHITE, Align.CENTER); 
		g.drawStringFont(m_adProvider.GetNativeAdTitle(),CONFIG.SCREEN_MID, adPosY-55,AssetManager.Comforta,genSet.paint); 

		genSet.setTextProperties( 20, Color.WHITE, Align.LEFT); 
		g.drawStringFont(m_adProvider.GetNativeAdDesc().substring(0, m_adProvider.GetNativeAdDesc().length()>60?60:m_adProvider.GetNativeAdDesc().length())+"...",20, adPosY-15,AssetManager.Comforta,genSet.paint); 

		drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.125f), adPosY+adheight+20-55, (int)(CONFIG.SCREEN_WIDTH*0.75f), (int)(CONFIG.SCREEN_HEIGHT*0.0859375f),Color.WHITE, Color.rgb(218,218,228),INSTALL_NOW_BTN );     
		if(m_animCtr[5]<1)
		{
			drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.125f), adPosY+adheight+20-55, (int)((int)(CONFIG.SCREEN_WIDTH*0.75f)*(1-m_animCtr[5])) , (int)(CONFIG.SCREEN_HEIGHT*0.0859375f),Color.rgb(200, 200, 200), Color.rgb(218,218,228),INSTALL_NOW_BTN );     
		    
		}
		genSet.setTextProperties(Constants.INSTALL_LS, Color.DKGRAY, Align.CENTER); 
		g.drawStringFont(W.INSTALL,CONFIG.SCREEN_MID, adPosY+adheight+20+77,AssetManager.Comforta,genSet.paint); 
		
		if(m_animCtr[5]>=1)
		{ 
			drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.205f), adPosY+adheight+20+90, (int)(CONFIG.SCREEN_WIDTH*0.59f), (int)(CONFIG.SCREEN_HEIGHT*0.06640625f),Color.WHITE, Color.rgb(218,218,228),CLOSE_AD_BTN );    
			genSet.setTextProperties( 35, Color.DKGRAY, Align.CENTER); 
			g.drawStringFont(W.MAYBE_LATER,CONFIG.SCREEN_MID, adPosY+adheight+20+72+115,AssetManager.Comforta,genSet.paint); 
		}
    	 
 	 	if(m_impressionSent==false)
 	 	{
 		 	m_adProvider.SendImpression();
 		 	m_impressionSent = true;
 		 	m_requestedAd    = false;
 	 	}  
	}
	  
	public void StopAnimation()
	{
		m_anim = ANIMATION.NO_ANIM;
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx]=0;
		}
	}
	
	public static void SetAnimation(ANIMATION p_anim)
	{
		m_anim = p_anim;
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx]=0;
		}
	} 
	
	private void PaintGame(Graphics g) 
	{   
//		 
//		for(int idx=m_ShooterDotIdx+1;idx<m_dots.size();idx++)
//		{  
//			m_dots.get(idx).PaintTail(g); 
//		}   
		m_dots.get(m_ShooterDotIdx).PaintTail(g); 
		m_colorWheel.Paint(g);  
		PaintDots(g);  
		switch(m_anim)
		{
			case DOT_RELOAD:   m_dots.get(m_ShooterDotIdx).Paint(g,m_animCtr[0]);  	break; 
			case WRONG_HIT:    m_dots.get(m_ShooterDotIdx).PaintShattered(g);	  
				m_dots.get(m_ShooterDotIdx).Paint(g);  
				if(m_animCtr[2]<1)
				{
					 g.drawCircle(m_dots.get(m_ShooterDotIdx).getPos(), (int)(Constants.DOT_RADIUS*(m_animCtr[2]+1)), ColorUtil.SetOpacity((1-m_animCtr[2]),m_dots.get(m_ShooterDotIdx).GetColor())); 
				}
			break;
			case QUICK_PAUSE: 
				if((((int)(m_animCtr[0]))%2)==1||m_animCtr[0]<3)
				{
					m_dots.get(m_ShooterDotIdx).Paint(g);
				}
			break;
			default: 

				if(m_ShooterDotIdx<m_dots.size())
				{ 
					m_dots.get(m_ShooterDotIdx).Paint(g);
				}
				break;
		}
		    
		if(m_state==State.PLAYING )
		{
			genSet.setTextProperties(Constants.CUR_LEVEL_LS, Color.argb(255, 40, 40, 40), Align.LEFT);    
			g.drawStringFont(m_currentLevel+"", 20, Constants.CUR_LEVEL_LS, AssetManager.Comforta,genSet.paint);  
			
			if(m_hasInstruction)
			{
				PaintOnGameInstructions(g,m_currentLevel);	
			}
			

//			drawRoundRect(g, CONFIG.SCREEN_WIDTH-95, -50, 100, 100,  Color.RED,  ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), RESTART_BTN);  //Btn test
			 
			drawRoundRect(g, CONFIG.SCREEN_WIDTH-80,   -20,   70,   70,   m_canRestart? GameScreen.GetThemeColorLight():Color.GRAY,  ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), RESTART_BTN);  
			drawRoundRect(g, CONFIG.SCREEN_WIDTH-80,   -20,   70,   70,  Color.argb(110, 230, 230, 230),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), RESTART_BTN);  
			drawRoundRect(g, CONFIG.SCREEN_WIDTH-80+3, -20+3, 70-6, 70-5, m_canRestart? GameScreen.GetThemeColorLight():Color.GRAY ,  GameScreen.GetThemeColor(), RESTART_BTN);    
			g.drawImage(AssetManager.sprites, CONFIG.SCREEN_WIDTH -72, 22, 335,198,56,56);//'restart' icon
		}  
 
	}  
	
	
	private void PaintOnGameInstructions(Graphics g,int p_level) 
	{  
		if(p_level==INTRO_INSTR)
		{
	  		 genSet.setTextProperties(Constants.PLAY_INSTR_1, Color.DKGRAY, Align.CENTER);  
			 g.drawRect(new Rect(0,Constants.ScaleY(0.6953125f),CONFIG.SCREEN_WIDTH, Constants.ScaleY(0.7578125f)), Color.argb(17, 0, 0, 0)); 
			 g.drawStringFont(W.INSTR_TAP_ANYWHERE, CONFIG.SCREEN_MID, Constants.ScaleY(0.7421875f), AssetManager.Comforta,genSet.paint);
//			 Constants.ScaleY(0.96875f)
			 g.drawRect(new Rect(0,Constants.ScaleY(0.7734375f),CONFIG.SCREEN_WIDTH, Constants.ScaleY(0.8359375f)), Color.argb(17, 0, 0, 0)); 
			 g.drawStringFont(W.INSTR_HIT_MATCH, CONFIG.SCREEN_MID,Constants.ScaleY(0.8203125f), AssetManager.Comforta,genSet.paint);
//	
//			 g.drawRect(new Rect(0,Constants.ScaleY(0.921875f),CONFIG.SCREEN_WIDTH, Constants.ScaleY(0.984375f)), Color.argb(17, 0, 0, 0)); 
//			 g.drawStringFont(W.INSTR_AVOID_SHIELD, CONFIG.SCREEN_MID,  Constants.ScaleY(0.96875f), AssetManager.Comforta,genSet.paint);
		}
		else if(p_level==COLORED_SHIELD_INSTR)
		{
			 genSet.setTextProperties( Constants.PLAY_INSTR_1, Color.DKGRAY, Align.CENTER);  
			 g.drawRect(new Rect(0,Constants.ScaleY(0.6953125f),CONFIG.SCREEN_WIDTH,Constants.ScaleY(0.8046875f)), Color.argb(17, 0, 0, 0)); 
			 g.drawStringFont(W.COLORED_SHIELD_INSTR_1, CONFIG.SCREEN_MID, Constants.ScaleY(0.7421875f), AssetManager.Comforta,genSet.paint); 
			 g.drawStringFont(W.COLORED_SHIELD_INSTR_2, CONFIG.SCREEN_MID, Constants.ScaleY(0.78515625f), AssetManager.Comforta,genSet.paint);
			
		}
		else if(p_level==DIF_COLORED_SHIELD_INSTR)
		{
			 genSet.setTextProperties( Constants.PLAY_INSTR_1, Color.DKGRAY, Align.CENTER);  
			 g.drawRect(new Rect(0,Constants.ScaleY(0.6953125f),CONFIG.SCREEN_WIDTH,Constants.ScaleY(0.8046875f)), Color.argb(17, 0, 0, 0)); 
			 g.drawStringFont(W.DIF_COLORED_SHIELD_INSTR_1, CONFIG.SCREEN_MID, Constants.ScaleY(0.7421875f), AssetManager.Comforta,genSet.paint); 
			 g.drawStringFont(W.DIF_COLORED_SHIELD_INSTR_2, CONFIG.SCREEN_MID, Constants.ScaleY(0.78515625f), AssetManager.Comforta,genSet.paint);
			
		}
	}
 
	public void PaintManualBackBtn(Graphics g)
	{
		g.drawCircle(70+(buttonPressed==BACK_BTN?4:0),70+(buttonPressed==BACK_BTN?4:0),50,  Color.argb(200, 110, 110, 110));  
    
		genSet.setTextProperties( 65, Color.WHITE, Align.CENTER);   
		g.drawStringFont("<",70+(buttonPressed==BACK_BTN?4:0),93+ (buttonPressed==BACK_BTN?4:0), AssetManager.Comforta,genSet.paint);  
	} 
	
	public void PaintManualBackBtn(Graphics g, float p_scale)
	{
		g.drawCircle(70+(buttonPressed==BACK_BTN?4:0),70+(buttonPressed==BACK_BTN?4:0),(int)(50*p_scale),  Color.argb(230, 95, 95, 95));  
    
		genSet.setTextProperties( (int)(65*p_scale), Color.WHITE, Align.CENTER);   
		g.drawStringFont("<",70+(buttonPressed==BACK_BTN?4:0),93+ (buttonPressed==BACK_BTN?4:0), AssetManager.Comforta, genSet.paint);  
	} 
	 
	private void LeaveWindow(LeaveDest p_leaveDest)
	{ 
		m_leaveDest         = p_leaveDest;
		m_state             = State.LEAVE;
		switch(m_leaveDest)
		{
		case RESTART:
			m_leaveFadeOutSpeed = 0.03f;
		    m_leaveStaySpeed    = 0.03f;
		    break;
		default:
			m_leaveFadeOutSpeed = 0.05f;
		    m_leaveStaySpeed    = 0.05f;
			break;
		
		}
		SetAnimation(ANIMATION.NO_ANIM);
	}

	private void toCountdownUpdates(float p_deltaTime)
	{
		//5 - fade Out White
		if(m_animCtr[5]<1)
		{
			m_animCtr[5] += m_leaveFadeOutSpeed*p_deltaTime;
			if(m_animCtr[5]>=1)
			{
				m_animCtr[5]=1;
				SetState(State.COUNTDOWN);
//				SetAnimation(ANIMATION.NO_ANIM);
			}
		}
	}
 
	private void leaveUpdates(float p_deltaTime)
	{
		//0 - Fade in white
		//1 - Stay sa white
		
		if(m_animCtr[0]<1)
		{
			m_animCtr[0] += m_leaveFadeOutSpeed*p_deltaTime;
			if(m_animCtr[0]>=1)
				m_animCtr[0]=1;
		}
		if(m_animCtr[0]>=1.0f)
		{
			if(m_animCtr[1]<1)
			{
				m_animCtr[1]  += m_leaveStaySpeed *p_deltaTime;
				
				if(m_animCtr[1]>=1)
				{ 
					switch(m_leaveDest)
					{ 
						case LEVEL_SELECT:GameScreen.GoToLevelSelect(m_currentLevel);break;
						case MENU: GameScreen.GoToMenu();break;
						case RESTART:
							GameScreen.RestartGame();
							SetState(State.TO_COUNTDOWN);  
							break;
						default:
							break;
					}
				}
			}
		}
	}
	
	private void countdownUpdates(float p_deltaTime)
	{ 
		//3 - time speed
		//4 - Coundown time
		if(m_animCtr[3]<1)
		{
			m_animCtr[3]+=0.025f*p_deltaTime;
			if(m_animCtr[3]>=1)
			{
				m_animCtr[3]=0;
				if((int)m_animCtr[4]<2)
				{
					m_animCtr[4]++;
				}
				else
				{
					SetState(State.PLAYING);
				}
			}
		}
	}
	
	private void showLevelUpdates(float p_deltaTime)
	{
		//0 - Duration before level is shown
		//1 - Level appear animation
		//2 - Duration level is shown animation
		//3 - Preview close anim  
		if(m_animCtr[0]<1)
		{
			m_animCtr[0] += 0.04f*p_deltaTime;
			if(m_animCtr[0]>=1)
				m_animCtr[0]=1;
		}
		if(m_animCtr[0]>=1.0f)
		{
			if(m_animCtr[1]<1)
			{
				m_animCtr[1]  += 0.012f *p_deltaTime;
				
				if(m_animCtr[1]>=1)
				{ 
					m_animCtr[1]=1; 
				}
			}
		}
		
		if(m_animCtr[1]>=1.0f)
		{
			if(m_animCtr[2]<1)
			{
				m_animCtr[2]  += 0.012f * p_deltaTime;
				
				if(m_animCtr[2]>=1)
				{   
					m_animCtr[2]=1;
					m_colorWheel.Grow();
				}
			}
		
		}
		if(m_animCtr[2]>=1.0f)
		{
			if(m_animCtr[3]<1)
			{
				m_animCtr[3]  += 0.04f *(p_deltaTime);
				
				if(m_animCtr[3]>=1)
				{  
					SetState(State.PLAYING); 
					SetAnimation(ANIMATION.DOT_RELOAD);  
				}
			} 
		}
	}
	 
	private void afterFailAdUpdates(float p_deltaTime)
	{
		UpdateShatter(p_deltaTime);
		if(m_anim==ANIMATION.SHOW_AFTERGAME_AD )
		{ 
			if(m_animCtr[4]<1)
			{ 
				m_animCtr[4] += 0.04f*(p_deltaTime);
			} 
			
			if(m_animCtr[5]<1)
			{
				m_animCtr[5]+=0.004f*(p_deltaTime);
			}
		 
		}
	}
	
	private void onPlayUpdates(float p_deltaTime)
	{    
		if(m_anim==ANIMATION.NO_ANIM || m_anim==ANIMATION.DOT_RELOAD)
		{ 
			m_canRestart=true;
			boolean wheelSecOnPaintAnim = m_colorWheel.isAnimating();
		 
			 m_colorWheel.Update(p_deltaTime,m_dots.get(m_ShooterDotIdx).GetColor()); 
		 
			 UpdateCurrentDot(p_deltaTime);
			
			if(wheelSecOnPaintAnim&&!m_colorWheel.isAnimating())
			{
				ReloadDot();
				if(m_ShooterDotIdx==m_dots.size()) 
				{  
					GoToLevelComplete();
				}
			} 
		}
		
		UpdateShatter(p_deltaTime); 
		
	}
	
	private void gameOverUpdates(float p_deltaTime)
	{
		m_gameOverWindow.Updates(p_deltaTime); 
		UpdateShatter(p_deltaTime);
	}
	
	private void levelCompleteUpdates(float p_deltaTime)
	{
		UpdateShatter(p_deltaTime); 
		switch(m_levelCompWin.getState())
		{	
			case ENTRY:  m_colorWheel.Update(p_deltaTime);break;
			case IDLE:  break;
			case LEAVE: break;
			default: 	break;
		}

		m_levelCompWin.Updates(p_deltaTime);
	}
	 
	@Override
	public void Updates(float p_deltaTime)
	{  
		m_canRestart=false;
		
		switch (m_state)
		{  
		case TO_COUNTDOWN:   toCountdownUpdates(p_deltaTime);   break; 
		case COUNTDOWN:      countdownUpdates(p_deltaTime);     break;
		case LEAVE:          leaveUpdates(p_deltaTime);         break; 
		case SHOW_LEVEL:     showLevelUpdates(p_deltaTime);     break;
		case AFTER_FAIL_AD:  afterFailAdUpdates(p_deltaTime);   break; 
	    case PLAYING:        onPlayUpdates(p_deltaTime);        break;	
        case GAMEOVER:       gameOverUpdates(p_deltaTime);      break;
	    case LEVEL_COMPLETE: levelCompleteUpdates(p_deltaTime); break; 
		case PAUSED: break;
	    default:break;
		}
		
		switch(m_anim)
		{
			case DOT_RELOAD: 
				if(m_animCtr[0]<Constants.DOT_RADIUS)
				{
					m_animCtr[0]+=0.8f*(p_deltaTime); 
				}
				else
				{
					 if(m_ShooterDotIdx<m_dots.size())
					 {
						 m_dots.get(m_ShooterDotIdx).Activate();
					 }
					StopAnimation();
				}
				break;
			case WRONG_HIT:
			 
				// 2 - if <1: Screen shake
				
				if(m_animCtr[1]<3)// 5 = number of shakes
				{
					int shake =(int)m_animCtr[1];

					m_animCtr[0]= ((shake%2==0)?3:-6);
					m_animCtr[1]+=0.35*p_deltaTime;
				}
				else
				{
					m_animCtr[0]=0;
				}

				if(m_animCtr[2]<2f)
				{
					m_animCtr[2]+=0.04f*p_deltaTime; 
					
					if(m_animCtr[2]<2f) 
						m_animCtr[3]=1;
				} 
				
				if(m_animCtr[3]>0)
				{
					m_animCtr[3]-= (float)(0.012f*p_deltaTime); 
					if(m_animCtr[3]<=0)
					{
						EndGame();  
					}
				}
 
				break;

			case QUICK_PAUSE: 
				
				if(m_animCtr[0]<13)
				{
					m_animCtr[0]+=0.17f*p_deltaTime; 
				}
				else
				{
					AnimateWrongHit();
				}
				
				break;
				 
			default: break;
		}  
	}  
	
	private void UpdateShatter(float p_deltaTime)
	 {
		 m_dots.get(m_ShooterDotIdx).UpdateTail(p_deltaTime);   
		 m_dots.get(m_ShooterDotIdx).UpdateShatter(p_deltaTime);
	}
	 	
	private void UpdateCurrentDot(float p_deltaTime) 
	 { 
		 
		 m_dots.get(m_ShooterDotIdx).Update(p_deltaTime,m_hasInstruction?150:700);
		 
		 if(m_dots.get(m_ShooterDotIdx).isReleased() && m_dots.get(m_ShooterDotIdx).isActive())
		 {  
			boolean isWHeelHit	   = m_colorWheel.isHit(m_dots.get(m_ShooterDotIdx).getPos());
			
			if(isWHeelHit)
			{  
				m_dots.get(m_ShooterDotIdx).Stop(); 
				if(m_colorWheel.IsSameColorHit(m_dots.get(m_ShooterDotIdx).GetColor()))
				{
					m_dots.get(m_ShooterDotIdx).Disapear(); 
					m_colorWheel.PaintBottom();   
					AssetManager.wheel_painted.play(1.0f);  
				}
				else
				{ 
					if(m_dots.get(m_ShooterDotIdx).isActive())
					{
						WrongHit(HIT_TYPE.HIT_DIF_COLOR);
					}
				}
			} 
			else if(m_colorWheel.isShieldHitted(m_dots.get(m_ShooterDotIdx).GetColor(), m_dots.get(m_ShooterDotIdx).getBounds()))
			{
				WrongHit(HIT_TYPE.HIT_SHIELD);
			} 
		} 
			 
			
	 }
	
	public void WrongHit(HIT_TYPE p_hitType)
	 {
		 m_hitType=p_hitType;
		 switch(m_hitType)
		 {
		 case HIT_DIF_COLOR: 
		 case HIT_SHIELD:
			 break;
		 } 
		 SetAnimation(ANIMATION.QUICK_PAUSE);   
	 } 
	 
	public void AnimateWrongHit()
	 {  
		 SetAnimation(ANIMATION.WRONG_HIT);     
		 AssetManager.wrong_hit_shield.play(0.8f);
		 m_dots.get(m_ShooterDotIdx).Shatter(); 
		 
		 if(GameScreen.IsVibrateOn())
		 {
		 switch(m_hitType)
			 {
				 case HIT_SHIELD:    genSet.game.Vibrate(new long[]{0,85,50,85,50,160});break;
				 case HIT_DIF_COLOR: genSet.game.Vibrate(new long[]{0,65,45,170});	    break;
			 } 	
		 }
		 if(m_showAdAfterGame)
		 {   
			 ShowAfterGameAd(); 
		 } 
		
		 
	 }
	 
	public boolean ShowAfterGameAd()
	 {
		 if(m_adProvider.HasNativeAd() &&m_adProvider.GetNativeAdBitmap()!=null)
		 { 
			SetState(State.AFTER_FAIL_AD);
			SetAnimation(ANIMATION.SHOW_AFTERGAME_AD);
			m_animCtr[4] = 0.01f;
			m_showAdAfterGame = false;
			GameScreen.AdShowed();
			return true;
		}
		 else 
		 {
			 GameScreen.AdFailed(); 
			 return false;
		 }
	 }
	 
	private void ReloadDot()
	{ 
		SetAnimation(ANIMATION.DOT_RELOAD);
		m_ShooterDotIdx++;  

		
	}
	
	@Override
	public void TouchUpdates(TouchEvent g) 
	{  
		
		switch (m_state)
		{  
			case LEVEL_COMPLETE: 	m_levelCompWin.TouchUpdates(g);  break;
			case AFTER_FAIL_AD:   AfterFailAdButtonUpdates(g);break;  
  			case PLAYING:   	PlayButtonUpdates(g);	break;
			case GAMEOVER: m_gameOverWindow.TouchUpdates(g);break;
			case PAUSED: break;
			default: break;
	
			} 
		if(g.type== TouchEvent.TOUCH_UP)
		{
			buttonPressed =0; 
		} 
	}

	private void AfterFailAdButtonUpdates(TouchEvent g)
	{ 
		if(g.type==TouchEvent.TOUCH_DOWN)
		{
			float adWithdLimit = 0.66666667f; 
	    	int adheight = (int)(((m_animCtr[4]<adWithdLimit? m_animCtr[4]:adWithdLimit))*m_adProvider.GetNativeAdBitmap().getHeight());
	    	int adPosY= (int)(( CONFIG.SCREEN_HEIGHT/2)-adheight/2) -((int)(m_animCtr[4]*70)) ;  
	    	 
	    	drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.125f), adPosY+adheight+20-55, (int)(CONFIG.SCREEN_WIDTH*0.75f), (int)(CONFIG.SCREEN_HEIGHT*0.0859375f),Color.WHITE, Color.rgb(218,218,228),INSTALL_NOW_BTN );     
			drawRoundRect(g, (int)(CONFIG.SCREEN_WIDTH*0.205f), adPosY+adheight+20+90, (int)(CONFIG.SCREEN_WIDTH*0.59f), (int)(CONFIG.SCREEN_HEIGHT*0.06640625f),Color.WHITE, Color.rgb(218,218,228),CLOSE_AD_BTN );    
			 
			if(inBounds(g,0, adPosY-120,CONFIG.SCREEN_WIDTH, adheight+200))
			{
				buttonPressed=AD_CLICKED;
			}    

		}
		if(g.type==TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case AD_CLICKED: 	 
				case INSTALL_NOW_BTN:  
					buttonPressed=0;
					m_state = State.AD_OPENLOAD;
					m_adProvider.SendClick();   
				    genSet.game.ShowDialog("Loading. Please wait...");
					break;
				case CLOSE_AD_BTN: EndGame();	break;
			}
		} 
	}
	
	public void GoToLevelComplete()
	{ 
		m_ShooterDotIdx--;
		m_colorWheel.Shrink();  
		SetAnimation(ANIMATION.NO_ANIM);
		SetState(State.LEVEL_COMPLETE);
		m_levelCompWin = new LevelCompleteWindow(m_dots.get(m_ShooterDotIdx), m_colorWheel.GetColors());
		GameScreen.UnlockNextLevel();
	}
	
	private void PlayButtonUpdates(TouchEvent g) 
	{   
		if(g.type==TouchEvent.TOUCH_DOWN)
		{  
			if(m_canRestart)
			{
				drawRoundRect(g, CONFIG.SCREEN_WIDTH-95, -50, 100, 100,  Color.RED,  ColorUtil.SetOpacity(0.65f , GameScreen.GetThemeColor()), RESTART_BTN);  
			}
			if((buttonPressed==0)&&(m_ShooterDotIdx<m_dots.size()  && !m_colorWheel.isAnimating()))
			{  
				m_dots.get(m_ShooterDotIdx).Shoot();  
				if(GameScreen.IsVibrateOn())
				{
					genSet.game.Vibrate(10);
				}
			}
			else
			{
				switch(buttonPressed)
				{
					case RESTART_BTN: 
						
						if(m_restartCtr>2&& m_instestitial.isInstiReady())
						{ 
								m_instestitial.StartAppShowInsti( 
										new AdDisplayListener() {
										    @Override
										    public void adHidden(Ad ad) { 
										    	Restart();
										    }
										    @Override
										    public void adDisplayed(Ad ad) {
										    }
										    @Override
										    public void adClicked(Ad ad) {
										    }
										    @Override
										    public void adNotDisplayed(Ad ad) {
										    }
										}
										);
						    	m_restartCtr=0;
						} 
						else
						{ 	
							Restart();
							m_restartCtr++; 
						}
						break;
				}
			}
		}
	} 
	
	public void Restart()
	{  
		LeaveWindow(LeaveDest.RESTART);
	}
	
	public static void EndGame()
	{ 
		AssetManager.game_result_entry.play();
		SetAnimation(ANIMATION.NO_ANIM);
		m_gameOverWindow.Show(m_currentLevel, m_ShooterDotIdx>m_dots.size()/2);
		SetState(State.GAMEOVER);
	}
	
	public static void GoToGameOverIdle() 
	{
		m_state = State.GAMEOVER;  
		m_gameOverWindow.SetState(GameOverWindow.State.IDLE);
	}

	public static void SetState(State p_state) 
	{
		m_state = p_state; 
	}
	
	@Override
	public void backButton() 
	{
		switch (m_state)
		{  
			case AD_OPENLOAD: 
			    genSet.game.HideDialog();
				SetState(State.AFTER_FAIL_AD);
				
				break; 
			case PLAYING:      
				AssetManager.click.play(1.0f);
				if(GameScreen.GetLevelReached()==1)
					LeaveWindow(LeaveDest.MENU); 
				else
					LeaveWindow(LeaveDest.LEVEL_SELECT); 
				break;
			case PAUSED:         SetState(State.PLAYING); break;
			case AFTER_FAIL_AD:  
				if(m_animCtr[5]>=1) 
				{
					AssetManager.click.play(1.0f);
					EndGame();	
				}
				break;
			case GAMEOVER:    m_gameOverWindow.backButton(); 	break;
			case LEVEL_COMPLETE: m_levelCompWin.backButton(); break;
			default: break;
		}  
	} 

}
