package com.eugexstudios.colortwirl;
 
 
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;  
import com.eugexstudios.framework.Input.TouchEvent; 
public class PrivacyPolicyScreen extends Screen {
	 

	enum states {
		  	IDLE    
	}; 
	private states                               m_state;   
	private int                                  m_dragY; 
	private boolean                              m_onDrag;
	private int                                  m_holdPosY;  
	private final int MINIMUM_Y = -(int)(AssetManager.privacy_policy.getHeight())+((int)(CONFIG.SCREEN_HEIGHT*0.92f));  

	 boolean m_dragAllowed;
	 int m_touchDownY;;
	public PrivacyPolicyScreen()
	{ 
		m_state = states.IDLE;     
		m_dragY       = 0;
		m_onDrag      = false;
		m_holdPosY    = 0; 
		 
	}
	
	 
	 
	@Override
	public void Paint(Graphics g) {
		
		switch(m_state)
		{
			case IDLE:  PaintIdle(g);break; 
		}   
	} 
	
	private void PaintIdle(Graphics g) 
	{   
		g.drawImage(AssetManager.privacy_policy, 30,40+m_dragY); 
	}
	
	@Override
	public void Updates(float p_deltaTime) {

	 
		switch(m_state)
		{
			case IDLE:  	break; 
		}  
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{ 
		switch(m_state)
		{
			case IDLE:   IdleTouchUpdates(g);  break;
		} 
	}
 
	 
	private void IdleTouchUpdates(TouchEvent g) 
	{
		int l_dragSize=0; 
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			m_dragAllowed=true; 
			m_touchDownY=g.y; 
			
		} 
		else if(g.type == TouchEvent.TOUCH_DRAGGED)
		{

			if(buttonPressed==0)
			{
				if(m_dragAllowed)
				{
					if(m_onDrag==false)
					{
						m_onDrag = true;
						m_holdPosY=g.y;
					}
					else
					{
						  l_dragSize = (int)((g.y -m_holdPosY)*1.2f);
						m_holdPosY= g.y; 
						m_dragY+=l_dragSize;  
						
						if(m_dragY>0)
						{  
							m_dragY  = 0;  
							m_onDrag=false; 
						}
//						if(m_dragY<DRAG_UP_LIMIT-CONFIG.SCREEN_HEIGHT/2)
//						{  
//							m_dragY  = DRAG_UP_LIMIT; 
//							m_onDrag=false;
//							m_dragAllowed=false;
//							l_dragSize=m_touchDownY-g.y;
//						}
					}
				}
			} 
			 
		}
		
		else if(g.type == TouchEvent.TOUCH_UP)
		{
			if(buttonPressed!=0)
			{
				int l_level_dest =buttonPressed;    
			}
			else
			{
				if(m_onDrag)
				{
					m_onDrag=false;   
//					if(m_dragY<DRAG_UP_LIMIT)
//					{ 
//						m_dragY   = DRAG_UP_LIMIT;   
//						l_dragSize=m_touchDownY-g.y;
//					}
					if(m_dragY<MINIMUM_Y)
					{
						m_dragY   = MINIMUM_Y;    
						
					} 
				}
			}
			buttonPressed=0;
		}
		 
	}

	@Override
	public void backButton() 
	{		
		 
	}

}
