package com.eugexstudios.colortwirl;

import android.graphics.Color;
import android.graphics.Rect; 
import android.graphics.Paint.Align; 

import com.eugexstudios.colortwirl.AskUserWindow.QUESTION;
import com.eugexstudios.colortwirl.GameScreen.LANGUAGE;   
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.LanguageManager;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.LanguageManager.W;

public class SettingsWindow extends Screen{
 
	
	enum states {
		  	IDLE  
		  , ENTRY
		  , SELECT_LANG 
		  , ASK_TO_RESET
	}; 
	
	private final byte 
			  LANG_BTN   = 1
			, SOUND_BTN  = 2
			, MUSIC_BTN  = 3
			, VIB_BTN    = 4
			, LANG_BTN_1 = 5
			, LANG_BTN_2 = 7
			, LANG_BTN_3 = 8
			, LANG_BTN_4 = 9
			, LANG_BTN_5 = 10
			, LANG_BTN_6 = 11
		    , BACK_BTN   = 12
			, RESET_DATA  = 13
			, OKAY_SAVE_LANG_BTN   = 52
			, CANCEL_SAVE_LANG_BTN = 53
			;
	
	private states					   m_state; 
	private int 					   m_languagePreviewIdx;
	private float[]					   m_animCtr;
	private boolean 				   m_isVisib;
	private AskUserWindow		       m_askWindow;
	
	public SettingsWindow()
	{
		m_isVisib = true;
		m_state   = states.ENTRY;
		resetAnim();
	}
	
	public boolean isVisib()
	{
		return m_isVisib;
	}
	
	private void resetAnim()
	{
		m_animCtr = new float[5];
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx] = 0;
		}
	}
	
	@Override
	public void Paint(Graphics g) {
   
		switch(m_state)
		{
			case ENTRY:        PaintEntry(g);        break;
			case IDLE:         PaintIdle(g);		 break;
			case SELECT_LANG:  PaintLangSettings(g); break;
			case ASK_TO_RESET: PaintAskToReset(g);   break;
		} 
	}
	private void PaintAskToReset(Graphics g)
	{ 
		PaintIdle(g);	
		m_askWindow.Paint(g); 		
	}
	private void PaintEntry(Graphics g)
	{ 
		PaintIdle(g);	 
		g.drawRect(0, 0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, ColorUtil.SetOpacity(1-m_animCtr[0], Color.WHITE)); 
		
	} 
	final int BUTTON_GAP = Constants.ScaleY(0.109375f);
	final int BUTTON_LABEL_GAP = Constants.ScaleY(0.3395f);
	private void PaintIdle(Graphics g)
	{  
		 g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Constants.PASTELIZER_COLOR); 
		 g.drawRect(new Rect(0,(int)(CONFIG.SCREEN_HEIGHT * 0.17578125f),CONFIG.SCREEN_WIDTH, (int)(CONFIG.SCREEN_HEIGHT * 0.87109375f)),ColorUtil.SetOpacity(0.30f,  GameScreen.GetThemeColorLight())); 
		 g.drawRect(new Rect(0,(int)(CONFIG.SCREEN_HEIGHT * 0.17578125f),CONFIG.SCREEN_WIDTH, (int)(CONFIG.SCREEN_HEIGHT * 0.25390625f)),ColorUtil.SetOpacity(0.2f,  GameScreen.GetThemeColorLight())); 

		 genSet.setTextProperties( Constants.SETTINGS_LS, Color.argb(45, 0, 0, 0), Align.CENTER);  
		 g.drawStringFont(W.SETTINGS, CONFIG.SCREEN_MID+5, (int)(CONFIG.SCREEN_HEIGHT * 0.21f)+Constants.SETTINGS_LS/2+5, AssetManager.Comforta,genSet.paint);  
		 genSet.setTextProperties( Constants.SETTINGS_LS, Color.WHITE, Align.CENTER);  
		 g.drawStringFont(W.SETTINGS, CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.21)+Constants.SETTINGS_LS/2, AssetManager.Comforta,genSet.paint);  

		 drawRoundRect(g, CONST[5][0], CONST[5][1], 	           BUTTON_WIDTH,   BUTTON_HEIGHT,   GameScreen.GetThemeColor(),      GameScreen.GetThemeColorLight(), LANG_BTN); 
		 drawRoundRect(g, CONST[5][0], CONST[5][1], 	           BUTTON_WIDTH,   BUTTON_HEIGHT,   ColorUtil.SetOpacity(0.15f,      Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), LANG_BTN); 
		 drawRoundRect(g, CONST[5][0]+3, CONST[5][1]+3,            BUTTON_WIDTH-6, BUTTON_HEIGHT-6, GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(), LANG_BTN); 
 
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*1,     BUTTON_WIDTH,   BUTTON_HEIGHT,   GameScreen.GetThemeColor(),      GameScreen.GetThemeColorLight(), VIB_BTN);
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*1, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,   ColorUtil.SetOpacity(0.15f, Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), VIB_BTN); 
		 drawRoundRect(g, CONST[5][0]+3, CONST[5][1]+BUTTON_GAP*1+3, BUTTON_WIDTH-6, BUTTON_HEIGHT-6, GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(), VIB_BTN); 

		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*2,     BUTTON_WIDTH,   BUTTON_HEIGHT,   GameScreen.GetThemeColor(),      GameScreen.GetThemeColorLight(), SOUND_BTN);  
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*2, 	   BUTTON_WIDTH,   BUTTON_HEIGHT,   ColorUtil.SetOpacity(0.15f, Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), SOUND_BTN); 
		 drawRoundRect(g, CONST[5][0]+3, CONST[5][1]+BUTTON_GAP*2+3, BUTTON_WIDTH-6, BUTTON_HEIGHT-6, GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(), SOUND_BTN); 
		 
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*3,     BUTTON_WIDTH,   BUTTON_HEIGHT,   GameScreen.GetThemeColor(), GameScreen.GetThemeColorLight(), MUSIC_BTN); 
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*3, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,   ColorUtil.SetOpacity(0.15f, Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), MUSIC_BTN); 
		 drawRoundRect(g, CONST[5][0]+3, CONST[5][1]+BUTTON_GAP*3+3, BUTTON_WIDTH-6, BUTTON_HEIGHT-6, GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(), MUSIC_BTN); 
		 
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*4,     BUTTON_WIDTH,   BUTTON_HEIGHT,   GameScreen.GetThemeColor(), GameScreen.GetThemeColorLight(), RESET_DATA); 
		 drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*4, 	   BUTTON_WIDTH ,  BUTTON_HEIGHT,   ColorUtil.SetOpacity(0.15f, Color.DKGRAY),ColorUtil.SetOpacity(0.15f,  Color.DKGRAY), RESET_DATA); 
		 drawRoundRect(g, CONST[5][0]+3, CONST[5][1]+BUTTON_GAP*4+3, BUTTON_WIDTH-6, BUTTON_HEIGHT-6, GameScreen.GetThemeColorLight(), GameScreen.GetThemeColor(), RESET_DATA);    
		
		 genSet.setTextProperties( Constants.SETTINGS_BTN1_S, Color.DKGRAY, Align.CENTER);   
		 if(GameScreen.GetLanguage()!=LANGUAGE.ENGLISH)
			 g.drawStringFont("Language (" + LanguageManager.GetLocalized(W.LANGUAGE) + ")" ,  CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.302f),	    AssetManager.Comforta, genSet.paint);
		 else
			 g.drawStringFont(LanguageManager.GetLocalized(W.LANGUAGE),  CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT * 0.302f),	    AssetManager.Comforta, genSet.paint); 
		 
		 g.drawStringFont(W.VIBRATION, CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*1, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.SOUND,     CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*2, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.MUSIC, 	   CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.3f)+BUTTON_GAP*3, AssetManager.Comforta, genSet.paint); 
			 
 
		 genSet.setTextProperties( Constants.SETTINGS_BTN_S, Color.DKGRAY, Align.CENTER);  
		 g.drawStringFont(GameScreen.GetLanguageT(),  		    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP, 	       AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsVibrateOn()?W.ON:W.OFF,  CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*1, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsSoundOn()?W.ON:W.OFF,    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*2, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(GameScreen.IsMusicOn()?W.ON:W.OFF,    CONFIG.SCREEN_MID, BUTTON_LABEL_GAP+BUTTON_GAP*3, AssetManager.Comforta, genSet.paint);
		 g.drawStringFont(W.RESET_DATA,CONFIG.SCREEN_MID,                          BUTTON_LABEL_GAP+BUTTON_GAP*4, AssetManager.Comforta, genSet.paint);
		  
		 PaintManualBackBtn(g);
	} 
	public void PaintManualBackBtn(Graphics g)
	{	 
//		drawRoundRect(g, (int)(Constants.BACK_BTN_CIR*0.2f), -(int)(Constants.BACK_BTN_CIR*0.8f), (int)(Constants.BACK_BTN_CIR*1.0f),(int)( Constants.BACK_BTN_CIR*1.5f),  Color.rgb(200, 200, 200), Color.rgb(200, 200, 200), BACK_BTN);     
	    g.drawCircle(    (int)(Constants.BACK_BTN_CIR*0.2f)+(int)(Constants.BACK_BTN_CIR*0.5f),  (int)(Constants.BACK_BTN_CIR*0.2f)+ (int)(Constants.BACK_BTN_CIR*0.5f), Constants.BACK_BTN_CIR/2,   GameScreen.GetThemeColor());  
	    g.drawImage(AssetManager.sprites,(int)(Constants.BACK_BTN_CIR*0.2f),  (int)(Constants.BACK_BTN_CIR*0.2f),Constants.BACK_IMG); 
	} 
		 
	private final int[][] CONST = new int[][]{
			 new int[]{(int)(CONFIG.SCREEN_WIDTH*0.15f),       (int)(CONFIG.SCREEN_HEIGHT*0.171875f)}  //0 LANG BTNS X, Y  
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.7f),        (int)(CONFIG.SCREEN_HEIGHT*0.078125f)}  //1 LANG BTNS LEN, WID
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.1f),        (int)(CONFIG.SCREEN_HEIGHT*0.117187f)}  //2 BTN WINDOW X,Y
			,new int[]{(int)(CONFIG.SCREEN_WIDTH*0.8f),        (int)(CONFIG.SCREEN_HEIGHT*0.76171f)}   //3 BTN WINDOW LEN WID 
			,new int[]{(int)((CONFIG.SCREEN_WIDTH*0.7f)/2)-10, (int)(CONFIG.SCREEN_HEIGHT*0.078125f)}  //4 LANG SAVE/CANCEL BTNS LEN WID 
			,new int[]{(int)(CONFIG.SCREEN_WIDTH * 0.15f),     (int)(CONFIG.SCREEN_HEIGHT*0.234f)}   //5 MAIN SETTINGS BTN X Y 
			, new int[]{0,                                     (int)(CONFIG.SCREEN_HEIGHT*0.08984375f)}  //6  LANG TEXT BTNS X, Y  
	};
	
	private final int BUTTON_HEIGHT =  Constants.ScaleY(0.088f);
	private final int BUTTON_WIDTH =  Constants.ScaleX(0.7f);
	private void PaintLangSettings(Graphics g) 
	{  
		g.drawRect(0, 0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT, Constants.PASTELIZER_COLOR); 
		g.drawRoundRect( CONST[2][0],CONST[2][1],CONST[3][0],CONST[3][1],20,20, Color.argb(40, 0, 0, 0));  //BTN WINDOW 

		genSet.setTextProperties(Constants.SETT_LANG_LS, Color.WHITE, Align.CENTER);  
		g.drawStringFont(LanguageManager.GetLocalized(W.LANGUAGE), CONFIG.SCREEN_MID, Constants.ScaleY(0.17578125f), AssetManager.Comforta,genSet.paint); 

		genSet.setTextProperties(Constants.SETT_LANG_BTN_LS, Color.DKGRAY, Align.CENTER);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.ENGLISH.getIdx()?    GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_1);  
	 	drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.JAPANESE.getIdx()?   GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_2);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.SPANISH.getIdx()?    GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_3);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.GERMAN.getIdx()?     GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_4);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.KOREAN.getIdx()?     GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_5);  
		drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.PORTUGUESE.getIdx()? GameScreen.GetThemeColorLight():Color.WHITE, GameScreen.GetThemeColorLight(), LANG_BTN_6);  
		
		g.drawStringFont(LanguageManager.GetTranslation(W.ENGLISH,    LANGUAGE.ENGLISH),    CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), AssetManager.Comforta, genSet.paint); 	
		g.drawStringFont(LanguageManager.GetTranslation(W.JAPANESE,   LANGUAGE.JAPANESE),   CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), AssetManager.Comforta, genSet.paint); 
		g.drawStringFont(LanguageManager.GetTranslation(W.SPANISH,    LANGUAGE.SPANISH),    CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.GERMAN,     LANGUAGE.GERMAN),     CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.KOREAN,     LANGUAGE.KOREAN),     CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetTranslation(W.PORTUGUESE, LANGUAGE.PORTUGUESE), CONFIG.SCREEN_MID, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), AssetManager.Comforta, genSet.paint);  
		
		drawRoundRect(g, CONST[0][0],                CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,GameScreen.GetThemeColorLight(), OKAY_SAVE_LANG_BTN);  
		drawRoundRect(g, CONST[0][0]+CONST[4][0]+20, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,GameScreen.GetThemeColorLight(), CANCEL_SAVE_LANG_BTN);  
		
//		genSet.setTextProperties( 45, Color.DKGRAY, Align.CENTER);  
		g.drawStringFont(LanguageManager.GetLocalized(W.SAVE),   CONST[0][0]+CONST[4][0]/2,                CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*6),  AssetManager.Comforta, genSet.paint);  
		g.drawStringFont(LanguageManager.GetLocalized(W.CANCEL), CONST[0][0]+CONST[4][0]+20+CONST[4][0]/2, CONST[0][1]+CONST[6][1]+(Constants.SETT_LANG_BTN_DIST_Y*6),  AssetManager.Comforta, genSet.paint);  
		

		PaintManualBackBtn(g);
		
	}
 
	public void Updates(float p_deltaTime) 
	{ 
		switch(m_state)
		{
			case ENTRY:    
				// 0 - fade out
				if(m_animCtr[0]<1)
				{
					m_animCtr[0] += 0.095*p_deltaTime;
					if(m_animCtr[0]>=1)
					{
						m_state=states.IDLE;
					}
				}
				
				break;
			case IDLE:         		    break;
			case ASK_TO_RESET:
				 if(m_askWindow.isClosed()) 
 				 {
 					 m_state = states.IDLE;
 				 }
				 break;
			case SELECT_LANG:  break;
		} 
		
	}

	@Override
	public void TouchUpdates(TouchEvent g) {

		switch(m_state)
		{
			case IDLE: IdleButtonUpdates(g); break;
			case SELECT_LANG:LangBtnUpdates(g); break;
			case ASK_TO_RESET: m_askWindow.TouchUpdates(g);break;
			default:
				break;
		}

		if(g.type == TouchEvent.TOUCH_DOWN &&buttonPressed!=0)  
			AssetManager.click.play(1.0f);
	}

	public void IdleButtonUpdates(TouchEvent g)
	{
		
		if(g.type == TouchEvent.TOUCH_DOWN)
		{
			drawRoundRect(g, CONST[5][0], CONST[5][1],              BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), LANG_BTN);     
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*1, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), VIB_BTN);   
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*2, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), SOUND_BTN);     
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*3, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), MUSIC_BTN);  
			drawRoundRect(g, CONST[5][0], CONST[5][1]+BUTTON_GAP*4, BUTTON_WIDTH, BUTTON_HEIGHT, Color.WHITE, Color.rgb(200, 200, 200), RESET_DATA);   
			drawRoundRect(g, (int)(Constants.BACK_BTN_CIR*0.2f), -(int)(Constants.BACK_BTN_CIR*0.8f), (int)(Constants.BACK_BTN_CIR*1.0f),(int)( Constants.BACK_BTN_CIR*1.5f),  Color.rgb(200, 200, 200), Color.rgb(200, 200, 200), BACK_BTN);     

		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
				case LANG_BTN:   
					m_languagePreviewIdx= GameScreen.GetLanguage().getIdx();
					m_state = states.SELECT_LANG;	 break; 
				case SOUND_BTN: GameScreen.ToggleSoundSettings();  break; 
				case MUSIC_BTN: GameScreen.ToggleMusicSettings();  break; 
				case VIB_BTN:  	GameScreen.ToggleVirbrateSettings();break;  
				case RESET_DATA:
					m_askWindow = new AskUserWindow(QUESTION.RESET_DATA);
					m_state = states.ASK_TO_RESET;
					break;
				case BACK_BTN: this.backButton();break;
			}
			buttonPressed=0;
		} 
	}

	private void LangBtnUpdates(TouchEvent g)
	{

		if(g.type == TouchEvent.TOUCH_DOWN)
		{
		    drawRoundRect(g, 70-50, 70-100, 100, 100, Color.WHITE, Color.rgb(200, 200, 200), BACK_BTN);   
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*0), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.ENGLISH.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),    LANG_BTN_1);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*1), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.JAPANESE.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),   LANG_BTN_2);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*2), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.SPANISH.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),    LANG_BTN_3);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*3), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.GERMAN.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),     LANG_BTN_4);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*4), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.KOREAN.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200),     LANG_BTN_5);  
			drawRoundRect(g, CONST[0][0], CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*5), CONST[1][0], CONST[1][1], m_languagePreviewIdx==GameScreen.LANGUAGE.PORTUGUESE.getIdx()?Color.rgb(200, 200, 200):Color.WHITE,Color.rgb(200, 200, 200), LANG_BTN_6);  
			drawRoundRect(g, CONST[0][0],                                        CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,Color.rgb(200, 200, 200), OKAY_SAVE_LANG_BTN);  
			drawRoundRect(g, CONST[0][0]+(int)((CONFIG.SCREEN_WIDTH*0.7f)/2)+20, CONST[0][1]+(Constants.SETT_LANG_BTN_DIST_Y*6), CONST[4][0], CONST[4][1], Color.WHITE,Color.rgb(200, 200, 200), CANCEL_SAVE_LANG_BTN); 
		}
		if(g.type == TouchEvent.TOUCH_UP)
		{
			switch(buttonPressed)
			{
			case LANG_BTN_1: m_languagePreviewIdx = GameScreen.LANGUAGE.ENGLISH.getIdx();  break;
			case LANG_BTN_2: m_languagePreviewIdx = GameScreen.LANGUAGE.JAPANESE.getIdx();  break;
			case LANG_BTN_3: m_languagePreviewIdx = GameScreen.LANGUAGE.SPANISH.getIdx();  break;
			case LANG_BTN_4: m_languagePreviewIdx = GameScreen.LANGUAGE.GERMAN.getIdx();  break;
			case LANG_BTN_5: m_languagePreviewIdx = GameScreen.LANGUAGE.KOREAN.getIdx();  break;
			case LANG_BTN_6: m_languagePreviewIdx = GameScreen.LANGUAGE.PORTUGUESE.getIdx();  break;
			case OKAY_SAVE_LANG_BTN: 
				
				GameScreen.SetLanguage(GameScreen.LANGUAGE.values()[m_languagePreviewIdx]); 
				 m_state = states.IDLE;
				 break;
			case CANCEL_SAVE_LANG_BTN:  m_state = states.IDLE;	break;

			case BACK_BTN: this.backButton();break; 
			}

			buttonPressed=0;
		} 
	}
	public boolean isIdled()
	{
		return m_state ==states.IDLE;
	}
	@Override
	public void backButton() 
	{

		switch(m_state)
		{ 
			case IDLE: m_isVisib=false;break;
			case SELECT_LANG: m_state = states.IDLE; break;
			case ASK_TO_RESET: m_state = states.IDLE;break;
			default:
				break;
		}
  
	}
	 

}
