package com.eugexstudios.colortwirl;
  
import com.eugexstudios.colortwirl.shields.Shield;
import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.ColorUtil;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Debugger;
import com.eugexstudios.utils.GameMath;
import com.eugexstudios.utils.genSet;
import com.eugexstudios.utils.Constants.DIRECTION;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect; 
import android.graphics.Paint.Align;

public class Wheel { 

    private final int 				   SHELL_UNIT_RADIUS = 15;  
    private final int 				   SHELL_SWEEP_DIST  = (int)(SHELL_UNIT_RADIUS/Math.PI);
    private final int 			   	   SHELL_COUNT       = (int)(360/SHELL_SWEEP_DIST);
    
    private enum Animation{NONE, SHRINK,GROW};
    private Animation 				   m_animType;
    private float 				  	   m_animCtr[]; 
	private float					   m_rotation; 
	private int					  	   m_angleTarget;
	private float					   m_rotationSpeed;  
	private int[]		   			   m_colors; 
	private int						   m_sectionSize; 
	private int						   m_radius; 
    private int				   		   m_paintEffectRad; 
	private boolean[]		   		   m_painted;
	private float[]		   		  	   m_glowAnim;  
	private DIRECTION				   m_direction;
	private Shield[]		      	   m_shield;
	private boolean 				   m_onPaintAnim;
	private float 					   m_paintRad; 
	private int						   m_paintSectionIdx;
	private int						   m_paintAngle; 
	private Point					   m_pos;  
	private int[]					   SECTION_ANGLE; 
	private int[]					   SHELL_ANGLE; 
	private int[]					   BOTTOM_COLOR;
    private int 			   	   	   BOTTOM_WHEEL_Y;
	
	public Wheel(int[] p_colors, DIRECTION p_dir, float p_speed,int p_rad, Shield[] p_shield)
	{ 
		m_colors		  = p_colors;  
		m_direction		  = p_dir;
		m_rotationSpeed   = p_speed;
		m_shield		  = p_shield.clone();
		m_rotation        = p_dir==DIRECTION.LEFT?360:0;  
		m_angleTarget     = GetTargetAngle();
		m_sectionSize 	  = 360/p_colors.length;
		
		SECTION_ANGLE     = new int[p_colors.length];
		for(int idx=0;idx<SECTION_ANGLE.length;idx++)
		{
			SECTION_ANGLE[idx] = m_sectionSize*idx;
		}

		SHELL_ANGLE 	 = new int[SHELL_COUNT];
		SHELL_ANGLE 	 = new int[SHELL_COUNT];
		for(int idxs=0;idxs<SHELL_ANGLE.length;idxs++)
		{ 
			SHELL_ANGLE[idxs] = (int)(idxs*SHELL_SWEEP_DIST);   
		}
		
		BOTTOM_COLOR	 = new int[360];
		for(int angle=0;angle<BOTTOM_COLOR.length;angle++)
		{  
			int colIdx=(360-angle)/m_sectionSize;
			colIdx = colIdx>=m_colors.length?m_colors.length-1:colIdx;
			
			BOTTOM_COLOR[angle] = m_colors[colIdx];
		}
		
		SetRadius(p_rad); 
		m_painted		  = new boolean[m_colors.length]; 
		m_glowAnim	  	  = new float[m_colors.length]; 
		m_paintRad 	      = 0;
		m_paintSectionIdx = -1;
		m_paintAngle	  = 0;
		m_onPaintAnim 	  = false;
 
		m_pos 			  = new Point(Constants.WHEEL_POSITION); 
		for(int idx=0;idx<m_painted.length;idx++)
		{
			m_painted[idx]	= false;
			m_glowAnim[idx] = 0;
		}  
		m_animCtr           = new float[5];

		BOTTOM_WHEEL_Y   = m_pos.y + m_radius;
		
		SetAnimation(Animation.NONE);  
		
		
	}
	public int[] GetColors()
	{
		return m_colors;
	}
	
	public void SetAnimation(Animation p_anim)
	{
		m_animType = p_anim;
		for(int idx=0;idx<m_animCtr.length;idx++)
		{
			m_animCtr[idx]	= 0; 
		}  
	} 
	
	public void Shrink()
	{ 
		SetAnimation(Animation.SHRINK);
	}
	
	public void Grow()
	{ 
		SetAnimation(Animation.GROW);
	}
	   
	public void Paint(Graphics g)
	{
		switch(m_animType)
		{
			case NONE:   PaintDefault(g); 			  break;
			case GROW:   PaintShrinkOrGrow(g, false); break;
			case SHRINK: PaintShrinkOrGrow(g,true);   break;
		}
		
		if(Debugger.show_wheel_bottom&&!Debugger.DEMO_MODE )
		{
			PaintDebug(g);
		}
	}
	
	private void PaintDebug(Graphics g)
	{      
			g.drawLine(new Point(CONFIG.SCREEN_MID,0),   new Point(CONFIG.SCREEN_MID,CONFIG.SCREEN_HEIGHT),Color.RED);
			g.drawLine(new Point(CONFIG.SCREEN_MID-1,0), new Point(CONFIG.SCREEN_MID-1,CONFIG.SCREEN_HEIGHT),Color.RED);
			g.drawLine(new Point(CONFIG.SCREEN_MID+1,0), new Point(CONFIG.SCREEN_MID+1,CONFIG.SCREEN_HEIGHT),Color.RED);
			
			g.drawLine(new Point(0,BOTTOM_WHEEL_Y),   new Point(CONFIG.SCREEN_WIDTH,BOTTOM_WHEEL_Y),Color.RED); 
			g.drawLine(new Point(0,BOTTOM_WHEEL_Y-1), new Point(CONFIG.SCREEN_WIDTH,BOTTOM_WHEEL_Y-1),Color.RED); 
			g.drawLine(new Point(0,BOTTOM_WHEEL_Y+1), new Point(CONFIG.SCREEN_WIDTH,BOTTOM_WHEEL_Y+1),Color.RED); 
			
 			genSet.setTextProperties(35, Color.RED, Align.LEFT); 
			g.drawString("SEC SIZE:      " + (int)(m_sectionSize) +"" , 2, 50+36*21, genSet.paint);   
			g.drawString("Rotation:      " + (int)(m_rotation) +"" , 2, 50+36*22, genSet.paint);    
			g.drawString("Target Angle:  " + (int)(m_angleTarget) +"" , 2, 50+36*23, genSet.paint);    
			g.drawString("Bottom Color:  " + BOTTOM_COLOR[(int)m_angleTarget] +"" , 2, 50+36*24, genSet.paint);  
			
			genSet.setTextProperties( 20, Color.RED, Align.LEFT); 
			for(int idx=0;idx<m_colors.length;idx++)
			{
				g.drawString(  m_colors[idx]+"" , 2, 50+36*25 + idx*22, genSet.paint);  
			} 
			g.drawString(ColorManager.YELLOW+"" , 2, 50+36*24 + (m_colors.length+1)*25, genSet.paint);  
	 
	}
	
	private void PaintShrinkOrGrow(Graphics g, boolean p_isShrink)
	{
		if(p_isShrink&&m_animCtr[0]<1 || !p_isShrink&&m_animCtr[0]>=1.25f)
		{
			for(int idx=0;idx<m_shield.length;idx++)
			{ 
				m_shield[idx].PaintAnimation(g);
			}
		}
		
		float Scale = 1; 
		if(p_isShrink)
		{
			Scale=m_animCtr[0]<1?1+(m_animCtr[0]/4):(1.25f)-m_animCtr[1]; 
		}
		else
		{ 
			Scale=m_animCtr[0]<1.25f?m_animCtr[0]:1.25f-(m_animCtr[1]/4); 
		}
		
		int Wheel_radius = (int) (Scale*m_radius);
		
		if(m_onPaintAnim)
		{
			  int l_sweepAngle = (int)((SECTION_ANGLE[m_paintSectionIdx])+m_rotation);
		      g.drawArc(m_pos.x, m_pos.y,Wheel_radius, l_sweepAngle, m_sectionSize,  GetSectionColor(m_paintSectionIdx)); 
		      Point effectOrigin= GameMath.GetPointA(m_pos,Wheel_radius, (int)(m_paintAngle+m_rotation));      
		      g.drawCircleClippedByArc(effectOrigin, m_paintEffectRad-(int)(m_paintRad), Wheel_radius, l_sweepAngle, m_sectionSize, GetSectionColor(m_paintSectionIdx)); 
		      g.drawCircleClippedByArc(effectOrigin, m_paintEffectRad-(int)(m_paintRad), Wheel_radius, l_sweepAngle, m_sectionSize, ColorUtil.SetOpacity(0.14f, GetSectionColor(m_paintSectionIdx))); 
		}    
			 
		for(int idx=0;idx<m_colors.length;idx++)
		{
			int l_sweepAngle = (int)(SECTION_ANGLE[idx]+m_rotation);
			if(m_paintSectionIdx!=idx)
			{
				g.drawArc(m_pos.x, m_pos.y, Wheel_radius, l_sweepAngle, m_sectionSize,GetSectionColor(idx)); 
				g.drawArc(m_pos.x, m_pos.y, Wheel_radius, l_sweepAngle, m_sectionSize,ColorUtil.SetOpacity(0.14f ,Constants.ARC_INACTIVE_OVERLAY)); 
			} 
		} 
 
	}
	
	
	private void PaintDefault(Graphics g)
	{ 
		// PAG MAY BINAGO DITO DAPAT PATI SA SHRINKING DIN
		
		for(int idx=0;idx<m_shield.length;idx++)
		{ 
			m_shield[idx].PaintDefault(g); 
		}
  
		if(m_onPaintAnim==false)
		{
			for(int idx=0;idx<m_colors.length;idx++)
			{
				g.drawArc(m_pos.x, m_pos.y, m_radius, SECTION_ANGLE[idx]+(int)m_rotation, m_sectionSize+1,GetSectionColor(idx));  
			
				if(m_glowAnim[idx]>0)
				{ 
		    		g.drawArc(m_pos.x, m_pos.y, m_radius, SECTION_ANGLE[idx]+(int)m_rotation, m_sectionSize+1,ColorUtil.SetOpacity(m_glowAnim[idx]>1?2-m_glowAnim[idx]:m_glowAnim[idx], Color.WHITE)); 
				}  
			}   
		}
		else
		{
			int l_sweepAngle = SECTION_ANGLE[m_paintSectionIdx]+(int)m_rotation;
		    g.drawArc(m_pos.x, m_pos.y,m_radius, l_sweepAngle, m_sectionSize,  GetSectionColor(m_paintSectionIdx)); 
		    Point effectOrigin= GameMath.GetPointA(m_pos,m_radius, (int)(m_paintAngle+m_rotation));      
		    g.drawCircleClippedByArc(effectOrigin, m_paintEffectRad-(int)(m_paintRad), m_radius, l_sweepAngle, m_sectionSize+1, GetSectionColor(m_paintSectionIdx)); 
		    g.drawCircleClippedByArc(effectOrigin, m_paintEffectRad-(int)(m_paintRad), m_radius, l_sweepAngle, m_sectionSize+1, ColorUtil.SetOpacity(0.14f,GetSectionColor(m_paintSectionIdx))); 
			
		    for(int idx=0;idx<m_colors.length;idx++)
			{
				if(m_paintSectionIdx!=idx)
				{
					g.drawArc(m_pos.x, m_pos.y, m_radius, SECTION_ANGLE[idx]+(int)m_rotation, m_sectionSize+1,GetSectionColor(idx));  
				} 
				if(m_glowAnim[idx]>0)
				{ 
		    		g.drawArc(m_pos.x, m_pos.y, m_radius, SECTION_ANGLE[idx]+(int)m_rotation, m_sectionSize+1,ColorUtil.SetOpacity(m_glowAnim[idx]>1?2-m_glowAnim[idx]:m_glowAnim[idx], Color.WHITE)); 
				}  
			} 
		
		}     

		g.drawCircle(m_pos.x, m_pos.y, m_radius,ColorUtil.SetOpacity(0.1f ,Constants.ARC_INACTIVE_OVERLAY)); 
//		g.drawImage(AssetManager.wheel_detail, -1, 0);
		
		if(Debugger.show_bounds&&!Debugger.DEMO_MODE )
		{ 
			
			for(int idx=0;idx<m_shield.length;idx++)
			{ 
				m_shield[idx].PaintShellBounds(g);
			} 
		}  
	} 
	
	public void Update(float p_deltaTime)
	{	
		Update(p_deltaTime,-1);
	}
	 
	public void Update(float p_deltaTime, int p_currentDotColor)
	{
		 Rotate(p_deltaTime);  
		 UpdateWheelShield(p_deltaTime); 
		 
		 switch(m_animType)
		 {
		 case NONE:   UpdateWheelAnimation(p_deltaTime); break;
		 case GROW:
			 // 0 - expand na sobra
			 // 1 - Shinking  
			 
			if(m_animCtr[0]<1.25f)
			{
				m_animCtr[0] += 0.08f*(p_deltaTime);
				if(m_animCtr[0]>=1.25f)
					m_animCtr[0]=1.25f;
			}
			if(m_animCtr[0]>=1.25f)
			{
				if(m_animCtr[1]<1)
				{
					m_animCtr[1]  += 0.1f *(p_deltaTime);
						
					if(m_animCtr[1]>=1)
					{ 
						m_animCtr[1]=1; 
						SetAnimation(Animation.NONE);
					}
				}
			}
			 break;
		 case SHRINK:
			 // 0 - expand na bwelo
			 // 1 - Shinking  
			 // 2 - Twirl  
			 Rotate(23);
			if(m_animCtr[0]<1)
			{
				m_animCtr[0] += 0.08f*(p_deltaTime);
				if(m_animCtr[0]>=1)
					m_animCtr[0]=1;
			}
			if(m_animCtr[0]>=1.0f)
			{
				if(m_animCtr[1]<1.25f)
				{
					m_animCtr[1]  += 0.07f *(p_deltaTime);
						
					if(m_animCtr[1]>=1.25f)
					{ 
						m_animCtr[1]=1.25f; 
//						SetAnimation(Animation.NONE);
					}
				}
			}
			 
			 break;
		 default: break;
		 }
	}
 
	private void Rotate(float p_deltaTime)
	{	
		if(m_direction==DIRECTION.LEFT)
		{
			if(m_rotation>0)
			{
				m_rotation-= m_rotationSpeed * p_deltaTime; 
			}
			else
			{
				m_rotation+=360;  
			}
			
			if(m_rotation<0)
			{
				m_rotation=0;
			}
		}
		else
		{ 
			if(m_rotation<360)
			{
				m_rotation+= m_rotationSpeed * p_deltaTime;  
			}
			else
			{
				m_rotation-=360; 
				
			} 
			if(m_rotation>360)
			{
				m_rotation=360;
			}

		}  
		m_angleTarget = GetTargetAngle();
	}
 
	private int GetTargetAngle()
	{
		int l_angleTarget = (int) m_rotation-90;
		if(l_angleTarget<0)
		{
			l_angleTarget+=360; 
		}
		return l_angleTarget;
	}
	private void UpdateWheelShield(float p_deltaTime)
	{ 
		for(int idx=0;idx<m_shield.length;idx++)
		{ 
			m_shield[idx].Update(p_deltaTime); 
		}
	}

	public void AnimateShieldEntry()
	{
		for(int idx=0;idx<m_shield.length;idx++)
		{ 
			m_shield[idx].AnimateEntry();
		} 
	}
	public int GetColorIdxHitted(int p_angle)
	{
		return p_angle/m_sectionSize; 
	} 
	
  
	public boolean isHit(Point p_pos)
	{  
		return (p_pos.y-Constants.DOT_RADIUS )<=BOTTOM_WHEEL_Y;
		 
	} 
	
	public boolean IsSameColorHit(int p_DotColor)
	{  
		return BOTTOM_COLOR[m_angleTarget]==p_DotColor;
		 
	} 
	public boolean isShieldHitted(int p_color, Rect p_bounds)
	{
		boolean isHit = false;
		
		for(int idx=0;idx<m_shield.length;idx++)
		{  
			if(m_shield[idx].isHitted(p_bounds) && p_color!=m_shield[idx].getColor())
			{
				isHit=true;
			}
		} 
		return isHit;
	}  
	  
	public int GetSectionColor(int p_idx)
	{
		return  m_colors[p_idx];
	}
	
	public int GetBottomTipRad()
	{ 
		int radAta=(int)m_rotation;   
		
		if(m_direction==DIRECTION.LEFT)
		{
			if(radAta<=90)
			{
				radAta=90-radAta;
			}
			else
			{
				radAta=Math.abs(90-radAta);
				if(radAta>0)
				{
					radAta=360-radAta;
				}
			}
		}
		else
		{ 
			radAta=Math.abs(radAta-360)+90;
			 
			if(radAta>360)
			{
				radAta-=360; 
			} 
		}
		
		return radAta;
	}
   
	public void PaintBottom()
	{
		m_onPaintAnim=true;
		m_paintAngle=GetBottomTipRad(); 
		m_paintSectionIdx=m_paintAngle/m_sectionSize;
		m_paintRad=(float)m_paintEffectRad; 
	}
	public boolean isAnimating()
	{ 
		return m_onPaintAnim || onGlowAnimation(); 
	}
	public void ResetAnim()
	{ 
		  m_onPaintAnim =false;
		  for(int idx=0;idx<m_glowAnim.length;idx++)
		  { 
			  m_glowAnim[idx]=0;
			 }    
	}
	
	public boolean onGlowAnimation()
	{ 
		for(int idx=0;idx<m_glowAnim.length;idx++)
		{ 
			if(m_glowAnim[idx]>0)
			{
				return true; 
			}	
		}   
		return false;
	}

	private void UpdateWheelAnimation(float p_deltaTime) 
	{
		if(m_onPaintAnim) 
		{ 
			m_paintRad -= Constants.WHEEL_PAINT_ANIM_SPEED*(p_deltaTime);
			
			if(m_paintRad<=0)
			{ 
				m_painted[m_paintSectionIdx]  = true; 
				m_onPaintAnim				  = false;
				m_paintSectionIdx			  = -1;
			} 
 
			if(m_paintSectionIdx!=-1)
			{
				if(m_paintRad<m_paintEffectRad/2 && m_glowAnim[m_paintSectionIdx]==0)
				{
					m_glowAnim[m_paintSectionIdx] = 2;
				}
			} 
		}	
		
		for(int idx=0;idx<m_glowAnim.length;idx++)
		{ 
			if(m_glowAnim[idx]>0)
			{
				m_glowAnim[idx]-=(0.045f*(p_deltaTime));
			}	
		}   
	}
 
	public Point getPosition()
	{
		return m_pos;
	}
	
	public void SetPosition(Point p_pos)
	{
		m_pos = p_pos;
		for(int idx=0;idx<m_shield.length;idx++)
		{
			m_shield[idx].SetPostion(new Point(p_pos));
		}
	}
	
	public void SetPosition(int p_posX,int p_posY)
	{
		SetPosition(new Point(p_posX,p_posY));
	}
	public void SetRadius(int p_radius)
	{ 
		m_radius= (int)(p_radius*Constants.SCALE);
		m_paintEffectRad  = m_radius*2;
		
	}   
	
}
