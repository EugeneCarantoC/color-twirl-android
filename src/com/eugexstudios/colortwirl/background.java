package com.eugexstudios.colortwirl;

import java.util.Random;

import android.graphics.Color;

import com.eugexstudios.framework.CONFIG;
import com.eugexstudios.framework.Graphics;
import com.eugexstudios.framework.Input.TouchEvent;
import com.eugexstudios.utils.genSet;

public class background extends Screen{
	private final byte			   	   COLOR_SCHEMES = 6;
	private final byte 			       FLOATER_SIZE  = 17; 
	private float[][] 				   m_floaters;
	private int 			           m_colorIdx;
	private Random m_randomer; 
	
	public enum theme {
		 orange(0)
	    ,red(1)
		,purple(2)
		,blue(3)
		,green(4)
		,yellow(5)
		,cyan(6); 
		 int m_idx;
		theme(int p_idx)
		{
			m_idx= p_idx;
		}
		public int getIdx()
		{
			return m_idx;
		}
		
	}
	private int[] THEME_COLORS_LIGHT = new int[]
	{
			Color.rgb(244,168,119),
			Color.rgb(233,123,128),
			Color.rgb(184,139,184),
			Color.rgb(108,167,244),
			Color.rgb(125,194,137),
			Color.rgb(248,214,109) 
	}; 
	
	private int[] THEME_COLORS = new int[]
	{
			Color.rgb(241,148,86),  //orange
			Color.rgb(226,80,87),   //red
			Color.rgb(169,114,169), //purple
			Color.rgb(77,147,242),  //blue
			Color.rgb(96,181,111),  //green
			Color.rgb(245,196,46)   //Yellow
	};
	public background()
	{
	    m_randomer	    = new Random();
	    SwitchColor();
		m_floaters      = new float[FLOATER_SIZE][8]; 
		
		for(int idx=0;idx<m_floaters.length;idx++)
		{	
			m_floaters[idx][0] = m_randomer.nextInt(CONFIG.SCREEN_WIDTH/50)*50;	// X
			m_floaters[idx][1] = m_randomer.nextInt(CONFIG.SCREEN_HEIGHT/50)*50;	// Y
			m_floaters[idx][2] = m_randomer.nextInt(20)/10+1;				        // Speed X
			m_floaters[idx][3] = m_randomer.nextInt(20)/10+1;				        // Speed Y 
			m_floaters[idx][4] = m_randomer.nextBoolean()?-1:1; 		   			// Dir X
			m_floaters[idx][5] = m_randomer.nextBoolean()?-1:1;  			        // Dir Y
			m_floaters[idx][6] = m_randomer.nextInt(12)*5+15;    		    	    // Radius
			m_floaters[idx][7] = m_randomer.nextInt(8) +3; 				   	      // Opacity
		} 
	}
	@Override
	public void Paint(Graphics g)
	{ 
		g.drawStretchedImage(AssetManager.background_shades
				,0,0, CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT
				,m_colorIdx*20, 0, 20, 1280);// constant talaga yang 1280 dont change!
		
		for(int idx=0;idx<m_floaters.length;idx++)
		{
			g.drawCircle((int)m_floaters[idx][0], (int)m_floaters[idx][1], (int)m_floaters[idx][6],Color.argb(((int)m_floaters[idx][7]), 0, 0, 0)); 
 		}
		
	}
	public int GetThemeColor()
	{
		return THEME_COLORS[m_colorIdx];
	}	
	public int GetThemeColorLight()
	{
		return THEME_COLORS_LIGHT[m_colorIdx];
	}
	public void SwitchColor()
	{ 
		m_colorIdx = m_randomer.nextInt(COLOR_SCHEMES);  
	}
	public void setTheme(theme p_themeOveride)
	{ 
		m_colorIdx = p_themeOveride.getIdx(); 
	}
	

	@Override
	public void Updates(float p_deltaTime)
	{ 
		for(int idx=0;idx<m_floaters.length;idx++)
		{
			m_floaters[idx][0] += m_floaters[idx][4]*m_floaters[idx][2];
			m_floaters[idx][1] += m_floaters[idx][5]*m_floaters[idx][3]; 
			
			if(	m_floaters[idx][0]<=0 || m_floaters[idx][0]>=CONFIG.SCREEN_WIDTH)
			{
				m_floaters[idx][4] =m_floaters[idx][4]==1?-1:1;
				m_floaters[idx][0] += m_floaters[idx][4]*m_floaters[idx][2];
			}	
			if(	m_floaters[idx][1]<=0 || m_floaters[idx][1]>=CONFIG.SCREEN_HEIGHT)
			{
				m_floaters[idx][5] =m_floaters[idx][5]==1?-1:1;
				m_floaters[idx][1] += m_floaters[idx][5]*m_floaters[idx][3]; 
			}
		}
	}

	@Override
	public void TouchUpdates(TouchEvent g) 
	{ 
		
	}

	@Override
	public void backButton() 
	{ 
		
	}

}
