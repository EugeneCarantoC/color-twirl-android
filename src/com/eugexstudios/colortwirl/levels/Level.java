package com.eugexstudios.colortwirl.levels;

import java.util.Vector;

import com.eugexstudios.colortwirl.background.theme; 
import com.eugexstudios.colortwirl.shields.Shield;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.Constants.DIRECTION;

public class Level {

	private int[] 			  		   m_dotColors; 
	private int[] 			   		   m_colors; 
	private theme					   m_themeOveride;
	private float 					   m_wheelSpeed; 
	private int 					   m_wheelRad; 
	private Shield[]				   m_shield;
	private DIRECTION				   m_wheelDirection;

//	static WHEEL_TYPE m_wheelType = WHEEL_TYPE.PRIMARY; 
	
	public Level(int[]  p_colors, DIRECTION p_wheelDir, float p_wheelSpeed, Shield[] p_shield, int[] p_dotColors)
	{
		m_themeOveride   = null;
		m_colors		 = p_colors;
		m_wheelSpeed     = p_wheelSpeed;
		m_wheelDirection = p_wheelDir;
		m_shield	     = p_shield.clone();
		m_dotColors      = p_dotColors.clone();
		m_wheelRad		 = 300;
	}
	public Level(int[]  p_colors, DIRECTION p_wheelDir, float p_wheelSpeed, Vector<Shield> p_shield, int[] p_dotColors)
	{
		m_themeOveride   = null;
		m_colors		 = p_colors;
		m_wheelSpeed     = p_wheelSpeed;
		m_wheelDirection = p_wheelDir;
		m_shield	     = new Shield[p_shield.size()];
		
		for(int idx=0;idx<p_shield.size();idx++)
		{
			m_shield[idx] = p_shield.get(idx);
		}
		m_dotColors      = p_dotColors.clone();
		m_wheelRad		 = 300;
	}
	 
	public Level(int[]  p_colors, DIRECTION p_wheelDir, float p_wheelSpeed,int p_wheelRad, Vector<Shield> p_shield, int[] p_dotColors)
	{ 
		m_themeOveride   = null;
		m_colors		 = p_colors;
		m_wheelSpeed     = p_wheelSpeed;
		m_wheelDirection = p_wheelDir;
		m_shield	     = new Shield[p_shield.size()];
		m_wheelRad		 = p_wheelRad;
		for(int idx=0;idx<p_shield.size();idx++)
		{
			m_shield[idx] = p_shield.get(idx);
		}
		m_dotColors      = p_dotColors.clone();
		m_wheelRad		 = p_wheelRad;
	}
	public Level(int[]  p_colors, DIRECTION p_wheelDir, float p_wheelSpeed,int p_wheelRad, Vector<Shield> p_shield, int[] p_dotColors, theme p_themeOveride)
	{  
		m_themeOveride   = p_themeOveride;
		m_colors		 = p_colors;
		m_wheelSpeed     = p_wheelSpeed;
		m_wheelDirection = p_wheelDir;
		m_shield	     = new Shield[p_shield.size()];
		m_wheelRad		 = p_wheelRad;
		for(int idx=0;idx<p_shield.size();idx++)
		{
			m_shield[idx] = p_shield.get(idx);
		}
		m_dotColors      = p_dotColors.clone();
		m_wheelRad		 = p_wheelRad;
	}
	public Level setBackground(theme p_themeOveride)
	{
		m_themeOveride = p_themeOveride;
		return this;
	}
	
	public boolean hasThemeOveride()
	{
		return m_themeOveride != null; 
	}
 
	public Level()
	{ 
	}
	
	public Level(Level p_level)
	{
		m_colors          = p_level.GetColors();
		m_dotColors       = p_level.GetDotColors().clone();
		m_wheelSpeed      = p_level.GetWheelSpeed();
		m_wheelDirection  = p_level.GetWheelDirection(); 
		m_wheelRad		  = p_level.GetWheelRad();
		m_shield		  = p_level.GetShield();
	}
	public theme getTheme()
	{
		return m_themeOveride;
	}
	public int[] GetColors()
	{
		return m_colors;
	}
	public int[] GetDotColors()
	{
		return m_dotColors;
	}
	
	public float GetWheelSpeed()
	{
		return m_wheelSpeed;
	}
	
	public int GetWheelRad()
	{
		return m_wheelRad;
	}
	
	public DIRECTION GetWheelDirection()
	{
		return m_wheelDirection;
	} 
	
	public int GetWheelColor(int p_idx)
	{
		return m_colors[p_idx];
	}

	public Shield[] GetShield()
	{
		return m_shield.clone();
	}
}
