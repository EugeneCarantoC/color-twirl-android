package com.eugexstudios.colortwirl.levels;

import java.util.Vector; 

import com.eugexstudios.colortwirl.background.theme;
import com.eugexstudios.colortwirl.shields.ClockShield;
import com.eugexstudios.colortwirl.shields.ExpandingShield;
import com.eugexstudios.colortwirl.shields.Shield;
import com.eugexstudios.utils.ColorManager;
import com.eugexstudios.utils.Constants.DIRECTION;

import android.graphics.Color;
  
public class LevelManager { 
	 
	public static int GetMaxLevel()
	{
		return 20;
	}
	public static Level GetLevel(int p_lvl)
	{  
		  switch(p_lvl)
		  { 
//			case 1: return TESTWIN();//GetLevel_001();  // OKS
//			case 1: return GetLevel_001();//GetLevel_001();  // OKS
		  
		  	case 1: return A001a();   // LOCK
		  	case 2: return A001b();   // LOCK
		  	case 3: return A001c();   // LOCK
		  	case 4: return A001d();   // LOCK  
		  	case 5: return A001e();   // LOCK  
		  	case 6: return A001f();   // LOCK  
		  	case 7: return A001g();   // LOCK  
		  	case 8: return A001h();   // LOCK  
		  	case 9: return A001i();   // LOCK  
		  	case 10: return A001j();  // LOCK  
		  	case 11: return RW001d();  
		  	case 12: return RW001c();  
		  	case 13: return RW001a();    
		  	case 14: return RW001b();     
		  	case 15: return MV001a(); 
		  	case 16: return MV001b(); 
		  	case 17: return RW002a();  
		  	case 18: return RW002b(); 

		  	case 19: return MO001a(); 
		  	case 20: return MO001b(); 
		  	 
//		  	case 19: return A001(); 
//		  	case 20: return A002();    
//		  	case 21: return A004();   
//		  	case 22: return A005(); 
//		  	case 23: return A007();  
//		  	case 24: return A008();  
//		  	case 25: return A009(); 
		  	case 26: return A0010();   
		  	case 27: return A0011();  
		  	case 28: return A0012();  
		  	case 29: return A0013();  
		  	case 30: return A0014(); 
		  	case 31: return A0015();   
		  	case 32: return A0016();  
		  	case 33: return A0017();   
		  	case 34: return A0018(); 
		  	case 35: return A0019();  
		  	case 36: return A0020();  

//		  	case 1: return GetLevel_blue_sdgfdf();   
//		  	case 2: return GetLevel_Orange_asdf();  
//		  	case 2: return GetLevel_Violet_gsd();   
//		  	case 2: return GetLevel_Violet_fsg();   
//		  	case 2: return GetLevel_Violet_asdf();   
//		  	case 2: return GetLevel_GREEN_asdf();   
//		  	case 2: return GetLevel_RED_RWA();  
//		  	case 2: return GetLevel_RED_REW();  
//		  	case 2: return GetLevel_RED_sdf();  
//		  	case 2: return GetLevel_GREEN_FAS();  
//		  	case 3: return GetLevel_GREEN_asd();  
		  	
//		  	case 2: return GetLevel_002(); // OKS 
//		  	case 3: return GetLevel_003();  // OKS
//		  	case 4: return GetLevel_001a();   // OKS
//			case 5: return GetLevel_005();   // OKS
//		  	case 6: return GetLevel_006();   //OKS
//			case 7: return GetLevel_008(); //OKS 
//		  	case 8: return GetLevel_004();  // OK
//		  	case 9: return  GetLevel_009b();   // OK
//		  	case 10: return GetLevel_015asdasd();  // OK 
//		  	case 11: return GetLevel_012();   // OK 
//		  	case 12: return GetLevel_019();  // OK 
//		  	case 13: return GetLevel_020();      // OK  
//		  	case 14: return  GetLevel_009s();// OK
//		  	case 15: return GetLevel_0sdfsdf();   // OK 
//		  	case 16: return GetLevel_014();    // OK 
//		  	case 17: return GetLevel_016();    // OK 
//		  	case 18: return GetLevel_022();     // OK  
//			case 19: return   GetLevel_013();  // OK   
//			case 20: return   GetLevel_017();   // OK    
//			case 21: return  GetLevel_014a ();   // OK   
//			case 22: return GetLevel_024();    // OK   
//			case 23: return  GetLevel_022b();   // OK   
//			case 24: return   GetLevel_025();   // OK   
//			case 25: return   GetLevel_026(); // OK   
//			case 26: return GetLevel_027();    // OK   
//			case 27: return  GetLevel_023();   // OK   
//			case 28: return  GetLevel_028();    // OK   
//			case 29: return  GetLevel_asdsF() ;    // OK   
//			case 30: return  GetLevel_0ADASF();    // old
//			case 31: return  GetLevel_234asd ();     //old 
//			case 32: return  GetLevel_0agfd();     
//			case 33: return   GetLevel_we346s ();     
//			case 34: return  GetLevel_gfdf ();       
//			case 35: return  GetLevel_wewrs();      
//			case 36: return  GetLevel_werwerw();    
//			case 37: return  GetLevel_dsfsdf();     //mini
//			
//			case 38: return  GetLevel_asdfds();      
//			case 39: return  GetLevel_YELLOW_ASD1();     
//			case 40: return  GetLevel_BLUE_ASD2();      
//			case 41: return  GetLevel_BLUE_ASD3();      
			default:return TESTWIN(); 
//			default:return   null;
		  }  
	}
	private static Level TESTFAIL()
	{  
		float l_wheelSpeed= 0.0001f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
//		WHEEL_TYPE l_colors = WHEEL_TYPE.PRIMARY;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		Vector<Shield> l_shield = new Vector<Shield>();
		 
		int[] l_dotColors = new int[]{
		 ColorManager.RED
		};
		
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	private static Level TESTWIN()
	{  
		float l_wheelSpeed= 0.04f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		Vector<Shield> l_shield = new Vector<Shield>();
		 
		int[] l_dotColors = new int[]{
				ColorManager.YELLOW
				};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	
	private static Level GetLevel_024()
	{  
		float l_wheelSpeed= 0.8f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;  
		 int l_colors[] = new int[]{
		   ColorManager.GREEN
		 , ColorManager.YELLOW
		 , ColorManager.ORANGE
		 , ColorManager.RED
		 , ColorManager.VIOLET
		 , ColorManager.BLUE
		 };
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		
		l_shield.add(
				new Shield( 
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);

		l_shield.add(
				new Shield( 
						  shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.3f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 		 
				
		   
		int[] l_dotColors = new int[]{
		  ColorManager.RED 
		, ColorManager.BLUE   
		, ColorManager.RED  
		, ColorManager.BLUE  
		, ColorManager.ORANGE  
		, ColorManager.VIOLET  
		, ColorManager.ORANGE  
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}

	private static Level GetLevel_001()
	{  
		float l_wheelSpeed= 0.55f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		 
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.7f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
 
 
		int[] l_dotColors = new int[]{
		  ColorManager.YELLOW
		 , ColorManager.BLUE 
		, ColorManager.RED  
		};
		 
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors).setBackground(theme.orange);
		
	} 
	
	private static Level GetLevel_001a()
	{  
		float l_wheelSpeed= 0.55f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		 
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 
		
		l_shield.add(
				new Shield(
						   0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.6f
						, ColorManager.RED
						, DIRECTION.LEFT)
				); 
		
		  shieldBlockSize = 360/8;
		  
		l_shield.add(
				new Shield(
						   0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
		
		l_shield.add(
				new Shield(
						   shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 


 
		int[] l_dotColors = new int[]{
 
		 ColorManager.RED
		, ColorManager.RED
		, ColorManager.BLUE
		, ColorManager.BLUE
		, ColorManager.YELLOW
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
 
	private static Level GetLevel_003()
	{  
		float l_wheelSpeed= 0.55f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };  
		
		Vector<Shield> l_shield = new Vector<Shield>();
		
		int shieldBlockSize = 360/8;

		l_shield.add(
				new ClockShield(
						   shieldBlockSize*2 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 90, 25, 1.8f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*6 -shieldBlockSize/2 //p_sweepStartAngle
						, shieldBlockSize //p_arcSize
						, 300 +50 //p_radius
						, (int)((300+50)*0.93f) //p_shellEndRadius 
						, ColorManager.DEFAULT_SHIELD_COLOR //p_direction
						, DIRECTION.LEFT //p_sweepPerTick
						, false
						, 90 //p_tickDuration
						, 25 // p_clockRotateSpeed
						, 1.8f //p_moveImmediate
						)
				
				
//				 int 
//					, int 
//					, int 
//					, int  
//					, int 
//					, DIRECTION 
//					, float 
//					, float 
//					, float 
//					, boolean 
				);
 
		 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize*4 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.RED
						, DIRECTION.LEFT, false, 90, 25, 1.8f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*8 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.BLUE
						, DIRECTION.LEFT, false, 90, 25, 1.8f)
				);
 
 
		int[] l_dotColors = new int[]{ 
		  ColorManager.BLUE
		, ColorManager.RED
		, ColorManager.BLUE  
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
 
	private static Level GetLevel_009s()
	{  
		float l_wheelSpeed= 0.75f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		Vector<Shield> l_shield = new Vector<Shield>();
		
		int shieldBlockSize = 360/10;

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*1
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 45, 50, 3.8f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*3  
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 45, 50, 3.8f)
				);

		shieldBlockSize = 360/8;
		l_shield.add(
				new Shield(
						  shieldBlockSize*6  
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.702f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT) 
				);
		l_shield.add(
				new Shield(
						  shieldBlockSize*6  
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.6999f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT) 
				);
		int[] l_dotColors = new int[]{ 
		  ColorManager.BLUE
		, ColorManager.RED
		, ColorManager.YELLOW  
		, ColorManager.ORANGE  
		, ColorManager.GREEN  
		, ColorManager.ORANGE 
		, ColorManager.BLUE 
		, ColorManager.ORANGE 
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
 
 
	private static Level LEVEL_TESTA()
	{  
		float l_wheelSpeed= 0.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 }; 
		int shieldBlockSize = 360/60;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		 

		int[] l_dotColors = new int[]{
		 ColorManager.YELLOW
		};
		 
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	 
	private static Level GetLevel_016()
	{  
		float l_wheelSpeed= 0.78f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT; 
		 
		 int l_colors[] = new int[]{
		   ColorManager.GREEN
		 , ColorManager.YELLOW
		 , ColorManager.ORANGE
		 , ColorManager.RED
		 , ColorManager.VIOLET
		 , ColorManager.BLUE
		 };
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/3;
 
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.8f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);
		
		shieldBlockSize = 360/12;
		 
		l_shield.add(
				new Shield(
						  shieldBlockSize
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
				);
		 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*7
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
				);
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);
		 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*8
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);
		 

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);
		 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*6
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);
		 
		   
		int[] l_dotColors = new int[]{
		  ColorManager.RED
		, ColorManager.GREEN  
		, ColorManager.BLUE  
		, ColorManager.RED 
		, ColorManager.ORANGE   
		, ColorManager.BLUE    
		, ColorManager.RED
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	
	private static Level GetLevel_017()
	{  
		float l_wheelSpeed= 0.79f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/8;

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*1 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 90, 25, 2.1f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*5 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 90, 25, 2.1f)
				);
 
		 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize*3 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 90, 25, 2.1f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*7 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 90, 25, 2.1f)
				);
 
		   
		int[] l_dotColors = new int[]{  
		  ColorManager.VIOLET 
		, ColorManager.GREEN 
		, ColorManager.ORANGE 
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	
	
	private static Level GetLevel_023()
	{  
		float l_wheelSpeed= 0.79f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/6;

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*1 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT,false, 180, 70, 2.1f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*4 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 180, 70, 2.1f)
				);
 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize*4 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 360, 140, 2.1f)
				);
 
 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize*1 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 360, 140, 2.1f)
				);
 
 
  
		   
		int[] l_dotColors = new int[]{  
		  ColorManager.RED    
		, ColorManager.BLUE    
		, ColorManager.YELLOW  
		, ColorManager.RED   
		, ColorManager.GREEN    
		, ColorManager.ORANGE   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	private static Level GetLevel_014a()
	{  
		float l_wheelSpeed= 0.79f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/6;

		l_shield.add(
				new ClockShield( 
						  shieldBlockSize*1 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 180, 50, 2.1f)
				);
 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*4 -shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 180, 50, 2.1f)
				);
 
 
  
		   
		int[] l_dotColors = new int[]{  
		  ColorManager.VIOLET    
		, ColorManager.GREEN    
		, ColorManager.ORANGE  
		, ColorManager.VIOLET   
		, ColorManager.GREEN    
		, ColorManager.ORANGE   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	
	private static Level GetLevel_002()
	{  
		float l_wheelSpeed= 0.8f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT; 
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);

		l_shield.add(
				new Shield(
						  shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.8f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 		 
				
		   
		int[] l_dotColors = new int[]{
		  ColorManager.RED 
		, ColorManager.BLUE   
		, ColorManager.RED  
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	private static Level GetLevel_020()
	{  
		float l_wheelSpeed= 0.8f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		

	 
		
		shieldBlockSize = 360/10;	
		l_shield.add(
				new Shield(
						  360-shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.8f
						, ColorManager.GREEN
						, DIRECTION.LEFT) 
				); 		
l_shield.add(
		new Shield(
				  360-shieldBlockSize/2
				, shieldBlockSize 
				, 300 +50
				, (int)((300+50)*0.93f) 
				, 1.9f
				, ColorManager.GREEN
				, DIRECTION.LEFT) 
		); 
		l_shield.add(
				new ClockShield(
						  360-shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 180, 25, 2.1f)
				);

		l_shield.add(
				new ClockShield(
						  360-shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT,false, 180, 30, 2.1f)
				); 
		

	 
		int[] l_dotColors = new int[]{
		  ColorManager.GREEN 
		, ColorManager.VIOLET    
		, ColorManager.ORANGE   
		, ColorManager.GREEN   
		, ColorManager.ORANGE   
		, ColorManager.GREEN   
		, ColorManager.ORANGE   
		, ColorManager.GREEN   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	 
	private static Level GetLevel_004()
	{  
		float l_wheelSpeed= 0.6f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };   
		Vector<Shield> l_shield = new Vector<Shield>();
		int shieldBlockSize = 360/12; 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
					);

		  shieldBlockSize = 360/10; 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.0f
						, ColorManager.BLUE
						, DIRECTION.RIGHT)
					);
		 
		  shieldBlockSize = 360/8; 
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.1f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
			
		  shieldBlockSize = 360/6; 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.2f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
					);
		 
		int[] l_dotColors = new int[]{
		  ColorManager.RED 
		, ColorManager.BLUE   
		, ColorManager.RED   
		, ColorManager.BLUE  
		, ColorManager.RED   
		, ColorManager.RED   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors); 		
	}

	private static Level GetLevel_005()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		int shieldBlockSize = 360/4;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.3f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				);
		
		l_shield.add(
						new Shield(
								  0
								, shieldBlockSize/2
								, 300 +50
								, (int)((300+50)*0.93f) 
								, 2.0f
								, ColorManager.RED
								, DIRECTION.RIGHT)
						); 
 
		int[] l_dotColors = new int[]{
		  ColorManager.RED
		, ColorManager.BLUE   
		, ColorManager.RED   
		, ColorManager.YELLOW  
		, ColorManager.RED    
		, ColorManager.YELLOW  
		, ColorManager.BLUE 
		, ColorManager.RED    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}

	private static Level GetLevel_006()
	{   

		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };   
		Vector<Shield> l_shield = new Vector<Shield>();
		int shieldBlockSize = 360/12; 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.9f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
					);
		  
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.0f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
			 
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.1f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
			
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.2f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
 
			int[] l_dotColors = new int[]{
			  ColorManager.YELLOW
			, ColorManager.BLUE    
			, ColorManager.RED   
			, ColorManager.YELLOW  
			, ColorManager.BLUE    
			, ColorManager.RED     
			};
			return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors); 	}

	private static Level GetLevel_022()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
	
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.6f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  shieldBlockSize*3
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.6f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.65f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  shieldBlockSize*3
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.65f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		int[] l_dotColors = new int[]{
		  ColorManager.BLUE   
		, ColorManager.YELLOW   
		, ColorManager.RED    
		, ColorManager.GREEN   
		, ColorManager.RED   
		, ColorManager.VIOLET 
		, ColorManager.RED   
		, ColorManager.GREEN   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	
	private static Level GetLevel_022b()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
	
		int shieldBlockSize = 360/12;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 

		l_shield.add(
				new ClockShield(
						  shieldBlockSize*6+shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 30, 70, 4.5f)
				); 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT, false, 30, 70, 4.5f)
				); 
	 
		l_shield.add(
				new ClockShield(
						 shieldBlockSize*6+shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 30, 70, 4.5f)
				); 
		l_shield.add(
				new ClockShield(
						  shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT, false, 30, 70, 4.5f)
				); 
	 
		shieldBlockSize= 360/6;
		l_shield.add(
				new Shield(
						  shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.8f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT) 
				); 
	 
		l_shield.add(
				new Shield(
						  shieldBlockSize*3+shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.8f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT) 
				);  
		
		 
		int[] l_dotColors = new int[]{
		  ColorManager.YELLOW   
		, ColorManager.GREEN   
		, ColorManager.RED    
		, ColorManager.GREEN   
		, ColorManager.BLUE   
		, ColorManager.VIOLET 
		, ColorManager.GREEN    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	
	private static Level GetLevel_009b()
	{   
		float l_wheelSpeed= 0.71f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		l_shield.add(
				new Shield(
						  shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.34f
						, ColorManager.BLUE
						, DIRECTION.LEFT)
				);	
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*4+shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.34f
						, ColorManager.RED
						, DIRECTION.LEFT)
				);

		  shieldBlockSize = 360/3;
			l_shield.add(
					new Shield(
							  shieldBlockSize*5+shieldBlockSize/2
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.0f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);	
			 
		
		int[] l_dotColors = new int[]{
				   
					  ColorManager.BLUE   
					, ColorManager.YELLOW   
					, ColorManager.BLUE   
					, ColorManager.RED   
					, ColorManager.YELLOW   
					, ColorManager.RED    
					, ColorManager.YELLOW   
					, ColorManager.BLUE   
				};
				return new Level( l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
				
	}
	
	private static Level GetLevel_008()
	{   
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;

		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
 		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		  
		int shieldBlockSize2 = 360/4;

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.8f
						, ColorManager.BLUE
						, DIRECTION.LEFT)
				); 

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize2 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.1f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize2*2
						, shieldBlockSize2
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.1f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
		int[] l_dotColors = new int[]{
		  ColorManager.BLUE
		, ColorManager.BLUE   
		, ColorManager.RED    
		, ColorManager.BLUE   
		, ColorManager.BLUE   
		, ColorManager.RED    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	 
	
	private static Level GetLevel_025()
	{   
		float l_wheelSpeed= 0.8f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };

 		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		  
		int shieldBlockSize2 = 360/4;

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.9f
						, ColorManager.GREEN
						, DIRECTION.RIGHT)
				); 

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize2 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.2f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize2*2
						, shieldBlockSize2
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.2f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		
		int[] l_dotColors = new int[]{
		  ColorManager.GREEN
		, ColorManager.GREEN   
		, ColorManager.GREEN    
		, ColorManager.GREEN   
		, ColorManager.GREEN   
		, ColorManager.GREEN    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	 
	
	 
	 
	private static Level GetLevel_009()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;

		int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.RED
				 , ColorManager.BLUE
				 };
		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		l_shield.add(
				new Shield(
						  10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
		 
		l_shield.add(
				new Shield(
						  shieldBlockSize*2+10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);  

		 
		l_shield.add(
				new Shield(
						  shieldBlockSize*4+10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				);  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);  

	   shieldBlockSize = 360/4;
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.4f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				); 
 
		int[] l_dotColors = new int[]{
		  ColorManager.RED 
		, ColorManager.YELLOW    
		, ColorManager.BLUE   
		, ColorManager.RED   
		, ColorManager.RED   
		, ColorManager.BLUE  
		, ColorManager.RED    
		, ColorManager.YELLOW   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors) ;
		
	}
	
	
	private static Level GetLevel_026()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };

		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();  
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				);  
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				);  

		   
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				);  

	   shieldBlockSize = 360/4;
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.4f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
 
		int[] l_dotColors = new int[]{
		  ColorManager.RED 
		, ColorManager.YELLOW    
		, ColorManager.BLUE   
		, ColorManager.RED   
		, ColorManager.GREEN    
		, ColorManager.ORANGE   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors) ;
		
	}
	 
	private static Level GetLevel_021()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.YELLOW_ORANGE
				 , ColorManager.ORANGE
				 , ColorManager.RED_ORANGE
				 , ColorManager.RED
				 , ColorManager.RED_VIOLET
				 , ColorManager.VIOLET
				 , ColorManager.BLUE_VIOLET
				 , ColorManager.BLUE
				 , ColorManager.BLUE_GREEN
				 , ColorManager.GREEN
				 , ColorManager.YELLOW_GREEN
				 }; 

		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		l_shield.add(
				new Shield(
						  10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.YELLOW_ORANGE
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.ORANGE
						, DIRECTION.RIGHT)
				); 
		
		
		


		l_shield.add(
				new Shield(
						  shieldBlockSize*2+10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.RED_VIOLET
						, DIRECTION.RIGHT)
				); 
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				);  

		 
		l_shield.add(
				new Shield(
						  shieldBlockSize*4+10
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
				);  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.15f
						, ColorManager.BLUE_VIOLET
						, DIRECTION.RIGHT)
				);  

	   shieldBlockSize = 360/20;
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.4f
						, ColorManager.GREEN
						, DIRECTION.RIGHT)
				); 
 
		int[] l_dotColors = new int[]{
		  ColorManager.YELLOW   
		, ColorManager.RED   
		, ColorManager.BLUE   
		, ColorManager.RED   
		, ColorManager.RED   
		, ColorManager.BLUE  
		, ColorManager.RED    
		, ColorManager.YELLOW   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors) ;
		
	}
	  
	private static Level GetLevel_013()
	{  
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };

		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 
		
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.9f
							, ColorManager.ORANGE
							, DIRECTION.RIGHT)
					); 

			l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.9f
							, ColorManager.ORANGE
							, DIRECTION.RIGHT)
					); 

			 shieldBlockSize = 360/4;
				l_shield.add(
						new Shield(
								  0
								, shieldBlockSize
								, 300 +50
								, (int)((300+50)*0.93f) 
								, 0.7f
								, ColorManager.DEFAULT_SHIELD_COLOR
								, DIRECTION.RIGHT)
						); 		
				
				l_shield.add(
								new Shield(
										  shieldBlockSize*2
										, shieldBlockSize
										, 300 +50
										, (int)((300+50)*0.93f) 
										, 0.7f
										, ColorManager.DEFAULT_SHIELD_COLOR
										, DIRECTION.RIGHT)
								); 

 
		int[] l_dotColors = new int[]{ 
		  ColorManager.RED   
		, ColorManager.YELLOW  
		, ColorManager.GREEN   
		, ColorManager.RED  
		, ColorManager.RED    
		, ColorManager.GREEN  
		, ColorManager.GREEN   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors) ;
		
	}
	
	private static Level GetLevel_012()
	{   
		float l_wheelSpeed= 0.52f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		 
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/6; 
		 
		l_shield.add(
				new Shield(
						  shieldBlockSize*1
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.35f
						, ColorManager.ORANGE
						, DIRECTION.RIGHT)
				); 
  
		l_shield.add(
				new Shield(
						  shieldBlockSize*1
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.35f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				); 
 

		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.25f
						, ColorManager.RED
						, DIRECTION.RIGHT)
				); 
  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.25f
						, ColorManager.RED
						, DIRECTION.LEFT)
				); 
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.75f
						, ColorManager.GREEN
						, DIRECTION.RIGHT)
				); 
  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.75f
						, ColorManager.GREEN
						, DIRECTION.LEFT)
				); 
		
 
		int[] l_dotColors = new int[]{ 
		  ColorManager.VIOLET 
		, ColorManager.GREEN  
		, ColorManager.ORANGE    
		, ColorManager.YELLOW  
		, ColorManager.VIOLET  
		, ColorManager.RED   
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors); 		
	}
	 
	private static Level GetLevel_015asdasd()
	{  
		float l_wheelSpeed= 0.71f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		 
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.6f
						, ColorManager.GREEN
						, DIRECTION.LEFT)
				); 
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.66f
						, ColorManager.GREEN
						, DIRECTION.LEFT)
				); 
		
		  shieldBlockSize = 360/8;
		  
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 


 
		int[] l_dotColors = new int[]{
 
	     ColorManager.RED  
		, ColorManager.GREEN   
		, ColorManager.VIOLET  
		, ColorManager.ORANGE   
		, ColorManager.GREEN   
		, ColorManager.ORANGE   
		, ColorManager.GREEN 
		};
		return new Level( l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	} 
	private static Level GetLevel_027()
	{   
		float l_wheelSpeed= 0.61f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		l_shield.add(
				new Shield(
						  shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.34f
						, ColorManager.VIOLET
						, DIRECTION.LEFT)
				);	
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*4+shieldBlockSize/2
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.34f
						, ColorManager.VIOLET
						, DIRECTION.LEFT)
				);

		  shieldBlockSize = 360/3;
			l_shield.add(
					new Shield(
							  shieldBlockSize*5+shieldBlockSize/2
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 2.2f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);	
			 
		
		int[] l_dotColors = new int[]{
				  
				  ColorManager.ORANGE   
				, ColorManager.GREEN   
				, ColorManager.ORANGE   
				, ColorManager.GREEN 
				, ColorManager.VIOLET 
				, ColorManager.ORANGE   
				, ColorManager.VIOLET  
				};
				return new Level( l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
				
	}
	
	private static Level GetLevel_028()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;

		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		int shieldBlockSize = (int)( 360);
		
		Vector<Shield> l_shield = new Vector<Shield>();
	 
		  shieldBlockSize = (int)( 360/2);
		l_shield.add(
				new ClockShield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.ORANGE
						, DIRECTION.RIGHT, false, 180, 150, 1.8f)
				);
		l_shield.add(
				new Shield(
						  shieldBlockSize*1
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.54f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
				);	
		int[] l_dotColors = new int[]{
				  
				  ColorManager.ORANGE   
				, ColorManager.BLUE   
				, ColorManager.GREEN   
				, ColorManager.RED 
				, ColorManager.ORANGE 
				, ColorManager.RED   
				, ColorManager.BLUE  
				, ColorManager.ORANGE 
				};
				return new Level( l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
				
	}
	
	
	private static Level GetLevel_asdsF()
	{   
		float l_wheelSpeed= 0.91f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  260; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		
		 int shieldBlockSize = (int)( 360/8);
		  

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							,  1.24f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							,  1.24f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							,  1.24f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							,  1.24f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT) 
					);
		 shieldBlockSize = (int)( 360/6);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							,  1.54f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT) 
					);
		int[] l_dotColors = new int[]{

				   ColorManager.RED    
				  ,ColorManager.YELLOW    
				  ,ColorManager.BLUE    
				  ,ColorManager.RED    
				  ,ColorManager.YELLOW    
				  ,ColorManager.BLUE   
				  ,ColorManager.YELLOW    
				  ,ColorManager.BLUE    
				  ,ColorManager.RED    
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	 
	private static Level GetLevel_234asd()
	{  
		float l_wheelSpeed= 0.42f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		int l_colors[] = new int[]{
				   ColorManager.RED
				 , ColorManager.YELLOW
				 , ColorManager.GREEN
				 };
		int l_wheelSize =  200; 
		int shieldBlockSize = 360/6;
		theme l_background =  theme.green;
		Vector<Shield> l_shield = new Vector<Shield>(); 
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.GREEN
						, DIRECTION.LEFT) 
				);  
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.RED
						, DIRECTION.LEFT) 
				);  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 0.9f
						, ColorManager.YELLOW
						, DIRECTION.LEFT) 
				);  
 
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*0
						, shieldBlockSize
						, 300 +20
						, (int)((300+20)*0.93f) 
						, 0.8f
						, ColorManager.GREEN
						, DIRECTION.LEFT) 
				);  
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*2
						, shieldBlockSize
						, 300 +20
						, (int)((300+20)*0.93f) 
						, 0.8f
						, ColorManager.RED
						, DIRECTION.LEFT) 
				);  
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +20
						, (int)((300+20)*0.93f) 
						, 0.8f
						, ColorManager.YELLOW
						, DIRECTION.LEFT) 
				);  
 
		int[] l_dotColors = new int[]{
 
		 ColorManager.YELLOW
		, ColorManager.GREEN
		, ColorManager.YELLOW
		, ColorManager.GREEN
		, ColorManager.YELLOW
		, ColorManager.GREEN  
		, ColorManager.YELLOW
		, ColorManager.GREEN 
		};
		 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(l_background);
		
	} 
	private static Level GetLevel_we346s()
	{  
		float l_wheelSpeed= 1.42f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		int l_colors[] = new int[]{
				   ColorManager.BLUE
				 , ColorManager.ORANGE
				 , ColorManager.YELLOW
				 };
		int l_wheelSize =  200; 
		int shieldBlockSize = 360/6;
		
		Vector<Shield> l_shield = new Vector<Shield>(); 
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.6f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				); 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*3
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.6f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				); 
		
		  shieldBlockSize = 360/8;
		  
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 
		
		
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 

		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize
						, l_wheelSize +80
						, (int)((l_wheelSize+80)-25) 
						, 1.0f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				); 

		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, l_wheelSize +80
						, (int)((l_wheelSize+80)-25) 
						, 1.0f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				); 

		  shieldBlockSize = 360/3;
		l_shield.add(
				new Shield(
						  shieldBlockSize*4
						, shieldBlockSize
						, l_wheelSize +115
						, (int)((l_wheelSize+115)-25) 
						, 1.7f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				); 


 
		int[] l_dotColors = new int[]{
 
		 ColorManager.ORANGE
		, ColorManager.YELLOW
		, ColorManager.BLUE
		, ColorManager.BLUE
		, ColorManager.YELLOW
		, ColorManager.ORANGE 
		, ColorManager.YELLOW
		, ColorManager.BLUE
		, ColorManager.ORANGE 
		};
		 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
		
	} 
 
	private static Level GetLevel_wewrs()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  260; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, false, 180, 150, 0.81f)
					);	 

		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-25)  
							, ColorManager.GREEN
							, DIRECTION.RIGHT, false, 180, 150, 0.81f)
					);	 
		 shieldBlockSize = (int)( 360/5 );
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*5-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25)  
							, ColorManager.GREEN
							, DIRECTION.LEFT, false, 180, 150, 0.81f)
					);	 

		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*5-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT, false, 180, 150, 0.81f)
					);	 

		  
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.VIOLET  
				, ColorManager.GREEN    
				, ColorManager.RED  
				, ColorManager.GREEN    
				, ColorManager.ORANGE   
				, ColorManager.VIOLET
				, ColorManager.GREEN  
				, ColorManager.ORANGE    
				, ColorManager.VIOLET    
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	
	private static Level GetLevel_BLUE_ASD2()
	{   
		float l_wheelSpeed= 1.11f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				  ColorManager.BLUE
				 , ColorManager.BLUE_GREEN
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );

		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +30
							, (int)((l_wheelSize+30)-25) 
							, 0.84f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +30
							, (int)((l_wheelSize+30)-25) 
							, 0.84f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 


		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +60
							, (int)((l_wheelSize+60)-25) 
							, 0.84f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 

 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.BLUE 
				, ColorManager.BLUE       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	} 
	private static Level GetLevel_BLUE_ASD3()
	{   
		float l_wheelSpeed= 1.31f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				  ColorManager.BLUE
				 , ColorManager.BLUE_GREEN
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/5 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.94f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.84f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.94f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 


		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.94f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.84f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.94f
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT)
					);	 


 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.BLUE 
				, ColorManager.BLUE       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}
	private static Level GetLevel_GREEN_asd()
	{   
		float l_wheelSpeed= 0.71f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN
				 , ColorManager.WHITE
				 , ColorManager.GREEN 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  230; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/5 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.44f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.44f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	
	private static Level GetLevel_GREEN_FAS()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN
				 , ColorManager.YELLOW_GREEN
				 , ColorManager.GREEN 
				 , ColorManager.YELLOW_GREEN 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/2 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.44f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	
		 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25) 
							, 1.44f
							, ColorManager.GREEN
							, DIRECTION.LEFT)
					);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	
	
	
	private static Level GetLevel_RED_REW()
	{   
		float l_wheelSpeed= 0.71f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.RED
				 , ColorManager.WHITE
				 , ColorManager.RED 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  160; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1  
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 1.44f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 1.44f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*5
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 1.44f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	} 
	
	private static Level GetLevel_Violet_asdf()
	{   
		float l_wheelSpeed= 1.51f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET
				 , ColorManager.WHITE
				 , ColorManager.VIOLET 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  100; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-25) 
							, 0.64f
							, ColorManager.VIOLET
							, DIRECTION.RIGHT)
					);	

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-25) 
							, 0.64f
							, ColorManager.VIOLET
							, DIRECTION.RIGHT)
					);	

		 
		   
		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	 
	private static Level GetLevel_blue_sdgfdf()
	{   
		float l_wheelSpeed= 1.5f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.WHITE
				 , ColorManager.BLUE_GREEN
				 , ColorManager.WHITE 
				 , ColorManager.BLUE_GREEN 
				 }; 
		int l_wheelSize =  230; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1 
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-25)  
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT, false, 45, 50, 1.0f)
					);	 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +35
							, (int)((l_wheelSize+35)-25)  
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT, false, 45, 50, 1.0f)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +65
							, (int)((l_wheelSize+65)-25) 
							, 1.1f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT) 
					);	 
		 shieldBlockSize = (int)( 360/6 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +65
							, (int)((l_wheelSize+65)-25) 
							, 1.1f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT) 
					);	 
		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	
	private static Level A001b()
	{   
		float l_wheelSpeed= 0.55f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.YELLOW_1
				 , ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 , ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 , ColorManager.GREEN_2
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		   
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-35) 
							, 0.0f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT)
					);
					
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*3-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +120
										, (int)((l_wheelSize+120)-35) 
										, 0.0f
										, ColorManager.GREEN_2
										, DIRECTION.LEFT)

								);
 
 	 
		int[] l_dotColors = new int[]
		{ 
				   	  ColorManager.GREEN_2  
					, ColorManager.YELLOW_1  
					, ColorManager.GREEN_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	
	private static Level A001c()
	{   
		float l_wheelSpeed= 0.6f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.YELLOW_1
				 , ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 , ColorManager.GREEN_2 
				 }; 
		int l_wheelSize =  250; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		   
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-35) 
							, 0.3f
							, ColorManager.GREEN_2
							, DIRECTION.RIGHT)
					);
					
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*3-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.3f
										, ColorManager.GREEN_2
										, DIRECTION.RIGHT)

								);
 
 	 
		int[] l_dotColors = new int[]
		{ 
				   	  ColorManager.GREEN_2  
					, ColorManager.GREEN_2    
					, ColorManager.GREEN_2    
					, ColorManager.GREEN_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	} 
	
	private static Level A001a()
	{   
		float l_wheelSpeed= 0.50f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON
				 , ColorManager.ORANGE_2
				 , ColorManager.MAROON  
				 ,  ColorManager.ORANGE_2  
				 }; 
		int l_wheelSize =  250; 
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = (int)( 360/6 );
 
 
		int[] l_dotColors = new int[]
		{ 
					ColorManager.MAROON     
					, ColorManager.MAROON        
			 		,ColorManager.ORANGE_2   
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	private static Level A001i()
	{   
		float l_wheelSpeed= 0.72f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_3
				 , ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 }; 
		int l_wheelSize =  210; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*3-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.45f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								);
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*1-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.45f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								);
		 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*1-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.75f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								);
		 
		 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.GREEN_3 
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3   
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3      
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3      
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}
	
	private static Level A001j()
	{   
		float l_wheelSpeed= 0.72f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_3
				 , ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 }; 
		int l_wheelSize =  170; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/5 );
		 
	 
		 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*1-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +100
										, (int)((l_wheelSize+100)-35) 
										, 0.75f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								); 
		  
		 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*4-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +100
										, (int)((l_wheelSize+100)-35) 
										, 0.75f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								); 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*4-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +170
										, (int)((l_wheelSize+170)-40) 
										, 0.95f
										, ColorManager.BLUE_GREEN
										, DIRECTION.LEFT)

								); 
		 
		 
		 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.BLUE_GREEN 
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3   
					, ColorManager.BLUE_GREEN   
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3    
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}

	private static Level RW001a()
	{   
		float l_wheelSpeed= 0.75f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.WHITE
				 , ColorManager.MAROON_1 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
	 
		 shieldBlockSize = (int)( 360/6 );
 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-45) 
							, 1.50f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)
					);

					 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*5 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-45) 
							, 1.50f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)			
		
		);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-45) 
							, 1.50f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)			
		
		);	 
		int[] l_dotColors = new int[]
		{ 
				  	  ColorManager.WHITE 
						, ColorManager.MAROON_1   
						, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	

	private static Level RW001b()
	{   
		float l_wheelSpeed= 0.85f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.RED_3
				 , ColorManager.WHITE
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  210; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/8 );


		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0-shieldBlockSize/2 +90
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.RED_3
							, DIRECTION.LEFT, false, 45 , 120, 1.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*2-shieldBlockSize/2+90
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, false, 45 , 120, 1.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4-shieldBlockSize/2+90
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.RED_3
							, DIRECTION.LEFT, false, 45 , 120, 1.8f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*6-shieldBlockSize/2+90
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, false, 45 , 120, 1.8f)
					);	
		 
		 
		  
		int[] l_dotColors = new int[]
		{ 
				  	  ColorManager.RED_3 
					, ColorManager.RED_3       
					, ColorManager.RED_3        
					, ColorManager.RED_3      
					, ColorManager.RED_3       
					, ColorManager.RED_3       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level RW002a()
	{   
		float l_wheelSpeed= 0.78f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.RED_3
				 , ColorManager.WHITE
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  235; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/6 );

		 l_shield.add(
					new Shield(
							  shieldBlockSize*3 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-40) 
							, 1.14f
							, ColorManager.RED_3
							, DIRECTION.LEFT)			
		
		);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-40) 
							, 0.54f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT)
					);

		 l_shield.add(
					new Shield(
							  shieldBlockSize*2 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-40) 
							, 0.54f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT)
					);

					 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-40) 
							, 0.54f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT)			
		
		);	 
 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.RED_3       
					, ColorManager.RED_3     
					, ColorManager.RED_3      
					, ColorManager.RED_3      
					, ColorManager.RED_3         
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	private static Level RW002b()
	{   
		float l_wheelSpeed= 0.79f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.RED_3
				 , ColorManager.WHITE
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 , ColorManager.RED_3 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  215; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/4 );

		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*2 -shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.RED_3
							, DIRECTION.RIGHT, false, 45 , 100, 1.5f)
					);	
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0 -shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40)  
							, ColorManager.RED_3
							, DIRECTION.RIGHT, false, 45 , 100, 1.5f)
					);	
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.WHITE       
					, ColorManager.WHITE    
					, ColorManager.WHITE    
					, ColorManager.WHITE            
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level MV001a()
	{   
		float l_wheelSpeed= 0.9f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.MAROON_2
				 , ColorManager.MAROON_1  
				 ,  ColorManager.MAROON_2 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-40) 
							, 0.74f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-40) 
							, 0.84f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-40) 
							, 0.94f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);

//		 shieldBlockSize = (int)( 360/2 );
//		 l_shield.add(
//					new Shield(
//							  shieldBlockSize*1
//							, shieldBlockSize 
//							, l_wheelSize +50
//							, (int)((l_wheelSize+50)-35) 
//							, 1.74f
//							, ColorManager.MAROON_2
//							, DIRECTION.LEFT) 
//					);
//		 
		int[] l_dotColors = new int[]
		{ 
				      ColorManager.MAROON_2 
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2         
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	private static Level MV001b()
	{   
		float l_wheelSpeed= 0.9f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_2
				 , ColorManager.MAROON_1
				 , ColorManager.MAROON_2  
				 ,  ColorManager.MAROON_1 
				 }; 
		int l_wheelSize =  175; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +105
							, (int)((l_wheelSize+105)-40) 
							, 0.94f
							, ColorManager.MAROON_2
							, DIRECTION.RIGHT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +105
							, (int)((l_wheelSize+105)-40) 
							, 1.04f
							, ColorManager.MAROON_2
							, DIRECTION.RIGHT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +105
							, (int)((l_wheelSize+105)-40) 
							, 1.14f
							, ColorManager.MAROON_2
							, DIRECTION.RIGHT) 
					);

//		 shieldBlockSize = (int)( 360/2 );
//		 l_shield.add(
//					new Shield(
//							  shieldBlockSize*1
//							, shieldBlockSize 
//							, l_wheelSize +50
//							, (int)((l_wheelSize+50)-35) 
//							, 1.74f
//							, ColorManager.MAROON_2
//							, DIRECTION.LEFT) 
//					);
//		 
		int[] l_dotColors = new int[]
		{ 
				      ColorManager.MAROON_2 
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1     
					, ColorManager.MAROON_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
 	
	private static Level MO001a()
	{   
		float l_wheelSpeed= 0.95f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1 
				 }; 
		int l_wheelSize =  180; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +60
							, (int)((l_wheelSize+60)-35) 
							, 0.0f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT)
					);	

		 shieldBlockSize = (int)( 360/8 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +125
							, (int)((l_wheelSize+125)-40) 
							, 1.4f
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +125
							, (int)((l_wheelSize+125)-40) 
							, 1.44f
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT)
					);	 
		 
	 
 	 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE_1 
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	
	
	private static Level MO001b()
	{   
		float l_wheelSpeed= 0.95f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1 
				 }; 
		int l_wheelSize =  220; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +65
							, (int)((l_wheelSize+65)-40) 
							, 0.0f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)
					);	

		 shieldBlockSize = (int)( 360/8 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40) 
							, 1.4f
							, ColorManager.ORANGE_1
							, DIRECTION.RIGHT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40) 
							, 1.44f
							, ColorManager.ORANGE_1
							, DIRECTION.RIGHT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40) 
							, 1.48f
							, ColorManager.ORANGE_1
							, DIRECTION.RIGHT)
					);	 
		 
	 
 	 
		int[] l_dotColors = new int[]
		{ 
				  	ColorManager.MAROON_1 
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1     
					, ColorManager.MAROON_1   
					, ColorManager.MAROON_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	private static Level RW001c()
	{   
		float l_wheelSpeed= 0.75f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.WHITE
				 , ColorManager.MAROON_1 
				 , ColorManager.WHITE 
				 , ColorManager.MAROON_1 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/9 );

		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0 
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-40) 
							, 1.00f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)
					);

					 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4 
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-40) 
							, 1.00f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)			
		
		);	 
		  

		 shieldBlockSize = (int)( 360/8 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +150
							, (int)((l_wheelSize+150)-40) 
							, 1.14f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)		 
		
		);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*5 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +150
							, (int)((l_wheelSize+150)-40) 
							, 1.14f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)		 
		
		);	 
		int[] l_dotColors = new int[]
		{ 
				  	  ColorManager.MAROON_1 
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1      
					, ColorManager.MAROON_1      
					, ColorManager.MAROON_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	private static Level MW001a()
	{   
		float l_wheelSpeed= 0.85f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.WHITE
				 , ColorManager.MAROON_1 
				 , ColorManager.WHITE  
				 }; 
		int l_wheelSize =  150; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/6 );
		  
		 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1 
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-40)  
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT, false, 360*2.5f, 650, 1.5f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4 
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-40)  
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT, false, 360*2.5f, 650, 1.5f)
					);	
		int[] l_dotColors = new int[]
		{ 
				  	  ColorManager.MAROON_1 
					, ColorManager.MAROON_1       
					, ColorManager.WHITE        
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level RW001d()
	{   
		float l_wheelSpeed= 0.73f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.WHITE
				 , ColorManager.MAROON_1 
				 , ColorManager.WHITE  
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-40)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, false, 360*3, 650, 1.6f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*3-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-40)  
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, false, 360*3, 650, 1.6f)
					);	
		 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1 
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-40)  
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT, false, 360*3, 650, 1.6f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4 
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-40)  
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT, false, 360*3, 650, 1.6f)
					);	
		int[] l_dotColors = new int[]
		{ 
				  	  ColorManager.MAROON_1 
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1       
					, ColorManager.MAROON_1        
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level A001e()
	{   
		float l_wheelSpeed= 0.66f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_2
				 , ColorManager.YELLOW_1
				 , ColorManager.GREEN_2  
				 ,  ColorManager.YELLOW_1
				 , ColorManager.GREEN_2  
				 ,  ColorManager.YELLOW_1
				 }; 
		int l_wheelSize =  260; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		 
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*3-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.25f
										, ColorManager.GREEN_2
										, DIRECTION.RIGHT)

								);
		 l_shield.add(
				 new Shield(
						 				shieldBlockSize*1-shieldBlockSize/2
										, shieldBlockSize 
										, l_wheelSize +70
										, (int)((l_wheelSize+70)-35) 
										, 0.25f
										, ColorManager.GREEN_2
										, DIRECTION.RIGHT)

								);
		 
		 
		int[] l_dotColors = new int[]
		{ 
					 ColorManager.GREEN_2 
					, ColorManager.YELLOW_1   
					, ColorManager.GREEN_2   
					, ColorManager.YELLOW_1   
					, ColorManager.GREEN_2      
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	private static Level A001d()
	{    
		float l_wheelSpeed= 0.65f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] { 
				   ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 ,  ColorManager.GREEN_2  
				 , ColorManager.YELLOW_1  
				 }; 
		int l_wheelSize =  220; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-35) 
							, 0.44f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT) 
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-35) 
							, 0.44f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT) 
					);	
		 
		int[] l_dotColors = new int[]
		{ 
					 ColorManager.YELLOW_1  
					, ColorManager.YELLOW_1   
					, ColorManager.YELLOW_1     
					, ColorManager.GREEN_2     
					, ColorManager.GREEN_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	 
	 
	private static Level A005()
	{   
		float l_wheelSpeed= 1.3f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET_1
				 , ColorManager.VIOLET_2
				 , ColorManager.VIOLET_1  
				 ,  ColorManager.VIOLET_2
				 , ColorManager.VIOLET_1  
				 ,  ColorManager.VIOLET_2
				 }; 
		int l_wheelSize =  175; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 ); 
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +65
							, (int)((l_wheelSize+65)-30)  
							, ColorManager.VIOLET_1
							, DIRECTION.LEFT, false, 360*6, 950, 1.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*2-shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +65
							, (int)((l_wheelSize+65)-30)  
							, ColorManager.VIOLET_1
							, DIRECTION.LEFT, false, 360*6, 950, 1.8f)
					);	

		 shieldBlockSize = (int)( 300); 
		 l_shield.add(
					new Shield(
							  120
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-35) 
							, 0.0f
							, ColorManager.VIOLET_2
							, DIRECTION.LEFT)
					);	
 	 
		int[] l_dotColors = new int[]
		{ 
					ColorManager.VIOLET_2 
					, ColorManager.VIOLET_2  
					, ColorManager.VIOLET_2  
					, ColorManager.VIOLET_2  
					, ColorManager.VIOLET_2  
					, ColorManager.VIOLET_2  
					, ColorManager.VIOLET_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.purple);
				
	}
	
	
	
	private static Level A002()
	{   
		float l_wheelSpeed= 1.2f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_2
				 , ColorManager.WHITE
				 , ColorManager.MAROON_2  
				 ,  ColorManager.WHITE
				 , ColorManager.MAROON_2  
				 ,  ColorManager.WHITE
				 }; 
		int l_wheelSize =  230; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 ); 
	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-40) 
							, 0.64f
							, ColorManager.WHITE
							, DIRECTION.RIGHT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-40) 
							, 0.64f
							, ColorManager.MAROON_2
							, DIRECTION.RIGHT) 
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40) 
							, 1.24f
							, ColorManager.MAROON_2
							, DIRECTION.RIGHT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-40) 
							, 1.24f
							, ColorManager.WHITE
							, DIRECTION.RIGHT) 
					); 
		int[] l_dotColors = new int[]
		{ 
				  	  	 ColorManager.MAROON_2 
						, ColorManager.MAROON_2   
						, ColorManager.MAROON_2   
						, ColorManager.WHITE 
						, ColorManager.WHITE  
						, ColorManager.WHITE   
						, ColorManager.MAROON_2   
						, ColorManager.MAROON_2    
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	 
	private static Level A001f()
	{   
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1
				 }; 
		int l_wheelSize =  240; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 ); 
	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35) 
							, 0.64f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35) 
							, 0.64f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT) 
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-35) 
							, 0.24f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-35) 
							, 0.24f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);
		 
//		 
		int[] l_dotColors = new int[]
		{ 
				  	  	 ColorManager.MAROON_1 
						, ColorManager.MAROON_1   
						, ColorManager.ORANGE_1   
						, ColorManager.MAROON_1 
						, ColorManager.ORANGE_1  
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	 
	private static Level A001g()
	{   
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.ORANGE_1
				 , ColorManager.MAROON_2
				 , ColorManager.ORANGE_1  
				 ,  ColorManager.MAROON_2
				 , ColorManager.ORANGE_1  
				 ,  ColorManager.MAROON_2
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		  
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*1
								, shieldBlockSize 
								, l_wheelSize +65
								, (int)((l_wheelSize+65)-35)  
								, ColorManager.MAROON_2
								, DIRECTION.LEFT, false, 90, 600, 0.7f)
						);	
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*2 +shieldBlockSize/2
								, shieldBlockSize 
								, l_wheelSize +65
								, (int)((l_wheelSize+65)-35)  
								, ColorManager.MAROON_2
								, DIRECTION.LEFT, false, 90, 600,  0.7f)
						);	
			 
			  
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*0 +shieldBlockSize/4
								, shieldBlockSize 
								, l_wheelSize +105
								, (int)((l_wheelSize+115)-45)  
								, ColorManager.ORANGE_1
								, DIRECTION.RIGHT, false, 90, 600, 0.7f)
						);	
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*1+shieldBlockSize/2+shieldBlockSize/4
								, shieldBlockSize 
								, l_wheelSize +105
								, (int)((l_wheelSize+115)-45)  
								, ColorManager.ORANGE_1
								, DIRECTION.RIGHT, false, 90, 600, 0.7f)
						);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE_1 
					, ColorManager.ORANGE_1   
					, ColorManager.MAROON_2   
					, ColorManager.MAROON_2   
					, ColorManager.ORANGE_1    
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors,theme.orange) ;
				
	}
	
	private static Level A001h()
	{   
		float l_wheelSpeed= 0.7f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 ); 
	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35) 
							, 0.22f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35) 
							, 0.22f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT) 
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-35) 
							, 0.74f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-35) 
							, 0.74f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT) 
					);
		 
//		 
		int[] l_dotColors = new int[]
		{ 
				  	  	 ColorManager.MAROON_1 
						, ColorManager.MAROON_1   
						, ColorManager.ORANGE_1   
						, ColorManager.MAROON_1 
						, ColorManager.ORANGE_1  
						, ColorManager.ORANGE_1  
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	private static Level A0017()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET_3
				 , ColorManager.YELLOW_1
				 , ColorManager.VIOLET_3  
				 ,  ColorManager.YELLOW_1
				 , ColorManager.VIOLET_3  
				 ,  ColorManager.YELLOW_1
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30) 
							, 1.84f
							, ColorManager.VIOLET_3
							, DIRECTION.LEFT) 
					);	
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30) 
							, 0.64f
							, ColorManager.VIOLET_3
							, DIRECTION.LEFT) 
					);	
		 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +110
							, (int)((l_wheelSize+110)-30) 
							, 1.84f
							, ColorManager.VIOLET_3
							, DIRECTION.RIGHT) 
					);	
		 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.VIOLET_3 
					, ColorManager.VIOLET_3     
					, ColorManager.VIOLET_3     
					, ColorManager.VIOLET_3     
					, ColorManager.VIOLET_3   
					, ColorManager.VIOLET_3       
					, ColorManager.YELLOW_1         
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.purple);
				
	}	   
	private static Level A004()
	{   
		float l_wheelSpeed= 1.2f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_1
				 , ColorManager.YELLOW_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.YELLOW_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.YELLOW_2
				 }; 
		int l_wheelSize =  220; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/2 ); 
	 
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-40) 
							, 1.64f
							, ColorManager.GREEN_1
							, DIRECTION.RIGHT, 0.5f, 200)
					);	
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-40) 
							, 1.64f
							, ColorManager.YELLOW_2
							, DIRECTION.RIGHT, 0.5f,200)
					);	
	 
		  
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN_1 
					, ColorManager.GREEN_1    
					, ColorManager.YELLOW_2    
					, ColorManager.YELLOW_2
					, ColorManager.GREEN_1
					, ColorManager.GREEN_1
					, ColorManager.GREEN_1   
					, ColorManager.YELLOW_2
					, ColorManager.YELLOW_2   
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	
	private static Level A0011()
	{   
		float l_wheelSpeed= 0.78f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_3
				 , ColorManager.RED_3
				 , ColorManager.YELLOW_3  
				 ,  ColorManager.BLUE_3 
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		     
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-40) 
							, 0.69f
							, ColorManager.GREEN_3
							, DIRECTION.RIGHT,0.4f, 400).setRoundEnd(false)
					);
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-40) 
							, 0.69f
							, ColorManager.RED_3
							, DIRECTION.RIGHT, 0.4f, 400).setRoundEnd(false)
					);
		 
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-40) 
							, 0.69f
							, ColorManager.YELLOW_3
							, DIRECTION.RIGHT, 0.4f, 400).setRoundEnd(false)
					);
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-40) 
							, 0.69f
							, ColorManager.BLUE_3
							, DIRECTION.RIGHT, 0.4f, 400).setRoundEnd(false)
					);
	 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.BLUE_3 
				, ColorManager.YELLOW_3    
			    , ColorManager.RED_3     
			    , ColorManager.RED_3       
			    , ColorManager.BLUE_3       
			    , ColorManager.YELLOW_3       
			    , ColorManager.RED_3       
				 
		};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}
	private static Level A0019()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_3
				 , ColorManager.RED_3
				 , ColorManager.YELLOW_3  
				 ,  ColorManager.BLUE_3 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.BLUE_3
							, DIRECTION.LEFT, false, 90, 15, 2.1f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.BLUE_3
							, DIRECTION.RIGHT, false, 90, 15, 2.1f)
					);	
	 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.YELLOW_3
							, DIRECTION.LEFT, false, 90, 15, 2.1f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.YELLOW_3
							, DIRECTION.RIGHT, false, 90, 15, 2.1f)
					);

		 shieldBlockSize = (int)( 360/3 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-35) 
							, 2.64f
							, ColorManager.RED_3
							, DIRECTION.RIGHT) 
					);
	 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.YELLOW_3 
				, ColorManager.YELLOW_3    
			    , ColorManager.YELLOW_3    
			    , ColorManager.RED_3       
			    , ColorManager.RED_3       
			    , ColorManager.RED_3       
			    , ColorManager.BLUE_3       
				 
		};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}
	
	
	private static Level A0013()
	{   
		float l_wheelSpeed= 1.2f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.ORANGE_1
				 , ColorManager.MAROON_2
				 , ColorManager.ORANGE_1  
				 ,  ColorManager.MAROON_2
				 , ColorManager.ORANGE_1  
				 ,  ColorManager.MAROON_2
				 }; 
		int l_wheelSize =  170; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		  
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*1
								, shieldBlockSize 
								, l_wheelSize +65
								, (int)((l_wheelSize+65)-30)  
								, ColorManager.MAROON_2
								, DIRECTION.LEFT, false, 90, 50, 1.5f)
						);	
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*2 +shieldBlockSize/2
								, shieldBlockSize 
								, l_wheelSize +65
								, (int)((l_wheelSize+65)-30)  
								, ColorManager.MAROON_2
								, DIRECTION.LEFT, false, 90, 50, 1.5f)
						);	
			 
			  
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*0 +shieldBlockSize/4
								, shieldBlockSize 
								, l_wheelSize +105
								, (int)((l_wheelSize+105)-30)  
								, ColorManager.ORANGE_1
								, DIRECTION.RIGHT, false, 90, 50, 1.5f)
						);	
			 l_shield.add(
						new ClockShield(
								  shieldBlockSize*1+shieldBlockSize/2+shieldBlockSize/4
								, shieldBlockSize 
								, l_wheelSize +105
								, (int)((l_wheelSize+105)-30)  
								, ColorManager.ORANGE_1
								, DIRECTION.RIGHT, false, 90, 50, 1.5f)
						);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE_1 
					, ColorManager.ORANGE_1   
					, ColorManager.MAROON_2   
					, ColorManager.ORANGE_1    
					, ColorManager.MAROON_2   
					, ColorManager.ORANGE_1    
					, ColorManager.MAROON_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors,theme.orange) ;
				
	}
	private static Level A009()
	{   
		float l_wheelSpeed= 1.0f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.MAROON_1
				 , ColorManager.ORANGE_1
				 , ColorManager.MAROON_1  
				 ,  ColorManager.ORANGE_1 
				 }; 
		int l_wheelSize =  180; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30) 
							, 0.0f
							, ColorManager.ORANGE_1
							, DIRECTION.LEFT)
					);	

		 shieldBlockSize = (int)( 360/8 ); 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-30) 
							, 1.3f
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT)
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-30) 
							, 1.3f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-30) 
							, 1.3f
							, ColorManager.MAROON_1
							, DIRECTION.RIGHT)
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*6
							, shieldBlockSize 
							, l_wheelSize +95
							, (int)((l_wheelSize+95)-30) 
							, 1.3f
							, ColorManager.MAROON_1
							, DIRECTION.LEFT)
					);	
	 
 	 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE_1 
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1   
					, ColorManager.ORANGE_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	}
	
	private static Level A008()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_1
				 , ColorManager.YELLOW_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.YELLOW_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.YELLOW_2
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 320); 
		  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-35) 
							, 1.64f
							, ColorManager.GREEN_1
							, DIRECTION.LEFT, 0.5f,200)
					);	
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +120
							, (int)((l_wheelSize+120)-30) 
							, 1.64f
							, ColorManager.GREEN_1
							, DIRECTION.RIGHT, 0.5f,200)
					);	
//		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN_1 
					, ColorManager.YELLOW_2 
					, ColorManager.GREEN_1 
					, ColorManager.YELLOW_2 
					, ColorManager.YELLOW_2    
					, ColorManager.GREEN_1    
					, ColorManager.YELLOW_2     
					, ColorManager.GREEN_1    
					, ColorManager.YELLOW_2    
					, ColorManager.YELLOW_2       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	private static Level A0018()
	{   
		float l_wheelSpeed= 1.3f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_1
				 , ColorManager.GREEN_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.GREEN_2
				 , ColorManager.GREEN_1  
				 ,  ColorManager.GREEN_2
				 }; 
		int l_wheelSize =  220; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		 
		  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-40) 
							, 1.94f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, 0.9f,300)
					);	
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-40) 
							, 1.94f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT, 0.9f,300)
					);	 
		 shieldBlockSize = (int)( 360/2);
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +115
							, (int)((l_wheelSize+115)-30) 
							, 1.94f
							, ColorManager.GREEN_1
							, DIRECTION.LEFT, 1.9f,100)
					);	

		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +115
							, (int)((l_wheelSize+115)-30) 
							, 1.94f
							, ColorManager.GREEN_1
							, DIRECTION.LEFT, 1.9f,100)
					);	
		int[] l_dotColors = new int[]
		{ 
				      ColorManager.GREEN_1
					, ColorManager.GREEN_1  
					, ColorManager.GREEN_2
					, ColorManager.GREEN_1
					, ColorManager.GREEN_1
					, ColorManager.GREEN_2 
					, ColorManager.GREEN_1
					, ColorManager.GREEN_2     
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	
	
	
	private static Level A007()
	{   
		float l_wheelSpeed= 0.9f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.YELLOW_1
				 , ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 ,  ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 ,  ColorManager.GREEN_2
				 }; 
		int l_wheelSize =  180; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		   
			  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-35) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT, 0.8f,50)
					);	
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-35) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT, 0.8f,50)
					);	

		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-35) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.LEFT, 0.8f,50)
					);	

		 shieldBlockSize = (int)( 360/4 )-20;
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3+shieldBlockSize/4+50
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 1.8f
							, ColorManager.SHIELD_1
							, DIRECTION.LEFT) 
					);	
//		 
//		 .SetAsClockType(90, 50, 2.8f)
		int[] l_dotColors = new int[]
		{  
				  ColorManager.GREEN_2
				, ColorManager.GREEN_2
				, ColorManager.GREEN_2
				, ColorManager.GREEN_2
				, ColorManager.YELLOW_1  
				
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors,theme.blue);
				
	}
	private static Level A001()
	{   
		float l_wheelSpeed= 0.9f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.YELLOW_1
				 , ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 ,  ColorManager.GREEN_2
				 , ColorManager.YELLOW_1  
				 ,  ColorManager.GREEN_2
				 }; 
		int l_wheelSize =  170; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		   
			  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.RIGHT, 0.8f,50)
					);	
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.RIGHT, 0.8f,50)
					);	

		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 1.8f
							, ColorManager.GREEN_2
							, DIRECTION.RIGHT, 0.8f,50)
					);	

		 shieldBlockSize = (int)( 360/4 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3+shieldBlockSize/4
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 1.8f
							, ColorManager.SHIELD_1
							, DIRECTION.RIGHT) 
					);	
//		 
//		 .SetAsClockType(90, 50, 2.8f)
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN_2 
					, ColorManager.GREEN_2 
					, ColorManager.GREEN_2 
					, ColorManager.GREEN_2 
					, ColorManager.GREEN_2 
					, ColorManager.GREEN_2  
					, ColorManager.GREEN_2   
//					ColorManager.YELLOW_1   // SUPER HARD!
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	private static Level A0016()
	{   
		float l_wheelSpeed= 1.3f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.LIGHT_ORANGE_1
				 , ColorManager.ORANGE_2
				 , ColorManager.LIGHT_ORANGE_1  
				 ,  ColorManager.ORANGE_2
				 , ColorManager.LIGHT_ORANGE_1  
				 ,  ColorManager.ORANGE_2
				 }; 
		int l_wheelSize =  210; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		 
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-30) 
							, 0.74f
							, ColorManager.ORANGE_2
							, DIRECTION.LEFT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-30) 
							, 0.74f
							, ColorManager.ORANGE_2
							, DIRECTION.LEFT) 
					);	

		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +75
							, (int)((l_wheelSize+75)-30) 
							, 0.74f
							, ColorManager.ORANGE_2
							, DIRECTION.LEFT) 
					);	
		 

		 shieldBlockSize=(int)(360/2);
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +135
							, (int)((l_wheelSize+135)-30)  
							, ColorManager.ORANGE_2
							, DIRECTION.LEFT, false, 360*4, 200, 1.5f)
					);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE_2 
					, ColorManager.ORANGE_2 
					, ColorManager.ORANGE_2 
					, ColorManager.ORANGE_2 
					, ColorManager.ORANGE_2 
					, ColorManager.LIGHT_ORANGE_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	private static Level A0015()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET_1
				 , ColorManager.VIOLET_2
				 , ColorManager.VIOLET_1  
				 ,  ColorManager.VIOLET_2 
				 }; 
		int l_wheelSize =  140; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		 
		  
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 0.84f
							, ColorManager.VIOLET_2
							, DIRECTION.LEFT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +85
							, (int)((l_wheelSize+85)-30) 
							, 0.94f
							, ColorManager.VIOLET_2
							, DIRECTION.LEFT) 
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +113
							, (int)((l_wheelSize+113)-30) 
							, 1.64f
							, ColorManager.VIOLET_1
							, DIRECTION.LEFT) 
					);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +113
							, (int)((l_wheelSize+113)-30) 
							, 1.84f
							, ColorManager.VIOLET_1
							, DIRECTION.LEFT) 
					);
		 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.VIOLET_1 
					, ColorManager.VIOLET_2   
					, ColorManager.VIOLET_1   
					, ColorManager.VIOLET_2   
					, ColorManager.VIOLET_1   
					, ColorManager.VIOLET_2   
					, ColorManager.VIOLET_1     
					, ColorManager.VIOLET_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.purple);
				
	}
	private static Level A0020()
	{   
		float l_wheelSpeed= 1.2f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.CYAN_2
				 , ColorManager.BLUEGREEN_1
				 , ColorManager.CYAN_2   
				 , ColorManager.BLUEGREEN_1   
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 300 );
		  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-25) 
							, 1.54f
							, ColorManager.BLUEGREEN_1
							, DIRECTION.LEFT, 1.14f,50)
					);	
 shieldBlockSize = (int)( 220 );
 l_shield.add(
			new Shield(
					  shieldBlockSize*1
					, shieldBlockSize 
					, l_wheelSize +90
					, (int)((l_wheelSize+90)-25) 
					, 1.24f
					, ColorManager.SHIELD_2
					, DIRECTION.RIGHT) 
			);	
 
 l_shield.add(
			new Shield(
					  shieldBlockSize*1
					, shieldBlockSize 
					, l_wheelSize +125
					, (int)((l_wheelSize+125)-25) 
					, 1.44f
					, ColorManager.BLUEGREEN_1
					, DIRECTION.LEFT) 
			);	
//		 
		int[] l_dotColors = new int[]
		{ 
					  ColorManager.BLUEGREEN_1 
					, ColorManager.CYAN_2     
					, ColorManager.BLUEGREEN_1
					, ColorManager.BLUEGREEN_1
					, ColorManager.BLUEGREEN_1   
					, ColorManager.CYAN_2      
					, ColorManager.BLUEGREEN_1    
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	private static Level A0010()
	{   
		float l_wheelSpeed= 0.8f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET_5
				 , ColorManager.YELLOW_1
				 , ColorManager.VIOLET_5  
				 ,  ColorManager.YELLOW_1
				 , ColorManager.VIOLET_5  
				 ,  ColorManager.YELLOW_1
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		   
		 shieldBlockSize = (int)( 300 );
		 l_shield.add(
					new ClockShield(
							  280
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-30)  
							, ColorManager.VIOLET_5
							, DIRECTION.RIGHT, false, 360*10, 200, 1.3f)
					);	
//		 
		int[] l_dotColors = new int[]
		{  
				    ColorManager.YELLOW_1 
				  , ColorManager.YELLOW_1  
				  , ColorManager.VIOLET_5  
				  , ColorManager.VIOLET_5  
				  , ColorManager.VIOLET_5  
				  , ColorManager.YELLOW_1  
				  , ColorManager.YELLOW_1  
				  , ColorManager.YELLOW_1       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.purple);
				
	}
	private static Level A003()
	{   
		float l_wheelSpeed= 0.86f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.PINK_1
				 , ColorManager.YELLOW_1
				 , ColorManager.PINK_1  
				 ,  ColorManager.YELLOW_1
				 , ColorManager.PINK_1  
				 ,  ColorManager.YELLOW_1
				 }; 
		int l_wheelSize =  170; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360  );
		 
		  
		 l_shield.add(
					new ExpandingShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-30) 
							, 0.64f
							, ColorManager.PINK_1
							, DIRECTION.LEFT, 1.5f, 200) 
					);	

		 shieldBlockSize = (int)( 260  );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+90)-30) 
							, 0.94f
							, ColorManager.YELLOW_1
							, DIRECTION.RIGHT) 
					);	
//		 
		int[] l_dotColors = new int[]
		{ 
				  	ColorManager.PINK_1 
					, ColorManager.PINK_1     
					, ColorManager.YELLOW_1     
					, ColorManager.YELLOW_1     
					, ColorManager.PINK_1       
					, ColorManager.PINK_1     
				  
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.green);
				
	}
	private static Level A0014()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN_3
				 , ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 , ColorManager.GREEN_3  
				 ,  ColorManager.BLUE_GREEN
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		   
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.BLUE_GREEN
							, DIRECTION.RIGHT, false, 90, 50, 2.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.BLUE_GREEN
							, DIRECTION.LEFT, false, 90, 50, 2.8f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.GREEN_3
							, DIRECTION.RIGHT, false, 90, 50, 2.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-30)  
							, ColorManager.GREEN_3
							, DIRECTION.LEFT, false, 90, 50, 2.8f)
					);	
		 
		int[] l_dotColors = new int[]
		{ 
					 ColorManager.GREEN_3 
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3   
					, ColorManager.BLUE_GREEN   
					, ColorManager.GREEN_3   
					, ColorManager.BLUE_GREEN     
					, ColorManager.BLUE_GREEN     
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.blue);
				
	}
	private static Level A0012()
	{   
		float l_wheelSpeed= 1.1f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.ORANGE
				 , ColorManager.RED_ORANGE
				 , ColorManager.ORANGE  
				 ,  ColorManager.RED_ORANGE
				 , ColorManager.ORANGE  
				 ,  ColorManager.RED_ORANGE
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/6 );
		   
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*6
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35)  
							, ColorManager.RED_ORANGE
							, DIRECTION.RIGHT, false, 90, 150, 1.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*6
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+95)-35)  
							, ColorManager.RED_ORANGE
							, DIRECTION.LEFT, false, 90, 150, 1.8f)
					);	
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +90
							, (int)((l_wheelSize+95)-35)  
							, ColorManager.ORANGE
							, DIRECTION.RIGHT, false, 90, 150, 1.8f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +55
							, (int)((l_wheelSize+55)-35)  
							, ColorManager.ORANGE
							, DIRECTION.LEFT, false, 90, 150, 1.8f)
					);	
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE 
					, ColorManager.ORANGE  
					, ColorManager.RED_ORANGE  
					, ColorManager.RED_ORANGE  
					, ColorManager.RED_ORANGE  
					, ColorManager.RED_ORANGE   
					, ColorManager.ORANGE      
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.orange);
				
	} 
	private static Level GetLevel_Orange_asdf()
	{   
		float l_wheelSpeed= 2.0f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.ORANGE
				 , ColorManager.WHITE
				 , ColorManager.ORANGE 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  100; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/15 );
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1 
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25)  
							, ColorManager.ORANGE
							, DIRECTION.RIGHT, false, 30, 50, 1.5f)
					);	 
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*5 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25)  
							, ColorManager.ORANGE
							, DIRECTION.RIGHT, false, 30, 50, 1.5f)
					);	 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*10 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25)  
							, ColorManager.ORANGE
							, DIRECTION.RIGHT, false, 30, 50, 1.5f)
					);	 
		 
///////////

		 shieldBlockSize = (int)( 360/6 );
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*1  
							, shieldBlockSize 
							, l_wheelSize +130
							, (int)((l_wheelSize+130)-25)  
							, ColorManager.ORANGE
							, DIRECTION.LEFT, false, 30, 50, 1.5f)
					);	 
		  
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*3 
							, shieldBlockSize 
							, l_wheelSize +130
							, (int)((l_wheelSize+130)-25)  
							, ColorManager.ORANGE
							, DIRECTION.LEFT, false, 30, 50, 1.5f)
					);	 
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*5  
							, shieldBlockSize 
							, l_wheelSize+130
							, (int)((l_wheelSize+130)-25)  
							, ColorManager.ORANGE
							, DIRECTION.LEFT, false, 30, 50, 1.5f)
					);	  
		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	
	private static Level GetLevel_Violet_gsd()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.VIOLET
				 , ColorManager.WHITE
				 , ColorManager.VIOLET 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  150; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/4 );
		 
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +110
							, (int)((l_wheelSize+110)-25)  
							, ColorManager.VIOLET
							, DIRECTION.RIGHT, false, 180, 100, 2.1f)
					);	
		 l_shield.add(
					new ClockShield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +110
							, (int)((l_wheelSize+110)-25)  
							, ColorManager.VIOLET
							, DIRECTION.RIGHT, false, 90, 100, 2.1f)
					);	

		 shieldBlockSize = (int)( 360/2 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +150
							, (int)((l_wheelSize+150)-25) 
							, 1.14f
							, ColorManager.VIOLET
							, DIRECTION.RIGHT) 
					);	

		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	private static Level GetLevel_Violet_fsg()
	{   
		float l_wheelSpeed= 0.61f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.WHITE
				 , ColorManager.BLUE_VIOLET
				 , ColorManager.WHITE 
				 , ColorManager.BLUE_VIOLET 
				 }; 
		int l_wheelSize =  120; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +110
							, (int)((l_wheelSize+110)-25) 
							, 1.64f
							, ColorManager.BLUE_VIOLET
							, DIRECTION.RIGHT)
					);	

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1 +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +140
							, (int)((l_wheelSize+140)-25) 
							, 1.64f
							, ColorManager.BLUE_VIOLET
							, DIRECTION.RIGHT)
					);	

		 
		   
		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	private static Level GetLevel_GREEN_asdf()
	{   
		float l_wheelSpeed= 1.81f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				   ColorManager.GREEN
				 , ColorManager.YELLOW_GREEN
				 , ColorManager.GREEN 
				 , ColorManager.YELLOW_GREEN 
				 }; 
		int l_wheelSize =  190; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/3 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.44f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	
		 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0   +shieldBlockSize/2
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 1.64f
							, ColorManager.YELLOW_GREEN
							, DIRECTION.RIGHT)
					);	
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*2  
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 1.64f
							, ColorManager.YELLOW_GREEN
							, DIRECTION.RIGHT)
					);	
		 

		 shieldBlockSize = (int)( 360/6 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*0  
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.74f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	 
		   
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3  
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.74f
							, ColorManager.GREEN
							, DIRECTION.RIGHT)
					);	 
		   
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	private static Level GetLevel_RED_RWA()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[] {
				   ColorManager.RED_VIOLET
				 , ColorManager.WHITE
				 , ColorManager.RED_VIOLET 
				 , ColorManager.WHITE 
				 }; 
		int l_wheelSize =  110; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/6 );
		  
		 shieldBlockSize = (int)( 360/8 );
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1  
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.44f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.44f
							, ColorManager.RED_VIOLET
							, DIRECTION.RIGHT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*5
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.44f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.44f
							, ColorManager.RED_VIOLET
							, DIRECTION.RIGHT)
					);	 
		 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN 
				, ColorManager.GREEN       
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.yellow);
				
	}
	private static Level GetLevel_YELLOW_ASD1()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				  ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.YELLOW_ORANGE
				 }; 
		int l_wheelSize =  240; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/4 );

		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.04f
							, ColorManager.RED
							, DIRECTION.LEFT)
					);	 


		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.04f
							, ColorManager.RED
							, DIRECTION.LEFT)
					);	 
		 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*0
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25) 
							, 1.24f
							, ColorManager.RED
							, DIRECTION.LEFT)
					);	 


		 l_shield.add(
					new Shield(
							  shieldBlockSize*2
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25) 
							, 1.24f
							, ColorManager.RED
							, DIRECTION.LEFT)
					);	 
		 
 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.YELLOW_ORANGE 
					, ColorManager.YELLOW_ORANGE     
					, ColorManager.RED     
					, ColorManager.YELLOW_ORANGE     
					, ColorManager.RED     
					, ColorManager.YELLOW_ORANGE   
					, ColorManager.RED      
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level GetLevel_asdfds()
	{   
		float l_wheelSpeed= 1.21f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				  ColorManager.YELLOW
				 , ColorManager.YELLOW_ORANGE
				 , ColorManager.ORANGE 
				 }; 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/3 );

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.64f
							, ColorManager.ORANGE
							, DIRECTION.LEFT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25) 
							, 1.64f
							, ColorManager.ORANGE
							, DIRECTION.RIGHT)
					);	 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE 
				, ColorManager.YELLOW    
				, ColorManager.YELLOW_ORANGE   
				, ColorManager.ORANGE     
				, ColorManager.YELLOW_ORANGE   
				, ColorManager.ORANGE     
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level GetLevel_dsfsdf()
	{   
		float l_wheelSpeed= 1.11f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[] {
				  ColorManager.YELLOW
				 , ColorManager.YELLOW_ORANGE
				 , ColorManager.ORANGE 
				 };
		 
		int l_wheelSize =  200; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/2 );
			
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.64f
							, ColorManager.ORANGE
							, DIRECTION.LEFT)
					);	 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.ORANGE 
				, ColorManager.YELLOW    
				, ColorManager.YELLOW_ORANGE   
				, ColorManager.ORANGE     
				, ColorManager.YELLOW_ORANGE   
				, ColorManager.ORANGE     
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors).setBackground(theme.red);
				
	}
	
	private static Level GetLevel_werwerw()
	{   
		float l_wheelSpeed= 1.11f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  280; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/8 );
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.64f
							, ColorManager.VIOLET
							, DIRECTION.LEFT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.64f
							, ColorManager.VIOLET
							, DIRECTION.LEFT)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 1.64f
							, ColorManager.VIOLET
							, DIRECTION.LEFT)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 0.64f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 

		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 0.64f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*7
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 0.64f
							, ColorManager.RED
							, DIRECTION.RIGHT)
					);	 
		  
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.RED 
				, ColorManager.VIOLET   
				, ColorManager.RED   
				, ColorManager.RED    
				, ColorManager.VIOLET   
				, ColorManager.VIOLET
				, ColorManager.RED   
				, ColorManager.RED    
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	private static Level GetLevel_gfdf()
	{   
		float l_wheelSpeed= 1.11f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  250; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/4);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.74f
							, ColorManager.BLUE
							, DIRECTION.LEFT	)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.94f
							, ColorManager.BLUE
							, DIRECTION.LEFT	)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 0.64f
							, ColorManager.GREEN
							, DIRECTION.RIGHT	)
					);	 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*4
							, shieldBlockSize 
							, l_wheelSize +70
							, (int)((l_wheelSize+70)-25) 
							, 0.84f
							, ColorManager.GREEN
							, DIRECTION.RIGHT	)
					);	 
		 
		 l_shield.add(
					new Shield(
							  shieldBlockSize*5
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.14f
							, ColorManager.BLUE
							, DIRECTION.LEFT	)
					);	
		 l_shield.add(
					new Shield(
							  shieldBlockSize*3
							, shieldBlockSize 
							, l_wheelSize +100
							, (int)((l_wheelSize+100)-25) 
							, 1.14f
							, ColorManager.BLUE
							, DIRECTION.LEFT	)
					);	
		 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.GREEN  
				, ColorManager.GREEN   
				, ColorManager.BLUE   
				, ColorManager.BLUE    
				, ColorManager.YELLOW   
				, ColorManager.YELLOW
				, ColorManager.GREEN   
				, ColorManager.GREEN   
				, ColorManager.GREEN   
				 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	private static Level GetLevel_0agfd()
	{   
		float l_wheelSpeed= 1.21f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  240; 
		Vector<Shield> l_shield = new Vector<Shield>();
		
		 int shieldBlockSize = (int)( 360/4);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +40
							, (int)((l_wheelSize+40)-25) 
							, 0.74f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT	)
					);	
		  shieldBlockSize = (int)( 360/6);
			 l_shield.add(
						new Shield(
								  shieldBlockSize*0
								, shieldBlockSize 
								, l_wheelSize +70
								, (int)((l_wheelSize+70)-25) 
								, 0.74f
								, ColorManager.DEFAULT_SHIELD_COLOR
								, DIRECTION.RIGHT)
						);	
			 
			 l_shield.add(
						new Shield(
								  shieldBlockSize*4
								, shieldBlockSize 
								, l_wheelSize +70
								, (int)((l_wheelSize+70)-25) 
								, 0.74f
								, ColorManager.DEFAULT_SHIELD_COLOR
								, DIRECTION.RIGHT)
						);	
			 l_shield.add(
						new Shield(
								  shieldBlockSize*8
								, shieldBlockSize 
								, l_wheelSize +70
								, (int)((l_wheelSize+70)-25) 
								, 0.74f
								, ColorManager.DEFAULT_SHIELD_COLOR
								, DIRECTION.RIGHT)
						);	
		 
 
		int[] l_dotColors = new int[]
		{ 
				  ColorManager.RED  
				, ColorManager.GREEN   
				, ColorManager.GREEN   
				, ColorManager.RED      
				, ColorManager.ORANGE   
				, ColorManager.BLUE   
				, ColorManager.ORANGE   
				, ColorManager.YELLOW   
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	private static Level GetLevel_0ADASF()
	{   
		float l_wheelSpeed= 0.81f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		int l_wheelSize =  250; 
		Vector<Shield> l_shield = new Vector<Shield>();
		 int shieldBlockSize = (int)( 360/3);
		 l_shield.add(
					new Shield(
							  shieldBlockSize*1
							, shieldBlockSize 
							, l_wheelSize +50
							, (int)((l_wheelSize+50)-25) 
							, 0.74f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
					);	
		 
		 l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, l_wheelSize +80
							, (int)((l_wheelSize+80)-25) 
							, 1.9f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.LEFT)
					);	
		int[] l_dotColors = new int[]{
				  
				  ColorManager.RED   
				, ColorManager.ORANGE   
				, ColorManager.VIOLET    
				, ColorManager.RED 
				, ColorManager.BLUE 
				, ColorManager.ORANGE 
				, ColorManager.BLUE  
				, ColorManager.RED  
				, ColorManager.GREEN 
				};
		
	 return new Level( l_colors, l_wheelRotation, l_wheelSpeed,l_wheelSize, l_shield,l_dotColors);
				
	}
	private static Level GetLevel_014()
	{   
		float l_wheelSpeed= 0.71f;
		DIRECTION l_wheelRotation =DIRECTION.RIGHT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		int shieldBlockSize = 360/8;
		
		Vector<Shield> l_shield = new Vector<Shield>();
		l_shield.add(
				new Shield(shieldBlockSize+15
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.7f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				);	 
		l_shield.add(
				new Shield(shieldBlockSize
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.7f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.LEFT)
				);	
		
		
		l_shield.add(
				new Shield(shieldBlockSize+15
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.8f
						, ColorManager.ORANGE
						, DIRECTION.LEFT));
		l_shield.add(
						new Shield(
								  shieldBlockSize
								, shieldBlockSize 
								, 300 +50
								, (int)((300+50)*0.93f) 
								, 1.8f
								, ColorManager.DEFAULT_SHIELD_COLOR
								, DIRECTION.LEFT)
						);
		
		
		l_shield.add(
				new Shield(shieldBlockSize +15
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.9f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				);
		l_shield.add(
								new Shield(
										  shieldBlockSize
										, shieldBlockSize 
										, 300 +50
										, (int)((300+50)*0.93f) 
										, 1.9f
										, ColorManager.DEFAULT_SHIELD_COLOR
										, DIRECTION.LEFT)
								);
		
		
		l_shield.add(
				new Shield(shieldBlockSize +15
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 2.0f
						, ColorManager.ORANGE
						, DIRECTION.LEFT)
				);
		l_shield.add(
								new Shield(
										  shieldBlockSize
										, shieldBlockSize 
										, 300 +50
										, (int)((300+50)*0.93f) 
										, 2.0f
										, ColorManager.DEFAULT_SHIELD_COLOR
										, DIRECTION.LEFT)
								);
 
		int[] l_dotColors = new int[]{
		  ColorManager.BLUE 
		, ColorManager.RED   
		, ColorManager.YELLOW   
		, ColorManager.ORANGE    
		, ColorManager.VIOLET   
		, ColorManager.BLUE    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
	}
	 
//	public static int[] GetDotColor(WHEEL_TYPE p_wheelType)
//	{
//		int[]  m_colors = null;
//		switch(p_wheelType)
//		{
//		 case PRIMARY: 
//			 m_colors = new int[]{
//					   ColorManager.YELLOW
//					 , ColorManager.RED
//					 , ColorManager.BLUE
//					 };
//				break;
//		 case SECONDARY:
//			 m_colors = new int[]{
//					   ColorManager.GREEN
//					 , ColorManager.YELLOW
//					 , ColorManager.ORANGE
//					 , ColorManager.RED
//					 , ColorManager.VIOLET
//					 , ColorManager.BLUE
//					 };
//				break;
//		 case FULL:
//			 m_colors = new int[]{
//					   ColorManager.YELLOW
//					 , ColorManager.YELLOW_ORANGE
//					 , ColorManager.ORANGE
//					 , ColorManager.RED_ORANGE
//					 , ColorManager.RED
//					 , ColorManager.RED_VIOLET
//					 , ColorManager.VIOLET
//					 , ColorManager.BLUE_VIOLET
//					 , ColorManager.BLUE
//					 , ColorManager.BLUE_GREEN
//					 , ColorManager.GREEN
//					 , ColorManager.YELLOW_GREEN
//					 }; 
//				break;
//		 } 
//		return m_colors;
//		
//	}
	 
	 private static Level GetLevel_0sdfsdf()
	{   
		float l_wheelSpeed= 0.75f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		   
		Vector<Shield> l_shield = new Vector<Shield>();
		int shieldBlockSize = 360/18; 
		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.90f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
					);
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.8f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
					);		
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.7f
						, ColorManager.VIOLET
						, DIRECTION.RIGHT)
					);	
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.55f
							, ColorManager.VIOLET
							, DIRECTION.RIGHT)
						);

			
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.65f
							, ColorManager.VIOLET
							, DIRECTION.RIGHT)
						);
		l_shield.add(
				new Shield(
						  0
						, shieldBlockSize 
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.5f
						, ColorManager.DEFAULT_SHIELD_COLOR
						, DIRECTION.RIGHT)
					);

			 
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.6f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
		
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.75f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);

		
			l_shield.add(
					new Shield(
							  0
							, shieldBlockSize 
							, 300 +50
							, (int)((300+50)*0.93f) 
							, 1.85f
							, ColorManager.DEFAULT_SHIELD_COLOR
							, DIRECTION.RIGHT)
						);
			
 
			 int[] l_dotColors = new int[]{
			  ColorManager.RED   
			, ColorManager.BLUE  
			, ColorManager.ORANGE   
			, ColorManager.VIOLET  
			, ColorManager.VIOLET  
			, ColorManager.VIOLET   
		   };
			return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
			
	}
	private static Level GetLevel_019()
	{   
		float l_wheelSpeed= 0.79f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT; 
		 int l_colors[] = new int[]{
				   ColorManager.GREEN
				 , ColorManager.YELLOW
				 , ColorManager.ORANGE
				 , ColorManager.RED
				 , ColorManager.VIOLET
				 , ColorManager.BLUE
				 };
		
		Vector<Shield> l_shield = new Vector<Shield>();

		int shieldBlockSize = 360/4;  
 
		l_shield.add(
				new ClockShield(90+360-shieldBlockSize/2
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f)  
						, ColorManager.GREEN
						, DIRECTION.RIGHT, false, 180, 100, 2.1f)
				);
		l_shield.add(
				new Shield(0
						, shieldBlockSize
						, 300 +50
						, (int)((300+50)*0.93f) 
						, 1.0f
						, ColorManager.GREEN
						, DIRECTION.RIGHT) 
				);
		 

	 
 
		int[] l_dotColors = new int[]{   
		  ColorManager.GREEN 
		, ColorManager.ORANGE   
		, ColorManager.GREEN   
		, ColorManager.ORANGE    
		, ColorManager.GREEN    
		, ColorManager.BLUE    
		, ColorManager.VIOLET    
		};
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
	 
	private static Level MenuWheel()
	{  
		float l_wheelSpeed= 1.4f;
		DIRECTION l_wheelRotation =DIRECTION.LEFT;
		 int l_colors[] = new int[]{
				   ColorManager.YELLOW
				 , ColorManager.YELLOW_ORANGE
				 , ColorManager.ORANGE
				 , ColorManager.RED_ORANGE
				 , ColorManager.RED
				 , ColorManager.RED_VIOLET
				 , ColorManager.VIOLET
				 , ColorManager.BLUE_VIOLET
				 , ColorManager.BLUE
				 , ColorManager.BLUE_GREEN
				 , ColorManager.GREEN
				 , ColorManager.YELLOW_GREEN
				 }; 

		Vector<Shield> l_shield = new Vector<Shield>();

		int[] l_dotColors = new int[]{   
		  ColorManager.GREEN };
		 
		return new Level(l_colors, l_wheelRotation, l_wheelSpeed, l_shield,l_dotColors);
		
	}
}
