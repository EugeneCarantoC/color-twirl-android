package com.eugexstudios.colortwirl.shields;
 
import com.eugexstudios.utils.Constants.DIRECTION;

public class ClockShield extends Shield  {

	private float 					   m_sweepPerTick;
	private float 					   m_tickDuration; 
	private float 					   m_clockRotateSpeed;
	
	private float 					   m_clockBuffer;
	private boolean					   m_clockRotating; 
	
	public ClockShield( 
			  int p_sweepStartAngle
			, int p_arcSize
			, int p_radius
			, int p_shellEndRadius 
			, int p_color
			, DIRECTION p_direction
			, boolean p_moveImmediate
			, float p_sweepPerTick
			, float p_tickDuration
			, float p_clockRotateSpeed
			)
	{
		super(p_sweepStartAngle, p_arcSize, p_radius, p_shellEndRadius, -1, p_color, p_direction);
		   
		m_clockBuffer      = m_clockRotating?0:m_tickDuration;
		m_clockRotating    = p_moveImmediate; 
		m_sweepPerTick     = p_sweepPerTick;
		m_tickDuration     = p_tickDuration;
		m_clockRotateSpeed = p_clockRotateSpeed;
		m_clockBuffer      = 0; 
	}
	
	@Override 
	public void UpdateOverideable(float p_deltatime)
	{
		if(m_clockRotating==false)
		{
			m_clockBuffer+= 1*p_deltatime;
			if(m_clockBuffer>m_tickDuration)
			{  
				m_clockBuffer=0;
				m_clockRotating=true; 
			}
		}
		else
		{
			if(m_direction==DIRECTION.LEFT)
			{ 
				if(m_rotation>=0f)
				{  
					m_rotation -= m_clockRotateSpeed*p_deltatime;   
				}  
				if(m_rotation<0)
				{
					m_rotation+=360;
				} 
			 } 
			 else if(m_direction==DIRECTION.RIGHT)
			 {
				 if(m_rotation<=360)
				 {
					 m_rotation+= m_clockRotateSpeed*p_deltatime; 
				 }
				 if(m_rotation>360) 
				 {
					 m_rotation-=360;
				 }
			}
			

			m_clockBuffer+=m_clockRotateSpeed *p_deltatime ; 
			if(m_clockBuffer>=m_sweepPerTick)
			{	  
				if(m_direction==DIRECTION.RIGHT)  
				{
					m_rotation-=m_clockBuffer-m_sweepPerTick;
				}
				else
				{
					m_rotation+=m_clockBuffer-m_sweepPerTick; 
				}
				m_clockBuffer=0;
				m_clockRotating=false; 
			}
		} 
	}

}
