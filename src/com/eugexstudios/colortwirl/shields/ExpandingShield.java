package com.eugexstudios.colortwirl.shields;

import com.eugexstudios.utils.Constants.DIRECTION;

public class ExpandingShield extends Shield{


	private float 					   m_expandSpeed; 
	private float 					   m_expandSize; 
	private boolean 				   m_expanding; 
	private float 					   m_expHoldTime; 
	private float					   m_expandHoldBuff;

//	private float 					   m_clockBuffer;
//	private boolean					   m_clockRotating; 
	
	
	public ExpandingShield(int p_sweepStartAngle, int p_arcSize, int p_radius, int p_endRadius, float p_rotationSpeed, int p_color, DIRECTION p_direction,float p_expandSpeed,float p_expHoldTime)
	{
		super(p_sweepStartAngle, p_arcSize, p_radius, p_endRadius, p_rotationSpeed, p_color, p_direction); 
		m_expandSpeed    = p_expandSpeed;   
		m_expHoldTime    = p_expHoldTime;
		m_expandSize     = p_arcSize;
//		m_clockRotating  = false;
		m_expanding      = true;
		m_arcSize        = 0;
//		m_clockBuffer    = 0;
		m_expandHoldBuff = 0;
	} 
	
	@Override 
	public void UpdateOverideable(float p_deltatime)
	{ 
		Rotate(p_deltatime); 
		ExpandingRotate(p_deltatime); 
		recalc(); 
	}
	
	private void ExpandingRotate(float p_deltatime)
	{
		if(m_expandHoldBuff>0)
		{
			m_expandHoldBuff-=1*p_deltatime;
		}
		else
		{
			if(m_expanding)
			{
				if(m_arcSize<m_expandSize)
					m_arcSize+= m_expandSpeed*p_deltatime;
				else
				{
					m_expandHoldBuff=m_expHoldTime;
					m_expanding=false;
				}
			}
			else
			{
				if(m_arcSize>0)
					m_arcSize-= m_expandSpeed*p_deltatime;
				else
					m_expanding=true;
			} 
		}
	}

}
