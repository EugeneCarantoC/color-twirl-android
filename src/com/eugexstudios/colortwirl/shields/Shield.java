package com.eugexstudios.colortwirl.shields;

import java.util.Vector;

import com.eugexstudios.framework.Graphics;
import com.eugexstudios.utils.Constants;
import com.eugexstudios.utils.GameMath;
import com.eugexstudios.utils.Constants.DIRECTION;

import android.graphics.Point;
import android.graphics.Rect;

public class Shield {

	private final byte 			       SHELL_UNIT_RADIUS = 10;
	private final short 			   SHELL_SWEEP_DIST  = (int)(SHELL_UNIT_RADIUS/Math.PI);
	private short[]					   SHELL_ANGLE;
	private short 			           SHELL_COUNT; 

	protected float					   m_rotation;
	protected float					   m_rotationSpeed;
	protected DIRECTION				   m_direction; 
	protected float 				   m_arcSize;  
	private Point					   m_pos;
	private int						   m_color;
	private int 				  	   m_sweepStartAngle;
	private int 					   m_shellRadius;
	private int			  		   	   m_endRadius;
	private Vector<Rect>			   m_shellBounds; 
	private Point			   		   m_endPoinA;  
	private Point			   		   m_endPoinB;  
	private short					   m_endPointRad;
	private float				       m_anim[];
	private boolean					   m_onAnim;
	private boolean					   m_roundEnd;   
	 
	public Shield(int p_sweepStartAngle, int p_arcSize, int p_radius, int p_endRadius, float p_rotationSpeed, int p_color, DIRECTION p_direction)
	{	
		m_pos			  = Constants.WHEEL_POSITION; 
		m_sweepStartAngle = p_sweepStartAngle;
		m_arcSize		  = p_arcSize;
		m_roundEnd		  = true;
		recalc();  
		m_shellRadius	  =  (int)(p_radius*Constants.SCALE);  
		m_endRadius       =  (int)(p_endRadius*Constants.SCALE); 
		m_direction		  = p_direction;
		m_rotationSpeed	  = p_rotationSpeed;
		m_color			  = p_color;
		m_rotation        = p_direction==DIRECTION.LEFT?360:0; 
		m_endPointRad	  =(short)((m_shellRadius-m_endRadius)/2); 
		m_anim 			  = new float[5];
		m_onAnim		  =	false;
		UpdateWheelShell();
	}
	
	public Shield SetPosition(Point p_pos)
	{
		m_pos = p_pos;
		return this;
	}
	public void recalc()
	{
		SHELL_COUNT       = (short)(m_arcSize/SHELL_SWEEP_DIST);
		SHELL_ANGLE       = new short[SHELL_COUNT];
		for(int idx=0;idx<SHELL_ANGLE.length;idx++)
		{
			SHELL_ANGLE[idx] = (short)(SHELL_SWEEP_DIST*idx);
		} 
	} 
	 
	public Shield setRoundEnd(boolean p_isRound)
	{
		m_roundEnd = p_isRound;
		return this;
	} 
	 
	public void SetPostion(Point p_pos)
	{
		m_pos = new Point(p_pos);
		UpdateWheelShell();
	}
	
	public void AnimateEntry()
	{
		m_onAnim=true;
		for(int idx=0;idx<m_anim.length;idx++)
		{
			m_anim[idx]=0;
		} 			 
	}
	
	public void Update(float p_deltatime)
	{  
		UpdateOverideable(p_deltatime); 
		UpdateWheelShell();
		if(m_onAnim)
		{
			 AnimUpdate(p_deltatime);
		}
	} 

	public void UpdateOverideable(float p_deltatime)
	{ 
		Rotate(p_deltatime); 
	}

	private void AnimUpdate(float p_deltatime)
	{
		//0- expand anim
		if(m_anim[0]<1)
		{
			m_anim[0] += 0.004f * p_deltatime;
			if(m_anim[0]>=1)
			{
				m_onAnim=false;
			}
		 }
	} 
	
	protected void Rotate(float p_deltatime)
	{
		if(m_direction==DIRECTION.LEFT)
		{
			if(m_rotation>=0f)
			{
				m_rotation -= m_rotationSpeed*p_deltatime;   
			}  
			if(m_rotation<0)
			{
				m_rotation+=360;
			}
		} 
		else if(m_direction==DIRECTION.RIGHT)
		{
			if(m_rotation<=360)
			{
				m_rotation+= m_rotationSpeed *(p_deltatime); 
			} 
			if(m_rotation>360) 
			{
				m_rotation-=360;
			}
		} 
	}
 
	public void PaintAnimation(Graphics g)
	{
		g.drawMaskedArc(m_pos.x, m_pos.y,  (int)(m_shellRadius*(m_anim[0])), (int)(m_endRadius*(m_anim[0])),  (int)((m_sweepStartAngle*(m_anim[0])) + m_rotation), (int)m_arcSize, m_color);  

		if(m_roundEnd)
		{
			g.drawCircle(m_endPoinA, (int)(m_endPointRad*(m_anim[0])),m_color); 
			g.drawCircle(m_endPoinB, (int)(m_endPointRad*(m_anim[0])),m_color); 
		} 
	}
	
	public void PaintDefault(Graphics g)
	{
		g.drawMaskedArc(m_pos.x, m_pos.y, m_shellRadius,  m_endRadius,   (int)(m_sweepStartAngle + m_rotation), (int)m_arcSize, m_color);   

		if(m_roundEnd)
		{
			g.drawCircle(m_endPoinA, m_endPointRad, m_color); 
	 		g.drawCircle(m_endPoinB, m_endPointRad, m_color);  
		}
	}
	
	public void PaintShellBounds(Graphics g) 
	{
		for(int idx=0;idx<m_shellBounds.size();idx++)
		{
			g.drawRect(m_shellBounds.get(idx), Constants.DEBUG_BOUNDS_COLOR_2);	 
		} 
	}

	public void UpdateWheelShell()
	{
		m_shellBounds  = new Vector<Rect>();  
		m_endPoinA     = GameMath.GetPointA(m_pos, m_endRadius+ m_endPointRad, (int)(m_sweepStartAngle+m_rotation));  
		m_endPoinB     = GameMath.GetPointA(m_pos, m_endRadius+ m_endPointRad, (int)(m_sweepStartAngle+m_rotation+m_arcSize)); 
 
		if(m_roundEnd)
		{
			m_shellBounds.add(new Rect(m_endPoinA.x - m_endPointRad,m_endPoinA.y - m_endPointRad, m_endPoinA.x +m_endPointRad,m_endPoinA.y + m_endPointRad)); 
			m_shellBounds.add(new Rect(m_endPoinB.x - m_endPointRad,m_endPoinB.y - m_endPointRad, m_endPoinB.x +m_endPointRad,m_endPoinB.y + m_endPointRad));
		}
		
		for(int idx=0;idx<SHELL_COUNT;idx++)
		{
			Point ShellPoint = GameMath.GetPointA(m_pos, m_shellRadius-SHELL_UNIT_RADIUS,m_sweepStartAngle+SHELL_ANGLE[idx]+(int)m_rotation);  
			Rect PointBounds = new Rect(ShellPoint.x - SHELL_UNIT_RADIUS,ShellPoint.y - SHELL_UNIT_RADIUS, ShellPoint.x +SHELL_UNIT_RADIUS,ShellPoint.y + SHELL_UNIT_RADIUS);  
			m_shellBounds.add(PointBounds);
		}
	}
	
	public boolean isHitted(Rect p_bounds) 
	{ 
		for(int idx=0;idx<m_shellBounds.size();idx++)
		{
			if(m_shellBounds.get(idx).intersect(p_bounds))
			{
				return true;
			}
		 }
		return false;
	}
	
	public int getColor()
	{
		return m_color;
	}
}
