package com.eugexstudios.colortwirl.shields;

import com.eugexstudios.utils.Constants.DIRECTION;

public class SpeedyShield extends Shield{

	private float  				  	   m_speedMin;
	private float 					   m_speedMax;
	private float					   m_speedIncr;
	private boolean					   m_isIncrement;
	
	public SpeedyShield(int p_sweepStartAngle, int p_arcSize, int p_radius, int p_endRadius, float p_rotationSpeed, int p_color, DIRECTION p_direction,float p_speedMin, float p_speedMax, float p_speedIncr, boolean p_isIncrement) {
		super(p_sweepStartAngle, p_arcSize, p_radius, p_endRadius, p_rotationSpeed, p_color, p_direction);
		
		m_speedMin    = p_speedMin;
		m_speedMax    = p_speedMax;
		m_speedIncr   = p_speedIncr; 
		m_isIncrement = p_isIncrement;
	}

	@Override 
	public void UpdateOverideable(float p_deltatime)
	{
		Rotate(p_deltatime);
		
		if(m_isIncrement)
		{
			if(m_rotationSpeed<m_speedMax)
			{
				m_rotationSpeed+=m_speedIncr * p_deltatime;
			}
			else
			{
				m_isIncrement = false;
			}
		}
		else
		{
			if(m_rotationSpeed>m_speedMin)
			{
				m_rotationSpeed-=m_speedIncr * p_deltatime;
			}
			else
			{
				m_rotationSpeed = m_speedMin;
				m_isIncrement   = true;
			}
		}
		
		
	}
}
