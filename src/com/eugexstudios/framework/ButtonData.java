package com.eugexstudios.framework;

import android.graphics.Rect;

public class ButtonData {

	public int x,y, src_x, src_y, width, height;
	public Rect rect;
	public ButtonData(int p_x, int p_y, int p_wid, int p_hei)
	{
		x= p_x;
		y= p_y; 
		width= p_wid;
		height= p_hei;
	    calcRect();
	}
	public ButtonData(float p_x, int p_y,   int p_wid, int p_hei)
	{
		x=(int) p_x;
		y= p_y; 
		width= p_wid;
		height= p_hei;
	    calcRect();
	}
	public ButtonData(int p_x, float p_y,  int p_wid, int p_hei)
	{
		x= p_x;
		y= (int)p_y; 
		width= p_wid;
		height= p_hei;
	    calcRect();
	}
	public ButtonData(float p_x, float p_y,  int p_wid, int p_hei)
	{
		x= (int)p_x;
		y= (int)p_y; 
		width= p_wid;
		height= p_hei;
	    calcRect();
	}
	private void calcRect()
	{
		rect = new Rect(x,y,x+width, y+height);
	}
}
