package com.eugexstudios.framework.implementation;
  
import java.lang.reflect.Field;
  
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class AndroidFastRenderView extends SurfaceView implements Runnable {
    AndroidGame 			game;
    Bitmap 					framebuffer;
    Thread 				    renderThread = null;
    SurfaceHolder			holder;
    volatile boolean running = false;
    
    private void disableSurfaceViewLogging() 
    {
        try
        {
            Field field = SurfaceView.class.getDeclaredField("DEBUG");
            field.setAccessible(true);
            field.set(null, false); 
        }
        catch (Exception e)
        { 
        }
    }
    public AndroidFastRenderView(AndroidGame game, Bitmap framebuffer) {
        super(game);
        this.game = game;
        this.framebuffer = framebuffer;
        this.holder = getHolder();   
        disableSurfaceViewLogging();
    }
 
    public void resume() 
    { 
        running = true;
        renderThread = new Thread(this);
        renderThread.start();     
    }
    public void run() 
    {
    	final float DELTAL_IMIT=  4.15f;
        Rect dstRect = new Rect();
        float startTime = System.nanoTime();  
        float lastDeltaTime=0;   
        
        while(running)
        {
        	if(!holder.getSurface().isValid())
        		continue;
        	
        	float deltaTime = (System.nanoTime() - startTime) / 10000000.000f; //orig from kilobolt 
            startTime = System.nanoTime();
            
            if(deltaTime==0)
            {
            	deltaTime=lastDeltaTime;
            }
            else
            {
            	if(deltaTime>DELTAL_IMIT)
                {
            		deltaTime= DELTAL_IMIT;
                }
            	lastDeltaTime=deltaTime;
            }
            
            try
            {
            	game.getCurrentScreen().update(deltaTime);
	            game.getCurrentScreen().paint(deltaTime);
            }
            catch(Exception e)
            { 
//            	genSet.ShowToast("Error in AndroidFastRenderView: " + e.getMessage());
            }
            
            Canvas canvas = holder.lockCanvas();
            canvas.getClipBounds(dstRect);
            canvas.drawBitmap(framebuffer, null, dstRect, null);                           
            holder.unlockCanvasAndPost(canvas); 
        }
     }


    public void pause() {                        
        running = false;                        
        while(true) {
            try {
                renderThread.join();
                break;
            } catch (InterruptedException e) {
                // retry 
            }
            
        }
    }     
    
  
}