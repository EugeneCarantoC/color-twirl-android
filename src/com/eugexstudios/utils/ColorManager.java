package com.eugexstudios.utils;

import android.graphics.Color;

public class ColorManager {

//	public static int[] RED    	      = new int[]{Color.rgb(238,40,50)}; 
//	public static int[] BLUE   	      = new int[]{Color.rgb(0,128,255)};
//	public static int[] YELLOW 	      = new int[]{Color.rgb(255,255,18)}; 
//	public static int[] GREEN  	      = new int[]{Color.rgb(34,177,76)}; 
//	public static int[] YELLOW_GREEN  = new int[]{Color.rgb(198,236,1)};
//	public static int[] ORANGE 	      = new int[]{Color.rgb(255,138,43)};
//	public static int[] YELLOW_ORANGE = new int[]{Color.rgb(253,183,0)};
//	public static int[] RED_ORANGE    = new int[]{Color.rgb(254,48,1)};
//	public static int[] RED_VIOLET    = new int[]{Color.rgb(194,0,71)}; 
//	public static int[] VIOLET        = new int[]{Color.rgb(153,0,132)};
//	public static int[] BLUE_VIOLET   = new int[]{Color.rgb(73,1,169)};
//	public static int[] BLUE_GREEN    = new int[]{Color.rgb(0,150,207)};
	

	public final static int RED    	    = Color.rgb(238,40,50); 
	public final static int BLUE   	    = Color.rgb(0,128,255);
	public final static int YELLOW 	    = Color.rgb(255,255,18); 
	public final static int GREEN  	    = Color.rgb(34,177,76); 
	public final static int YELLOW_GREEN  = Color.rgb(198,236,1);
	public final static int ORANGE 	    = Color.rgb(255,138,43);
	public final static int YELLOW_ORANGE = Color.rgb(253,183,0);
	public final static int RED_ORANGE    = Color.rgb(254,48,1);
	public final static int RED_VIOLET    = Color.rgb(194,0,71); 
	public final static int VIOLET        = Color.rgb(153,0,132);
	public final static int BLUE_VIOLET   = Color.rgb(73,1,169);
	public final static int BLUE_GREEN    = Color.rgb(0,150,207);
	public final static int WHITE         = Color.rgb(255,255,255);

	public final static int DEFAULT_SHIELD_COLOR = Color.rgb(85, 85, 85);  
	public final static int BREAKABLE_SHIELD = Color.argb(180, 255, 255, 255);  

	public final static int SHIELD_1 = Color.rgb(110, 110, 110);  
	public final static int SHIELD_2 = Color.rgb(95, 95, 95);  

	public final static int YELLOW_1       = Color.rgb(253,228,132);
	public final static int PINK_1         = Color.rgb(245,128,127); 
	 
	
	public final static int CYAN_2         = Color.rgb(26,228,202);
	public final static int BLUEGREEN_1         = Color.rgb(0,153,159);

	public final static int VIOLET_1         = Color.rgb(255,150,255);
	public final static int VIOLET_2         = Color.rgb(187,74,240);

	public final static int LIGHT_ORANGE_1         = Color.rgb(255,206,165);
	public final static int ORANGE_2         = Color.rgb(255,131,121);

	public final static int MAROON         = Color.rgb(224,73,73);

	public final static int GREEN_1         = Color.rgb(125,195,131);
	public final static int GREEN_2         = Color.rgb(106,156,120);
	public final static int YELLOW_2       = Color.rgb(255,241,188);

	public final static int MAROON_1       = Color.rgb(179,44,80);
	public final static int ORANGE_1       = Color.rgb(245,106,71); 
	public final static int MAROON_2       = Color.rgb(104,26,30); 

	public final static int VIOLET_3       = Color.rgb(101,88,138);  
	public final static int VIOLET_5       = Color.rgb(87,73,146);  
	
	public final static int GREEN_3        = Color.rgb(161,222,147);  
	public final static int RED_3          = Color.rgb(244,124,124);  
	public final static int YELLOW_3       = Color.rgb(247,244,139);  
	public final static int BLUE_3       = Color.rgb(112,161,215);  
 
	
}
