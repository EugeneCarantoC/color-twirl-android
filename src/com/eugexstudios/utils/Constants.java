package com.eugexstudios.utils;
 

import com.eugexstudios.framework.CONFIG;  	
import com.eugexstudios.framework.ImageData;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
  
public class Constants { 

	public static void SetScreenConfig(int p_wid, int p_height)
	{ 
		Point resolution = new Point(p_wid, p_height);

		 m_reso = reso_mode.default_size;
	     CONFIG.SCREEN_WIDTH   = p_wid;
	     CONFIG.SCREEN_HEIGHT  = p_height;
	     
		 if(resolution.equals(new Point(720,1280)) // Screen 1
				  ||resolution.equals(new Point(810,1440))   // Screen 1
				  ||resolution.equals(new Point(768,1200))  
		  )  
		 {
			 m_reso = reso_mode.default_size;
		 }else if( 
				 resolution.equals(new Point(480,800))   ||
				 resolution.equals(new Point(480,854)) 
				   )
			 { 
				 m_reso = reso_mode.size_b; 
			 } 
		 else if(resolution.equals(new Point(1080,1920))) 
		 { 
			 m_reso = reso_mode.size_e; 
		 } 
		 else if(
				 resolution.equals(new Point(540,960)) 
				   )
		{ 
				 m_reso = reso_mode.size_c; 
		}  
		 else if( 
				 resolution.equals(new Point(600,1024)) 
				   )
		{ 
				 m_reso = reso_mode.size_d; 
		} 
		 
 
		

	     	// RECALC all dependencies
	    CONFIG.SCREEN_MID = CONFIG.SCREEN_WIDTH/2;
		SCALE = (float)((float)CONFIG.SCREEN_WIDTH/720);	
		DOT_RADIUS   	    = (int)(30*SCALE);
		DOT_SPEED   	    = (int)(10*SCALE);
		WHEEL_POSITION  = new Point(CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.38203125f));
		SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT);
		DOT_POS_Y   	    = ScaleY(0.88515625f); 
		BACK_BTN_CIR        = ScaleX(0.1388888888888889f);   
		SETT_LANG_BTN_DIST_Y =   ScaleY(0.093f);
		switch(m_reso)
		{
		case default_size: 
			TITLE_1_LS                = 213;
		    TITLE_2_LS                = 199;
		    SETTINGS_LS               = 65;
		    SETTINGS_BTN1_S = 30;
		    SETTINGS_BTN_S            = 45;
		    ABOUT_LS                  = 50;
		    VER_LS                    = 28;
		    CREATED_LS                = 38; 
		    EUGEX_LS                  = 40; 
		    PLAY_MORE_LS              = 26;
		    EXPLR_GAMES_LS            = 35; 
		    SHARE_CT_LS               = 26;  
		    SHARE_NOW_LS              = 35;  
		    LEVEL_NUM_LS              = 45;  
		    SELECT_STAGE_LS           = 65;
		    LEVEL_LS                  = 50;  
		    CUR_LEVEL_LS              = 100; 
		    GAME_OVER_LS              = 85;  
		    STAGE_COMP_LS             = 75;
		    ASK_EXIT_LS               = 50; 
		    RESTART_BS                = 155;  
		    START_BTN				  = 125;  
		    SETT_LANG_LS = 65;   
			SETT_LANG_BTN_LS = 50;  
			PLAY_INSTR_1 = 40;   
		    START_BTN_IMG = new ImageData(CONFIG.SCREEN_MID -116/2, Constants.ScaleY(0.765625f)-119/2,  5,324,124,119);   
		    MENU_SETTINGS_BTN = new ImageData(Constants.ScaleX(0.1402777777775556f),  Constants.ScaleY(0.04921875f), 0,0,76,79);
		    MENU_ABOUT_BTN = new ImageData(Constants.ScaleX(0.10f),  Constants.ScaleY(0.13515625f),105,10,35,55);
		    MENU_GIFT_BTN = new ImageData(Constants.ScaleX(0.1165f),  Constants.ScaleY(0.2033125f),271,205,56,60);
		    LOCK_IMG = new ImageData(-90/2, 0, 9+ 90*0,1112,90,90);     
		    BACK_IMG = new ImageData(Constants.BACK_BTN_CIR/2 -70/2, Constants.BACK_BTN_CIR/2-70/2, 3+70*0,1197,70,70);  
//		    HOME_IMG      = new ImageData( , 0,1288+80*0,85,85);  
//		    LEVEL_SEL_IMG = new ImageData( , 90,1288+80*0,85,85);   
			break;
		case size_b: 
			TITLE_1_LS                = 213-74;
		    TITLE_2_LS                = 199-74;
		    START_BTN				  = 125-44;  
		    SETTINGS_LS               = 65-25;
		    SETTINGS_BTN1_S           = 30-10;
		    SETTINGS_BTN_S            = 45-12;
		    ABOUT_LS                  = 50-15;
		    CT_LS                     = 75-22;
		    VER_LS                    = 28-8;
		    CREATED_LS                = 38-12; 
		    EUGEX_LS                  = 40-12; 
		    PLAY_MORE_LS              = 26-8;
		    EXPLR_GAMES_LS            = 35-8; 
		    SHARE_CT_LS               = 26-8;  
		    SHARE_NOW_LS              = 35-10;  
		    LEVEL_NUM_LS              = 45-17;  
		    SELECT_STAGE_LS           = 65-23;
		    LEVEL_LS                  = 50-16;  
		    CUR_LEVEL_LS              = 100-36; 
		    GAME_OVER_LS              = 85-21;  
		    STAGE_COMP_LS             = 75-25;
		    ASK_EXIT_LS               = 50-10; 
		    RESTART_BS                = 155-50;   
		    SETT_LANG_LS = 65-26;   
			SETT_LANG_BTN_LS = 50-18;   
			PLAY_INSTR_1 = 40-13;   
		    START_BTN_IMG = new ImageData(CONFIG.SCREEN_MID -145/2, Constants.ScaleY(0.765625f)-145/2,  145*3,953,145,145);  
		    LOCK_IMG = new ImageData(-90/2, 0,  9+ 90*4,1112, 90, 90);   
		    
		    MENU_SETTINGS_BTN = new ImageData(Constants.ScaleX(0.1505f),  Constants.ScaleY(0.04375f), 61,881,61,62);   
		    MENU_ABOUT_BTN    = new ImageData(Constants.ScaleX(0.100f),  Constants.ScaleY(0.133f), 140,887,31,49); 
		    MENU_GIFT_BTN = new ImageData(Constants.ScaleX(0.1221f),  Constants.ScaleY(0.2033125f),178,890,43,47);
		    BACK_IMG = new ImageData(Constants.BACK_BTN_CIR/2 -70/2, Constants.BACK_BTN_CIR/2-70/2, 3+70*4,1198,70,70);   

			break;
		case size_c: 
			TITLE_1_LS                = 213-58;
		    TITLE_2_LS                = 199-58;
		    START_BTN				  = 125-35;  
		    SETTINGS_LS               = 65-18;
		    SETTINGS_BTN1_S            = 30 -5;
		    SETTINGS_BTN_S            = 45 -8;
		    ABOUT_LS                  = 50-15;
		    CT_LS                     = 75-22;
		    VER_LS                    = 28-8;
		    CREATED_LS                = 38-12; 
		    EUGEX_LS                  = 40-12; 
		    PLAY_MORE_LS              = 26-8;
		    EXPLR_GAMES_LS            = 35-8; 
		    SHARE_CT_LS               = 26-8;  
		    SHARE_NOW_LS              = 35-10;  
		    LEVEL_NUM_LS              = 45-12;  
		    SELECT_STAGE_LS           = 65-16;
		    LEVEL_LS                  = 50-10;  
		    CUR_LEVEL_LS              = 100-36; 
		    GAME_OVER_LS              = 85-19;  
		    STAGE_COMP_LS             = 75-25;
		    ASK_EXIT_LS               = 50-10; 
		    RESTART_BS                = 155-50;    
		    SETT_LANG_LS = 65-22;   
			SETT_LANG_BTN_LS = 50-14;  
 
		    START_BTN_IMG = new ImageData(CONFIG.SCREEN_MID -145/2, Constants.ScaleY(0.765625f)-145/2,  145*2,953,145,145);   
		    
		    MENU_SETTINGS_BTN = new ImageData(Constants.ScaleX(0.1505f),  Constants.ScaleY(0.04375f), 278,811,66,67);   
		    MENU_ABOUT_BTN = new ImageData(Constants.ScaleX(0.100f),  Constants.ScaleY(0.133f), 368,821,28,47); 
		    MENU_GIFT_BTN = new ImageData(Constants.ScaleX(0.1221f),  Constants.ScaleY(0.2033125f),412,825,56,60);
		    LOCK_IMG = new ImageData(-90/2, 0,  9+ 90*3,1112, 90, 90);   
		    BACK_IMG = new ImageData(Constants.BACK_BTN_CIR/2 -70/2, Constants.BACK_BTN_CIR/2-70/2, 3+70*4,1198,70,70);   
			
			break;
			case size_d: 
				TITLE_1_LS                = 213-28;
			    TITLE_2_LS                = 199-28;
			    START_BTN				  = 125-15;  
			    SETTINGS_LS               = 65-13;
			    ABOUT_LS                  = 50-7;
			    CT_LS                     = 75-10;
			    VER_LS                    = 28-4;
			    CREATED_LS                = 38-6; 
			    EUGEX_LS                  = 40-6; 
			    PLAY_MORE_LS              = 26-4;
			    EXPLR_GAMES_LS            = 35-4; 
			    SHARE_CT_LS               = 26-4;  
			    SHARE_NOW_LS              = 35-5;  
			    LEVEL_NUM_LS              = 45-8;  
			    SELECT_STAGE_LS           = 65-10;
			    LEVEL_LS                  = 50-5;  
			    CUR_LEVEL_LS              = 100-16; 
			    GAME_OVER_LS              = 85-1;  
			    STAGE_COMP_LS             = 75-12;
			    ASK_EXIT_LS               = 50-5; 
			    RESTART_BS                = 155-203;    
				break;
				
			
				
			case size_e:
				TITLE_1_LS                = 213+100;
			    TITLE_2_LS                = 199+100;
			    START_BTN				  = 125+40;  
			    SETTINGS_LS               = 65+35; 
			    ABOUT_LS                  = 50+30; 
			    CT_LS                     = 75+20;
			    VER_LS                    = 28+20;
			    CREATED_LS                = 38+20; 
			    EUGEX_LS                  = 40+20; 
			    PLAY_MORE_LS              = 26+20; 
			    EXPLR_GAMES_LS            = 35+15; 
			    SHARE_CT_LS               = 35+10;  
			    SHARE_NOW_LS              = 35+15;  
			    LEVEL_NUM_LS              = 45+15;  
			    SELECT_STAGE_LS           = 65+30; 
			    LEVEL_LS                  = 50+30;  
			    CUR_LEVEL_LS              = 100+50;  
			    GAME_OVER_LS              = 85+50;  
			    STAGE_COMP_LS             = 75+40;   
			    ASK_EXIT_LS               = 50+30; 
			    RESTART_BS    	          = 155+100;   
			    START_BTN_IMG = new ImageData(CONFIG.SCREEN_MID -145/2, Constants.ScaleY(0.765625f)-145/2,  145*2,953,145,145);    
			    MENU_SETTINGS_BTN = new ImageData(Constants.ScaleX(0.1505f),  Constants.ScaleY(0.04375f), 278,811,66,67);   
			    MENU_ABOUT_BTN = new ImageData(Constants.ScaleX(0.100f),  Constants.ScaleY(0.133f), 368,821,28,47); 
			    MENU_GIFT_BTN = new ImageData(Constants.ScaleX(0.1221f),  Constants.ScaleY(0.2033125f),412,825,56,60);
			    LOCK_IMG = new ImageData(-90/2, 0,  9+ 90*3,1112, 90, 90);   
			    BACK_IMG = new ImageData(Constants.BACK_BTN_CIR/2 -70/2, Constants.BACK_BTN_CIR/2-70/2, 3+70*0,1197,70,70);    
			     
				break;
		}
		
	}

	 
	public static int ScaleX(float p_perc)
	{
		return (int)(CONFIG.SCREEN_WIDTH*p_perc);	
	}
	public static int ScaleY(float p_perc)
	{
		return (int)(CONFIG.SCREEN_HEIGHT*p_perc);	
	}
	 
   public enum reso_mode
   {
   	 default_size
    	,size_b
    	,size_c
    	,size_d 
    	,size_e 
   }
   public  static reso_mode m_reso;

	//STARTAPP 
	public final static String NATIVE_AD_EXIT     = "exit_native_ad_01"; 
	public final static String NATIVE_AD_GAMEOVER = "game_gameover_ad_02";   
	public final static String VIDEO_AD_CONTINUE  = "game_video_ad_01"; 
	public final static String VIDEO_AD_LVLCOMP  = "video_ad_lvlcomp"; 
	public final static String REWARD_VIDEO_AD_CONT  = "video_ad_reward_cont"; 
	public final static String BETA_LVL_TRY_      ="BETA_LVL_TRY_"; 
	
	public final static int VIDEO_AD_CHANCE =3; // 33% to get a video ad on level complete
	public final int NO_HIT =-1; 
	public final int WHEEL_SECTION_GUIDE_RAD = 50;	
	public  static float SCALE = (float)((float)CONFIG.SCREEN_WIDTH/720);	 // COnstant yung 720, jan original dinesign yung game
	public final int BANNER_AD_SPACE_HEIGHT=125;
	
	public final static int WHEEL_PAINT_ANIM_SPEED = 15;	
	public   static Point WHEEL_POSITION  = new Point(CONFIG.SCREEN_MID, (int)(CONFIG.SCREEN_HEIGHT*0.38203125f));
	
	public   static Rect SCREEN_RECT = new Rect(0,0,CONFIG.SCREEN_WIDTH, CONFIG.SCREEN_HEIGHT);
	public   static int DOT_RADIUS   	    = (int)(30*SCALE);
	public   static int DOT_POS_Y   	    = (int)(CONFIG.SCREEN_HEIGHT*0.88515625f);
	public final int DOT_CIRCUM   	    = DOT_RADIUS*2;
	public   static int DOT_SPEED   	    = (int)(10*SCALE);

	public final int SHADOW_COLOR    		= Color.argb(100,20, 20, 20);
	public final static int ARC_INACTIVE_OVERLAY = Color.argb(150,255, 255, 255); 
	public final int DEBUG_BOUNDS_COLOR   = Color.argb(200, 0, 255,0);
	public final static int DEBUG_BOUNDS_COLOR_2 = Color.argb(50, 255, 255,240);
	public final static int PASTELIZER_COLOR = Color.argb(70, 255, 255, 255);
	public final int GAMEOVER_COLOR_OVERLAY = Color.rgb(245, 30,20);
 	   
	public enum DIRECTION{LEFT, RIGHT};

	public final static int SHOW_AD_AFTER_N_PLAY = 3;

	public static ImageData START_BTN_IMG;
	public static ImageData MENU_SETTINGS_BTN;
	public static ImageData MENU_ABOUT_BTN;
	public static ImageData MENU_GIFT_BTN;
	public static ImageData LOCK_IMG;
	public static ImageData BACK_IMG;

	public static ImageData HOME_IMG;
	public static ImageData LEVEL_SEL_IMG; 
	
	public static int TITLE_1_LS               = 213;
	public static int TITLE_2_LS               = 199;
	public static int START_BTN               = 125;  
	public static int BACK_BTN_CIR               = ScaleX(0.0694444444444444f);   
	public static int SETTINGS_LS               = 65;
	public static int SETTINGS_BTN1_S               = 30; 
	public static int SETTINGS_BTN_S               = 45; 
	public static int ABOUT_LS               = 50;
	public static int CT_LS               = 75;
	public static int VER_LS               = 28; 
	public static int CREATED_LS               = 38; 
	public static int EUGEX_LS               = 40; 
	public static int PLAY_MORE_LS               = 26; 
	public static int EXPLR_GAMES_LS               = 35; 
	public static int SHARE_CT_LS               = 26;  
	public static int SHARE_NOW_LS               = 35; 
	public static int SELECT_STAGE_LS               = 65;  
	public static int LEVEL_NUM_LS               = 45;  

	public static int CONGRATS_LS               = 60;  

	public static int ASK_EXIT_LS               = 55; 
	public static int CONT_GAME_LS               = 75;  

	public static int GAME_OVER_LS               = 85;  
	public static int STAGE_COMP_LS               = 75;  
	public static int COUNTDOWN_LS               = 180;  
	public static int LEVEL_LS               = 50;  
	public static int INSTALL_LS               = 62;  
	public static int CUR_LEVEL_LS               = 100;  
	public static int RESTART_BS = 155;   
	public static int SETT_LANG_LS = 65;   
	public static int SETT_LANG_BTN_LS = 50;   
	public static int SETT_LANG_BTN_DIST_Y;
	public static int PLAY_INSTR_1 = 40;   
	
	
}
