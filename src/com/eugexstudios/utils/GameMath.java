package com.eugexstudios.utils; 

import android.graphics.Point;

public class GameMath {

	public static int GetTriangleAngleB(int p_cirX, int p_cirY, int p_pointX, int p_pointY)
	{
		float radian = (float) Math.atan2(p_pointY - p_cirY, p_pointX - p_cirX);
		int angle = (int) (radian * (180 / Math.PI));
		if (angle < 0.0)
			angle += 360.0;
		return angle;
	}
	public static int AdjustAngle(int p_old, int p_adjust)
	{
		int angle_adj=  p_old+p_adjust;
		angle_adj= angle_adj>360? angle_adj-360:angle_adj;
		angle_adj= angle_adj<0?   360 - angle_adj:angle_adj;
		return angle_adj;
		
	}

	public static int GetTriangleAngleB(Point p_pointB, Point p_pos) 
	{
		return GetTriangleAngleB(p_pointB.x,p_pointB.y,p_pos.x,p_pos.y);
	}

	public static int GetTriangleAngleB(Point p_pointB, int p_pointX, int p_pointY) 
	{
		return GetTriangleAngleB(p_pointB.x,p_pointB.y,p_pointX,p_pointY);
	}

	public static Point GetPointA(Point p_pointB, int p_rad, int p_angle)
	{  
		return 
				new Point(
				 (int)(p_pointB.x + (p_rad * Math.cos(p_angle * Math.PI / 180)))
				,(int)(p_pointB.y + (p_rad * Math.sin(p_angle * Math.PI / 180))));
	}

	public static Point getTrianglePointA(Point p_pointB, Point p_pointC, int p_angleB)
	{  
		Point Line1PointA = GetPointA(p_pointB, p_pointB.x*2, p_angleB);
	    Point Line1PointB = new Point(p_pointB.x,p_pointB.y);

	    Point Line2PointA = new Point(p_pointC.x,-p_pointC.y*2);
	    Point Line2PointB = new Point(p_pointC.x,p_pointC.y);
	    
	    Point PointA= getLineLineIntersection(Line1PointB,Line1PointA,Line2PointB,Line2PointA);
	    
	    return PointA;
	}
	
	/*---------------------------------------------------------------------------------------------A1
	 * Fastest linesIntersect method
	 * By CommanderKeit
	 * Posted June 09, 2010
	 * 
	 * JavaGaming.org
	 * */
	public static Point getLineLineIntersection(Point p_line1_pointA,Point p_line1_pointB, Point p_line2_pointA,Point p_line2_pointB)
	{
		return getLineLineIntersection(
					 p_line1_pointA.x,p_line1_pointA.y
					,p_line1_pointB.x,p_line1_pointB.y
					,p_line2_pointA.x,p_line2_pointA.y
					,p_line2_pointB.x,p_line2_pointB.y);
	}
	
	public static Point getLineLineIntersection(double x1, double y1, double x2, double y2,
										        double x3, double y3, double x4, double y4)
	{
			double det1And2 = det(x1, y1, x1, y2);
			double det3And4 = det(x3, y3, x4, y4);
			double x1LessX2 = x1 - x2;
			double y1LessY2 = y1 - y2;
			double x3LessX4 = x3 - x4;
			double y3LessY4 = y3 - y4;
			double det1Less2And3Less4 = det(x1LessX2, y1LessY2, x3LessX4, y3LessY4);
			if (det1Less2And3Less4 == 0){
			// the denominator is zero so the lines are parallel and there's either no solution (or multiple solutions if return null;
			}
			double x = (det(det1And2, x1LessX2,
			det3And4, x3LessX4) /
			det1Less2And3Less4);
			double y = (det(det1And2, y1LessY2,
			det3And4, y3LessY4) /
			det1Less2And3Less4);
			return new Point((int)x, (int)y);
			}
	
			protected static double det(double a, double b, double c, double d) {
			return a * d - b * c;
			} 
			
	public static boolean linesIntersect(double x1, double y1, double x2, double y2,double x3, double y3, double x4, double y4)
	{
					// Return false if either of the lines have zero length
					if (x1 == x2 && y1 == y2 ||
					x3 == x4 && y3 == y4){
					return false;
					}
					// Fastest method, based on Franklin Antonio's "Faster Line Segment Intersection" topic "in Graphics Gems III" double ax = x2�?x1;
					double ax = x2-x1;
					double ay = y2-y1;
					double bx = x3-x4;
					double by = y3-y4;
					double cx = x1-x3;
					double cy = y1-y3;
					double alphaNumerator = by*cx- bx*cy;
					double commonDenominator = ay*bx- ax*by;
					if (commonDenominator > 0){
					if (alphaNumerator < 0 || alphaNumerator > commonDenominator){
					return false;
					}
					}else if (commonDenominator < 0){
					if (alphaNumerator > 0 || alphaNumerator < commonDenominator){
					return false;
					}
					} 
					double betaNumerator = ax*cy - ay*cx;
					if (commonDenominator > 0){
						if (betaNumerator < 0 || betaNumerator > commonDenominator){
						return false;
						}
						}else if (commonDenominator < 0){
						if (betaNumerator > 0 || betaNumerator < commonDenominator){
						return false;
						}
						}
						if (commonDenominator == 0){
						// This code wasn't in Franklin Antonio's method. It was added by Keith Woodward.
						// The lines are parallel.
						// Check if they're collinear.
						double y3LessY1 = y3-y1;
						double collinearityTestForP3 = x1*(y2-y3) + x2*(y3LessY1) + x3*(y1-y2);
						// If p3 is collinear with p1 and p2 then p4 will also be collinear, sin
						if (collinearityTestForP3 == 0){
							// The lines are collinear. N ow check if they overlap.
							if (x1 >= x3 && x1 <= x4 || x1 <= x3 && x1 >= x4 ||
							x2 >= x3 && x2 <= x4 || x2 <= x3 && x2 >= x4 ||
							x3 >= x1 && x3 <= x2 || x3 <= x1 && x3 >= x2){
							if (y1 >= y3 && y1 <= y4 || y1 <= y3 && y1 >= y4 ||
							y2 >= y3 && y2 <= y4 || y2 <= y3 && y2 >= y4 ||
							y3 >= y1 && y3 <= y2 || y3 <= y1 && y3 >= y2){
							return true;
							}
							}
							}
							return false;
							}
							return true;
							}
	/*---------------------------------------------------------------------------------------------A1
	 * */
}
